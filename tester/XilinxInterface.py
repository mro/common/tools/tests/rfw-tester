# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-12-13
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name, disable=duplicate-code, disable=too-many-public-methods

"""Interface for automating Xilinx XSCT"""

import re
import struct
from typing import List

from robot.api import logger
from robot.api.deco import keyword, library
from robot.api.exceptions import Error
from tester.XilinxClient import XsctClient
from tester.XilinxServer import XsctServer

PORT = 56789


@library
class XilinxInterface:
    r"""
    Library for using Xilinx Software Command-Line Tool (XSCT) in Robot Framework.

    ``XilinxInterface`` is a library for using Xilinx Software Command-Line Tool (XSCT)
    providing a set of keywords for interacting with the XSCT prompt.

    = Table of Contents =

    %TOC%

    = Usage =

    This library is intended to be used in conjunction with a docker container
    (or some similar alternative) that contains a working Vitis environment and
    the relevant files. The library is accompanied by a companion set of keywords
    found in the resource file `XilinxInterfaceBDD`. It is recommended to use the
    keywords in the resource file when writing tests.

    To import the library, use the following syntax:

    | Library    tester.XilinxInterface

    """

    def __init__(
        self, command: str = None, host: str = None, port: int = PORT, **kwargs
    ) -> None:
        """
        Initialise XilinxInterface

        === Arguments ===

        ``command``: <str>
            Initial command - recommended to be a command to navigate to the directory
            containing the docker container and to execute `x-builder xsct`

            The aim of the command is to enter a working vitis environment and to start
            XSCT. It needs to be done in one line, if this isn't possible, perhaps the use
            of a shell script is recommended.

        ``host``: <str>
            Hostname of the xsdbserver

        ``port``: <int>
            Port to connect to the xsdbserver
        """

        self._server = XsctServer()
        self._client = XsctClient()

        if all([command, port]):
            self.start_server(command, port, **kwargs)

        if port and self._server.is_alive():
            self.start_client(host, port)

        self.device_aliases = {}
        self.connections = []

    def __del__(self):
        """
        Stops the XSCT server and client
        """
        try:
            self.stop()
        except Error:
            pass

    @keyword(types={"command": str, "host": str, "port": int, "timeout": int})
    def start(self, command, host=None, port=PORT, timeout=30, **kwargs) -> None:
        """
        Start the XSCT server and client

        === Arguments ===

        ``command``: <str>
            Command to start the XSCT server, required

        ``host``: <str>
            Hostname of the xsdbserver, optional - default is localhost

        ``port``: <int>
            Port to connect to the xsdbserver, optional - default is 56789

        ``timeout``: <int>
            Timeout for the connection, optional - default is 30 seconds

        === Description ===

        Starts the XSCT server and client. The XSCT server is started using the provided
        command and optionally, the hostname and port can be specified. The XSCT client
        is then started and connects to the server using the provided hostname and port.

        The specified command is recommended to be something similar to:

        |   bash -c 'cd /path/to/xilinx-workspace; x-builder xsct'

        This will start the container and XSCT server in the given directory.
        """

        self.start_server(command, host, port, timeout, **kwargs)
        self.start_client(host, port, timeout)

    @keyword
    def stop(self) -> None:
        """
        Stop the XSCT server and client
        """

        self._client.disconnect()
        self._server.stop_server()

    @keyword(types={"command": str, "port": int})
    def start_server(self, command, host=None, port=PORT, timeout=30, **kwargs) -> None:
        """
        Start the XSCT server

        Similar to `Start`, but only starts the XSCT server.

        === Arguments ===

        ``command``: <str>
            Command to start the XSCT server

        ``host``: <str>
            Hostname of the xsdbserver - hostname the server listens for incoming connections
            from the client

        ``port``: <int>
            Port to connect to the xsdbserver

        ``timeout``: <int>
            Timeout for the connection
        """

        if self._server.is_alive():
            raise Error("ERROR: XSCT server is already running")

        self._server.start_server(command, host, port, timeout, **kwargs)

    @keyword(types={"host": str, "port": int, "timeout": int})
    def start_client(self, host="localhost", port=PORT, timeout=10):
        """
        Start the XSCT client

        Similar to `Start`, but only starts the XSCT client.

        === Arguments ===

        ``host``: <str>
            Hostname of the xsdbserver, default is localhost

        ``port``: <int>
            Port to connect to the xsdbserver

        ``timeout``: <int>
            Timeout for the connection
        """

        if self._client.is_connected():
            raise Error("ERROR: XSCT client is already connected")
        if not self._server.is_alive():
            raise Error("ERROR: XSCT server is not running")

        self._client.connect(host, port, timeout)

    @keyword
    def stop_server(self) -> None:
        """
        Stop the XSCT server
        """

        self._server.stop_server()

    @keyword
    def stop_client(self) -> None:
        """
        Stop the XSCT client
        """

        self._client.disconnect()

    @keyword
    def get_interface_details(self):
        """
        Get the interface details

        === Description ===

        Get the interface details.
        """

        report = {"server": {}, "client": {}}

        if self._server.is_alive():
            report["server"]["state"] = "alive"
            report["server"]["hostname"] = self._server.xsdbserver.hostname
            report["server"]["port"] = self._server.xsdbserver.port
        else:
            report["server"]["state"] = "dead"
            report["server"]["hostname"] = None
            report["server"]["port"] = None

        if self._client.is_connected():
            report["client"]["state"] = "connected"
            report["client"]["hostname"] = self._client.host
            report["client"]["port"] = self._client.port
        else:
            report["client"]["state"] = "disconnected"
            report["client"]["hostname"] = None
            report["client"]["port"] = None

        logger.info(f"Interface details: {report}")

        return report

    @keyword
    def execute_command(self, command, timeout=10):
        """
        Execute a command on the XSCT client

        === Arguments ===

        ``command``: <str>
            Command to execute

        ``timeout``: <int>
            Timeout for the command

        === Description ===

        Execute a command on the XSCT client. This requires being connected to
        the XSCT server.
        """

        self._check()

        logger.info(f"Executing command: {command}")
        return self._client.exec(command, timeout=timeout)

    @keyword(types={"device": str, "timeout": int})
    def connect(self, device: str, timeout: int = 10):
        """
        Connect to a device

        === Arguments ===

        ``device``: <str>
            Device to connect to - either an alias or a URL

        ``timeout``: <int>
            Timeout for the connection

        === Description ===

        Connect to a JTAG device. Using either an alias or a URL, uses the XSCT command
        `connect` to connect to the device.

        https://docs.xilinx.com/r/en-US/ug1400-vitis-embedded/connect

        See `Connect to Device` and `Connect to Device URL` for more information.
        """

        self._check()

        if device in self.device_aliases:
            return self.connect_to_device(device, timeout)
        return self.connect_to_device_url(device, timeout)

    @keyword(types={"channel_id": str, "timeout": int})
    def disconnect(self, channel_id: str = None, timeout: int = 10):
        """
        Disconnect from a device

        === Arguments ===

        ``channel_id``: <str>
            Device to disconnect from - given as the channel-id
            Optional - defaults to the active channel

        ``timeout``: <int>
            Timeout

        === Description ===

        Disconnect from a JTAG device. Uses the channel-id and the XSCT command
        `disconnect` to disconnect from the device.

        https://docs.xilinx.com/r/en-US/ug1400-vitis-embedded/disconnect
        """

        self._check()

        if not self.connections:
            raise Error("ERROR: No connected devices")

        cmd = "disconnect"
        active_channel = None

        if channel_id in self.connections:
            logger.info(f"Disconnecting from channel {channel_id}")
            cmd += f" {channel_id}"
        else:
            logger.info("Disconnecting from active channel")
            active_channel = self.get_active_channel(timeout=timeout)

        self._client.exec(cmd, timeout=timeout)

        if channel_id:
            self.connections.remove(channel_id)
        elif active_channel:
            self.connections.remove(active_channel)
        else:
            raise Error(
                "ERROR: Failed to remove connection from list "
                "of connections after disconnecting"
            )

    def connect_to_device(self, alias, timeout=10):
        """
        Connect to a device using an alias

        === Arguments ===

        ``alias``: <str>
            Device to connect to (as an alias)

        === Example ===

        | Connect to Device    cfn-937-sam2

        == Description ==

        Connect to the device using the provided alias. The alias must be defined in the
        `device_aliases` dictionary and must correspond to a device URL.

        """

        self._check()

        if alias not in self.device_aliases:
            raise Error(f"ERROR: Device alias {alias} does not exist")
        logger.info(f"Connecting to {alias} -> {self.device_aliases[alias]}")
        return self.connect_to_device_url(self.device_aliases[alias], timeout)

    def connect_to_device_url(self, url, timeout=10):
        """
        Connect to a device

        === Arguments ===

        ``url``: <str>
            Device to connect to (as a URL)
        """

        self._check()

        logger.info(f"Connecting to target JTAG: {url}")
        try:
            connection = self._client.exec(f"connect -path {url}", timeout=timeout)
        except Error as e:
            raise Error(f"ERROR: Could not connect to {url} -> {e}") from e
        self.connections.append(connection)

        return connection

    @keyword(types={"timeout": int})
    def get_active_channel(self, timeout: int = 10):
        """
        Get the active channel

        === Description ===

        Get the active channel, queries the XSCT command `connect -list` to get the
        channels the client is connected to and returns the active channel.

        Returns the active channel as a string if found, otherwise returns None.
        """

        self._check()

        # if not self.connections:
        #     raise Error("ERROR: No connected devices")

        logger.info("Getting active channel")
        channels = self._client.exec("connect -list", timeout=timeout)
        try:
            active_channel = re.search(r"\*.*?(\S+)", channels).group(1)
        except AttributeError:
            active_channel = None

        if not active_channel:
            logger.info("No active channel found")
            return None
        logger.info(f"Active channel: {active_channel}")
        return active_channel

    @keyword(types={"channel_id": str, "timeout": int})
    def set_active_channel(self, channel_id: str, timeout: int = 10):
        """
        Set the active channel

        === Arguments ===

        ``channel_id``: <str>
            Channel to set as active

        === Description ===

        Set the active channel, uses the XSCT command `connect -set <channel-id>` to change
        the active connection
        """

        self._check()

        if not self.connections:
            raise Error("ERROR: No connected devices")

        if channel_id not in self.connections:
            raise Error(f"ERROR: Channel {channel_id} not found")

        logger.info(f"Setting active channel to {channel_id}")
        self._client.exec(f"connect -set {channel_id}", timeout=timeout)

    def _get_targets(self, timeout: int = 10):
        """
        Get the device targets, returns the raw output from XSCT
        """

        self._check()

        if not self.connections:
            raise Error("ERROR: No connected devices")

        logger.info("Getting targets")
        targets = self._client.exec("targets", timeout=timeout)
        return targets

    @staticmethod
    def _parse_targets(line: str):
        """
        Parse a line from the targets output from XSCT

        === Arguments ===

        ``line``: <str>
            line from the output from XSCT targets command

        == Description ==

        Process a line targets output from XSCT. This requires being connected to
        the XSCT prompt.

        Returns a tuple, where each tuple contains the index, name and
        state of the target.
        """

        logger.debug(f"Parsing target: {line}")
        line = line.strip()
        index, data = line.split(" ", 1)
        index = int("".join([char for char in index if char.isdigit()]))
        try:
            name, state = data.split("(")
            name = name.strip()
            state = state.strip(")")
        except ValueError:
            name = data.strip()
            state = None

        logger.debug(f"Target: {index}, {name}, {state}")
        return index, name, state

    @keyword(types={"timeout": int})
    def get_device_targets(self, timeout=10):
        """
        Get the device targets

        === Description ===

        Get the device targets. Only works when connected to a JTAG device
        (see `Connect`), otherwise raises an error.

        Returns a list of tuples, where each tuple contains the index, name and
        state of the target.
        """

        targets = self._get_targets(timeout)
        res = [self._parse_targets(line) for line in targets.split("\\n") if line]

        return res

    @keyword(types={"timeout": int})
    def get_active_target(self, timeout=10):
        """
        Get the presently active target

        === Description ===

        Get the active targets Only works when connected to a JTAG device
        (see `Connect`), otherwise raises an error.

        Returns the index, name and state of the active target as a tuple.
        """

        targets = self._get_targets(timeout)

        active_target = None

        for line in targets.split("\\n"):
            active_target = re.search(r"\d+\*", line)
            if active_target:
                active_target = line
                break

        if not active_target:
            return None

        index, name, state = self._parse_targets(active_target)
        target = (index, name, state)

        return target

    @keyword(types={"target": (str, int), "timeout": int})
    def select_target(self, target, timeout=10, **kwargs):
        """
        Select a target on the device

        === Arguments ===

        ``target``: <str> or <int>
            Target to select - either an index or a name

        ``timeout``: <int>
            Timeout for the command

        === Description ===

        Select a target on the device. Using either an index or a name, uses the XSCT command
        `targets` to select the target.

        https://docs.xilinx.com/r/en-US/ug1400-vitis-embedded/targets

        It makes the specified target the active target. Once a target is specified, further XSCT
        commands become available such as those involving reading and writing to memory.
        """

        cmd = None

        self._check()

        # check that there is a connection
        if not self.connections:
            raise Error("ERROR: No connected devices")

        targets = self.get_device_targets(timeout=timeout)

        # check that the target exists
        if isinstance(target, int) or target.isdigit():
            if int(target) not in [index for index, _, _ in targets]:
                raise Error(f"ERROR: Target {target} not found in list of targets")
            cmd = f"targets {target}"

        elif isinstance(target, str):
            if target not in [name for _, name, _ in targets]:
                raise Error(f"ERROR: Target {target} not found in list of targets")
            logger.info(f"Target given as string: {target}")
            target = [index for index, name, _ in targets if name == target][0]
            cmd = f"targets {target}"

        else:
            raise Error(f"ERROR: Target {target} not found in list of targets")

        # switch to the target
        logger.info(f"Selecting target {target}")
        return self._client.exec(cmd, timeout=timeout, **kwargs)

    @keyword(types={"address": str, "num": int, "fmt": str, "timeout": int})
    def memory_read(
        self, address: str, num: int = 1, fmt: str = None, timeout: int = 5, **kwargs
    ) -> list:
        """
        Read a range of memory

        Read <num> data values from the active target's memory address specified by <address>
        and return a list of values in the specified format.

        https://docs.xilinx.com/r/2022.2-English/ug1400-vitis-embedded/mrd

        === Arguments ===

        ``address``: <str>
            start address of memory range

        ``num``: <int>
            number of data values to read from the active targets memory

        ``fmt``: <str>
            python unpacking format
            (see https://docs.python.org/3/library/struct.html#format-strings)

            An optional argument, when None the data is returned in the native hexadecimal format,
            this argument is also used to specify the access size in bytes

        ``timeout``: <int>
            timeout in seconds

        *Further Options*

        ``size``: <int>
            access size in bytes (1, 2, 4, 8) -> if a format is specified,
            this argument is ignored and the data is accessed in the size specified
            by the format

        === Example ===

        | Memory Read    0x0    10

        == Description ==

        Read a range of memory from the target. This requires being connected to
        the XSCT prompt and a target selected. The XSCT command `mrd` is used to
        read a range of memory.

        The output is returned as a list of values in the specified format.
        """

        self._check()

        if not self.connections:
            raise Error("ERROR: No connected devices")

        cmd = "mrd"

        if fmt:
            size = struct.calcsize(fmt)
        else:
            size = int(kwargs.get("size", 4))

        logger.info(f"Reading memory range with size {size}")
        if size not in [1, 2, 4, 8]:
            logger.warn(
                f"Invalid access size {size}, defaulting to 4 (half-word/integer)"
            )
            size = 4

        # convert to letter representation
        size = {1: "b", 2: "h", 4: "w", 8: "d"}[size]
        cmd += f" -size {size}"

        cmd += f" {address} {num}"

        logger.info(f"Reading memory range: {cmd}")
        output = self._client.exec(cmd, timeout=timeout, **kwargs)

        # process output -> convert tcl list into python list
        values = self._process_mrd(output)

        # convert to specified format
        if fmt:
            logger.info(f"Converting values to format {fmt}")
            values = [struct.unpack(fmt, bytes.fromhex(val))[0] for val in values]

        return values

    @staticmethod
    def _process_mrd(output: str):
        """
        Process the mrd output from XSCT

        === Arguments ===

        ``output``: <str>
            output from XSCT targets command

        == Description ==

        Process the mrd output from XSCT. This requires being connected to
        the XSCT prompt.

        Returns a list of values

        """

        res = []

        for line in output.split("\\n"):
            line = line.strip()
            if line:
                _, value = line.split(":")
                value = value.strip()
                res.append(value)

        return res

    @keyword(types={"address": str, "values": List[str], "num": int, "timeout": int})
    def memory_write(
        self, address: str, values: List[str], num: int = 1, timeout: int = 5, **kwargs
    ):
        """
        Write a range of memory with a list of big-endian hexadecimal values

        Write ``<num>`` data values from list of ``<values>`` to active target memory
        address specified by ``<address>``. If ``<num>`` is not specified, all the ``<values>``
        from the list are written sequentially from the address specified by ``<address>``.
        If ``<num>`` is greater than the size of the ``<values>`` list, the last word in the
        list is filled at the remaining address locations.

        https://docs.xilinx.com/r/2022.2-English/ug1400-vitis-embedded/mwr

        === Arguments ===

        ``address``: <str>
            start address of memory range

        ``values``: <List[str]>
            list of values (as strings) to write to memory

        ``num``: <int>
            number of data values to write to the active targets memory

        ``timeout``: <int>
            timeout in seconds

        *Further Options*

        ``size``: <int>
            access size in bytes (1, 2, 4, 8)

        === Example ===

        | Write Memory Ranges    0x0    [0x1, 0x2, 0x3]

        == Description ==

        Write a range of memory to the target. This requires being connected to
        the XSCT prompt and a target selected. The XSCT command `mwr` is used to
        write a range of memory.

        """

        self._check()

        if not self.connections:
            raise Error("ERROR: No connected devices")

        vals = "{" + " ".join(map(str, values)) + "}"

        cmd = "mwr"

        if "size" in kwargs:
            logger.info(f"Writing memory range with size {kwargs['size']}")
            size = int(kwargs["size"])
            if size not in [1, 2, 4, 8]:
                size = 4
            # convert to letter representation
            size = {1: "b", 2: "h", 4: "w", 8: "d"}[size]
            cmd += f" -size {size}"

        cmd += f" {address} {vals}"
        if num != 1:
            cmd += f" {num}"
        print(cmd)

        logger.info(f"Writing memory range: {cmd}")
        self._client.exec(cmd, timeout=timeout)

    @keyword
    def get_target_state(self, timeout=10):
        """
        Get the state of the current target

        Requires being connected to a JTAG device and a target selected.

        Returns the current execution state of the target.
        """

        self._check()

        if not self.connections:
            raise Error("ERROR: No connected devices")

        logger.info("Getting target state")
        return self._client.exec("state", timeout=timeout)

    @keyword
    def get_connected_devices(self) -> list:
        """Get the connected devices"""
        return self.connections

    @keyword(types={"alias": str, "url": str})
    def add_device_alias(self, alias, url):
        """Add a device alias for a given url"""
        self.device_aliases[alias] = url

    @keyword(types={"url": str})
    def get_device_alias(self, url):
        """Get the device alias for a given url"""
        for alias, device_url in self.device_aliases.items():
            if device_url == url:
                return alias
        return None

    @keyword(types={"alias": str})
    def get_device_url(self, alias):
        """Get the device url for a given alias"""
        return self.device_aliases[alias]

    @keyword
    def get_device_aliases(self):
        """Get the device aliases"""
        return self.device_aliases

    @keyword
    def get_device_urls(self):
        """Get the device urls"""
        return list(self.device_aliases.values())

    @keyword(types={"alias": str})
    def remove_device_alias(self, alias):
        """Remove a device alias"""
        del self.device_aliases[alias]

    def _check(self):
        """
        Check if the XSCT server and client are running
        """

        if not self._server.is_alive():
            raise Error("ERROR: XSCT server is not running")
        if not self._client.is_connected():
            raise Error("ERROR: XSCT client is not connected")

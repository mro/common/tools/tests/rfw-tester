*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-12
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Library     tester.Expect    AS    _BDD_Expect


*** Keywords ***
I connect "${command}"
    [Documentation]    Connect using the given connection
    ...    examples:
    ...    I Connect "socat stdio,raw,echo=0 /dev/ttyUSB0,raw,b38400"
    ...    I Connect "${config.command}"
    Wait Until Keyword Succeeds    5x    1s
    ...    _BDD_Expect.Connect    ${command}

I disconnect
    [Documentation]    Disconnect current connection
    _BDD_Expect.Disconnect

I send "${line}"
    [Documentation]    Send a line on the connection
    _BDD_Expect.Sendline    ${line}

I expect "${regex}"
    [Documentation]    Expect the given regular expression on connection
    ${out} =    _BDD_Expect.Expect    ${regex}
    RETURN    ${out}

# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-12-13
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""Client for Xilinx XSCT"""

import socket

from robot.api import logger
from robot.api.deco import keyword, library
from robot.api.exceptions import Error


@library
class XsctClient:
    """
    Xsct Client Library

    This class is used to send commands to the xsdbserver and to receive the
    output.

    Inspired by https://github.com/raczben/pysct/tree/master

    While this is a robot framework library, it is recommended to use the
    `XilinxInterface` library instead.
    """

    def __init__(self, host: str = None, port: int = None):
        """
        Start Xsct

        === Arguments ===

        host: str
            Hostname of the docker container, optional - defaults to localhost

        port: int
            Port to connect to the docker container, required
        """

        self.host = host
        self.port = port

        self._socket = None

        if port is not None:
            self.connect(host, port)

    def __del__(self):
        """
        Close the connection on delete
        """
        if self._socket is not None:
            self.disconnect()

    @keyword
    def is_connected(self):
        """
        Check if the client is connected to the XSDB server

        === Returns ===

        connected: bool
            True if connected, False otherwise
        """

        return self._socket is not None

    @keyword(types={"host": str, "port": int, "timeout": int})
    def connect(self, host=None, port=None, timeout=10):
        """
        Connect to the XSDB server

        === Arguments ===

        host: str
            Hostname of the docker container, optional - defaults to localhost

        port: int
            Port to connect to the docker container, required

        timeout: int
            Timeout in seconds
        """

        if self._socket is not None:
            raise Error("Connection must be closed before starting a new one")

        if host is None:
            host = "localhost"
        self.host = host

        if port is None:
            raise Error("Port must be specified")
        self.port = port

        logger.info(f"XsctClient: connecting to {host}:{port}")
        self._socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self._socket.connect((str(host), int(port)))
        if timeout is not None:
            self._socket.settimeout(timeout)
        logger.info(f"XsctClient: connected to {host}:{port}")

    @keyword
    def disconnect(self):
        """
        Close the connection
        """

        if self._socket is not None:
            try:
                self._socket.close()
                self._socket = None
                logger.info("XsctClient: connection closed")
            except OSError:
                pass

    @keyword(types={"command": str})
    def send(self, command: str):
        """
        Send a command to the XSDB server

        === Arguments ===

        command: str
            Command to send
        """

        if self._socket is None:
            raise Error("Connection must be open before sending a command")

        cmd = command.encode()
        logger.info(f"XsctClient: sending command: {command}")
        self._socket.sendall(cmd)

    @keyword(types={"buffsize": int, "timeout": (int, None)})
    def receive(self, buffsize=1024, timeout=None):
        """
        Receive the output from the XSDB server

        === Returns ===

        output: str | None
            Output from the XSDB server, returns the output or None in case of timeout
        """

        if self._socket is None:
            raise Error("Connection must be open before receiving the output")

        if timeout is not None:
            self._socket.settimeout(timeout)

        ans = ""
        while True:
            try:
                res = self._socket.recv(buffsize)
                logger.info(f"XsctClient: received: {res}")
                ans += res.decode()
                ans = ans.split("\r\n")
                if len(ans) > 1:
                    return ans[0]
            except socket.timeout:
                break

        return None

    @keyword(types={"command": str})
    def exec(self, command: str, **kwargs):
        """
        Send a command to the XSDB server and receive the output

        === Arguments ===

        command: str
            Command to send and execute

        === Returns ===

        output: str
            Output from the XSDB server
        """

        timeout = kwargs.get("timeout", None)
        buffsize = kwargs.get("buffsize", 1024)

        command += "\r\n"
        self.send(command)
        ans = self.receive(buffsize, timeout)
        if ans.startswith("okay"):
            return ans[5:]
        if ans.startswith("error"):
            raise Error(f"ERROR: {ans[6:]}")
        raise Error(f"ERROR: Unexpected answer: {ans}")

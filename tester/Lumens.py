# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-07-03
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""Lumens library for Robot Framework"""

import os
import re
import sys
import shlex
import subprocess

from robot.api import logger
from robot.api.deco import library, keyword
from pexpect.utils import string_types


@library
class Lumens:
    """Controlling Services using Lumens"""

    def __init__(self, lumens_cmd: str = "/usr/local/sbin/lumensctl"):
        self.process = None
        self.command: list
        self.set_lumens_command(lumens_cmd)

    @keyword
    def set_lumens_command(self, lumens_cmd: str) -> None:
        """
        Set the Lumens command

        === Arguments ===

        lumens_cmd: <str>
            The command to use the lumens tool, this needs to include
            accessing the remote host if needed

        Example:

        | Set Lumens Command | ssh -tt hostname /usr/local/sbin/lumensctl |

        """

        if isinstance(lumens_cmd, string_types) and sys.platform != "win32":
            self.command = shlex.split(lumens_cmd, posix=os.name == "posix")
        else:
            self.command = lumens_cmd.split(" ")

    def run_command(self, cmd, encoding="utf-8") -> str:
        """Run a command and check for errors"""

        # we have a colour sender but the remote doesn't support colour and so we get
        # artifacts in the output that mess it up. We need to disable colour output

        logger.debug(f"Running command: {cmd}")
        self.process = subprocess.run(
            cmd,
            check=True,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE,
            encoding=encoding,
        )

        if self.process.returncode != 0:
            raise RuntimeError(f"Failed to run command: {cmd}")

        return self.process.stdout

    @staticmethod
    def remove_ansi(text: str):
        """Remove ANSI escape sequences from a string"""
        ansi_escape = re.compile(r"\x1B(?:[@-Z\\-_]|\[[0-?]*[ -/]*[@-~])")
        return ansi_escape.sub("", text)

    @keyword
    def start_service(self, service: str) -> None:
        """
        Start a service using Lumens

        === Arguments ===

        service: <str>
            The service to start
        """

        logger.debug(f"Starting Service {service} using Lumens")

        cmd = self.make_command(service, "start")
        self.run_command(cmd)

    @keyword
    def stop_service(self, service: str) -> None:
        """
        Stop a service using Lumens

        === Arguments ===

        service: <str>
            The service to stop
        """

        logger.debug(f"Stopping Service {service} using Lumens")

        cmd = self.make_command(service, "stop")
        self.run_command(cmd)

    @keyword
    def restart_service(self, service: str) -> None:
        """
        Restart a service using Lumens

        === Arguments ===

        service: <str>
            The service to restart
        """

        logger.debug(f"Restarting Service {service} using Lumens")

        cmd = self.make_command(service, "restart")
        self.run_command(cmd)

    @keyword
    def service_status(self, service: str) -> str:
        """
        Get the status of a service using Lumens

        === Arguments ===

        service: <str>
            The service to get the status off

        === Returns ===

        status: <str>
            The status of the service
        """

        logger.debug("Stopping FESA class using Lumens")

        cmd = self.make_command(service, "status")
        self.run_command(cmd)

        return self.process.stdout

    @keyword
    def service_is_running(self, service: str) -> bool:
        """
        Check if a service is running using Lumens

        === Arguments ===

        service: <str>
            The service to check if it is running

        === Returns ===

        running: <bool>
            True if the service is running, False otherwise
        """

        if self.command is None:
            raise ValueError("Provide a root command")

        logger.debug("Checking if FESA class is running using Lumens")

        cmd = self.make_command(service, "status")
        self.run_command(cmd)

        return "Active: active" in self.process.stdout

    @keyword
    def list_services(self):
        """
        List all services available using Lumens
        """

        logger.debug("Listing all services using Lumens")

        cmd = [*self.command, "list"]
        output = self.run_command(cmd)
        return self.process_services_list(output)

    def process_services_list(self, output: str):
        """
        Process the services list into a dictionary

        === Arguments ===

        output: <str>
            The list of services to process
        """

        output = self.remove_ansi(output)
        output = output.strip()

        rows = output.split("\n")
        services = {}

        keys = re.split(r"\s{2,}", rows[0])

        for row in rows[1:]:
            columns = row.split()
            zipped = zip(keys, columns)
            tmp = {}
            unit_name = ""
            for k, v in zipped:
                if "NAME" not in k:
                    tmp[k] = v
                else:
                    unit_name = v
            services[unit_name] = tmp

        return services

    def make_command(self, service: str, action: str) -> list:
        """
        Construct a command with a service and an action

        === Arguments ===
        service: <str>
            The service to act on

        action: <str>
            The action to perform, e.g. start, stop, restart

        === Returns ===

        command: <list>
            The command to run as a list of strings
        """

        return [*self.command, "-S", service, "-f", action]

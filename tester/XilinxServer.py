# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-12-13
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""Server for Xilinx XSCT"""

import re
import os
import time
import signal

from collections import namedtuple
from signal import ITIMER_REAL, SIGALRM, setitimer
from signal import signal as create_signal

import psutil
import pexpect

from robot.api import logger
from robot.api.deco import keyword, library
from robot.api.exceptions import Error, Failure

from tester.Shell import Shell
from tester.Expect import Expect

Server = namedtuple("Server", ["hostname", "port"])


@library
class XsctServer(Shell):
    """
    XsctServer class

    This class initialises the docker container (or similar), starts the xsdbserver, and
    manages it.

    Uses pexpect to communicate with the docker container and to start the
    xsdbserver.

    Inspired by https://github.com/raczben/pysct/tree/master

    While this is a library, it is recommended not to use it directly, but to use the
    `XilinxInterface` library instead.
    """

    def __init__(self, command=None, host=None, port=None, prompt=r"xsct% ", **kwargs):
        """
        Start XsctServer

        === Arguments ===

        command: str
            Initial command - recommended to be a command to navigate to the directory
            containing the docker container and to execute `x-builder xsct`

        host: str
            Hostname of the docker container from which the server listens for incoming
            connections

        port: int
            Port to connect to the docker container

        prompt: str
            Prompt to expect from the docker container/xsct

        encoding: str
            Encoding to use for the communication

        """

        keys = ["HOME", "PATH", "PWD"]
        env = dict((k, os.environ[k]) for k in keys if k in os.environ)
        kwargs["env"] = env

        self.prompt = re.compile(prompt) if not hasattr(prompt, "search") else prompt
        self.host = host
        self.port = port
        self.pid = None

        encoding = kwargs.pop("encoding", "utf-8")

        super().__init__(self.prompt, encoding, **kwargs)

        self.xsdbserver = None

        if command and port:
            self.start_server(command, host, port, **kwargs)

    def __del__(self):
        """
        Stop the server on delete
        """

        if self.xsdbserver is not None:
            self.stop_server()

    @keyword
    def is_alive(self):
        """
        Check if the XSDB server is alive
        """

        return self.xsdbserver is not None

    @keyword
    def start_server(
        self, command: str, host: str = None, port: int = None, timeout=30, **kwargs
    ):
        """
        Start the XSDB server

        === Arguments ===

        command: str
            Command to start the docker container and launch xsct
            Of the form `bash -c 'cd /path/to/container; x-builder xsct'`
            Requires that the docker container and x-builder are configured correctly

        host: str
            Hostname from which the server listens for incoming
            connections, if no hostname is specified then the xsdbserver defaults to listening
            on all hostnames ([::]:PORT)

        port: int
            Port to connect to the docker container
        """

        if command is None or port is None:
            raise Error("command and port must be specified")

        # navigate to the directory containing the docker container and launch
        # xsct
        if hasattr(self, "spawn"):
            raise Error("Connection must be closed before starting a new one")

        self.port = port

        # add the port to $OPTS to be exposed -> add to OPTS variable and add
        # that to kwargs[env]
        if "OPTS" not in os.environ:
            os.environ["OPTS"] = f"-p {port}:{port}"
        else:
            os.environ["OPTS"] += f" -p {port}:{port}"

        keys = ["HOME", "PATH", "PWD", "OPTS"]
        kwargs["env"] = dict((k, os.environ[k]) for k in keys if k in os.environ)

        try:
            super().connect(command, **kwargs)
            self.expect(self.prompt, timeout=timeout)
        except pexpect.exceptions.EOF as exc:
            raise Error("ERROR: EOF while starting the Xilinx Environment") from exc

        self.spawn.logfile_send = None

        # start the server
        command = "xsdbserver start"
        if host is not None:
            command += f" -host {host}"
        if port is not None:
            command += f" -port {port}"

        output = r"Connect to this XSDB server use host [\S]+ and port [\d]+"

        try:
            connection = self.execute_command(
                cmdline=command, output=output, timeout=timeout, check=False
            )
        except pexpect.exceptions.EOF as exc:
            raise Error("EOF while starting server") from exc
        connection = connection.group(0)
        logger.info(f"XsctServer: {connection}")

        # extract the hostname and port
        pattern = r"host (\S+) and port (\d+)"
        match = re.search(pattern, connection)
        if match is None:
            raise Error(f"Failed to extract hostname and port from {connection}")
        hostname = match.group(1)
        found_port = int(match.group(2))
        if host is not None and hostname != host:
            raise Error(f"Hostname mismatch: {hostname} != {host}")
        if port is not None and found_port != port:
            raise Error(f"Port mismatch: {found_port} != {port}")

        self.xsdbserver = Server(hostname, port)
        self.pid = self.spawn.pid
        logger.info(f"XsctServer: xsdbserver started with PID: {self.pid}")

    @keyword
    def stop_server(self):
        """
        Stop the XSDB server
        """

        pid = None

        if hasattr(self, "spawn"):
            if self.xsdbserver is None:
                raise Error("Server is not running")

            command = "xsdbserver stop"
            ret = self.execute_command(command, timeout=5, check=False)
            if ret or not ret.isspace():
                logger.info(f"XsctServer: {ret}")
            logger.info("XsctServer: xsdbserver stopped")

            pid = self.spawn.pid
            children = psutil.Process(pid).children(recursive=True)
            for child in reversed(children):
                logger.info(f"XsctServer: Killing PID: {child.pid}")
                os.kill(child.pid, signal.SIGTERM)

            # stop xsct and docker container
            self.spawn.sendline("exit")  # quits xsct
            self.spawn.expect(pexpect.EOF, timeout=5)
            self.spawn.sendline("exit")  # quits docker container

        self.disconnect()
        self.xsdbserver = None

    @keyword(types={"timeout": int})
    def execute_command(self, cmdline, output=None, timeout=5, check=True):
        """
        Send a cmdline to the XSCT

        Wrapper around `Expect.py` execute command method. This method is used
        to execute XSCT commands and works best in the XSCT environment.

        === Arguments ===

        - ``cmdline``: command to execute

        - ``output``: output matching regex

        - ``timeout``: timeout in seconds

        - ``check``: check for command return code

        if output is provided then matching object is returned.

        if output is None (default) then complete output is returned as a string.

        setting ``timeout`` to ``None`` will disable the timeout.

        === Example ===

        | Execute Command    targets

        | Execute Command    target 10

        | Execute Command    mrd 0x0 10

        == Description ==

        Send a command to the XSCT or shell and execute it. If output is provided
        then a matching object is returned. If output is None (default) then the
        complete output is returned as a string.

        If check is True (default) then the return code is checked. If the return
        code is non-zero, or for Xilinx not `NONE` then a Failure is raised.
        """
        ret = None
        buff = None

        if output:
            ret = Expect.execute_command(self, cmdline, output, timeout)
        else:
            # send line
            logger.info(f"Executing XSCT command: {cmdline}")
            self.sendline(cmdline)
            ret = self._readlines(cmdline, timeout=timeout)

            if output and not ret:
                raise Failure(f"Unmatched pattern: {output[:16]}, before: {buff}")

        if check:
            self.expect(self.prompt, timeout=1)
            self.sendline("puts $errorCode")
            try:
                self.expect("NONE", timeout=1)
            except pexpect.TIMEOUT as exc:
                raise Failure("Command returned error") from exc

        return ret

    def _readlines(self, cmd, timeout=5):
        lines = []
        end_time = time.time() + timeout

        cmd_pattern = re.compile(cmd)
        info_pattern = re.compile("Info:")
        invalid_pattern = re.compile("invalid", re.IGNORECASE)

        while True:
            if timeout is not None and timeout < 0:
                return self._timeout("Timeout while reading lines")

            try:
                line = self._interruptable(self.spawn.readline, timeout=5)
            except Exception as e:
                raise e

            if not line or "Timeout" in line:
                break

            if self.prompt.search(line):
                if not cmd_pattern.search(line) and not info_pattern.search(line):
                    logger.debug("Readlines: Prompt found, breaking")
                    break

            if invalid_pattern.search(line):
                raise Failure(f"Error: {cmd} raises {line.strip()}")

            if not cmd_pattern.search(line) and not str(line).isspace():
                lines.append(line)

            if timeout is not None:
                timeout = end_time - time.time()

        return "".join(lines)

    @staticmethod
    def _interruptable(func, args=None, kwargs=None, timeout=120):
        """
        Decorator to interrupt a function
        """

        def handler(signum, frame):
            raise TimeoutError

        try:
            create_signal(SIGALRM, handler)
            setitimer(ITIMER_REAL, timeout)
            return func(*(args or ()), **(kwargs or {}))

        except TimeoutError:
            return "Timeout"

        except Exception as exc:  # pylint: disable=broad-except
            return exc

        finally:
            setitimer(ITIMER_REAL, 0)

    def _timeout(self, err=None):
        self.spawn.after = pexpect.exceptions.TIMEOUT
        raise pexpect.exceptions.TIMEOUT(err)

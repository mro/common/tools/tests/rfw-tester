# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-01-22
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""
GUDE Power Control Module
--------------------------

Robot Framework Library for controlling GUDE Power Distribution Units (PDUs) through the GUDE Expert
Power Controller HTTP Interface.

"""

from enum import IntEnum
from typing import Union

import requests

from robot.api import logger
from robot.api.deco import keyword, library
from robot.api.exceptions import Error
from robot.libraries.BuiltIn import BuiltIn

builtin = BuiltIn()


class Commands(IntEnum):
    """Gude Commands.

    From: https://wiki.gude-systems.com/EPC_HTTP_Interface#CGI_GET_Request
    """

    EASY_SWITCH = 1
    CONFIGURE_OUTPUTS = 3
    CANCEL_BATCH = 2
    START_BATCH = 5
    RESET = 12


class Components(IntEnum):
    """Gude Status Components.

    From: https://wiki.gude-systems.com/EPC_HTTP_Interface#Status_values_from_statusjsn.js
    """

    OUTPUTS = 1
    INPUTS = 2
    DNS_CACHE = 4
    ETHERNET = 8
    MISC = 16
    EVENTS = 128
    PORT_SUMMARY = 256
    HARDWARE = 512
    GSM_STATUS = 1024
    GSM_LOG = 2048
    GSM_COUNTER = 4096
    SIM = 8192
    SENSOR_VALUES = 16384
    SENSOR_DESCR = 65536
    ALL = 1073741823


class GudeBase:
    """base Gude Class, provides an interface between the GUDE Expert Power Control HTTP Interface
    API and a Robot Framework library.

    See https://wiki.gude-systems.com/EPC_HTTP_Interface for more information on the
    GUDE API.
    """

    def __init__(self, url: str = None, username: str = None, password=None) -> None:
        """Initialise the GUDE Base class.

        === Arguments ===

        ``url``: <str>
            (Optional)The URL of the GUDE PDU to connect to. Something like
            ``http://<switch_name/ip>.cern.ch`` where the URL points to the GUDE web interface.
        """

        self._session = requests.Session()
        self.url = None
        self._status_url = None
        self._config_url = None

        if url:
            self.set_device_url(url)

        if username:
            self.set_device_credentials(username, password)

    def get(self, url: str, params: dict = None, timeout: int = 5) -> requests.Response:
        """Make a GET request to the GUDE API."""

        response = self._session.get(url, params=params, timeout=timeout)
        response.raise_for_status()
        return response

    def set_device_url(self, url: str) -> None:
        """Set the device URL.

        === Arguments ===

        ``url``: <str>
            The URL of the GUDE PDU to connect to. Something like
            ``http://<switch_name/ip>.cern.ch`` where the URL points to the GUDE web interface.
        """

        self.url = url
        # The URL to the page returning device status information
        self._status_url = f"{url}/statusjsn.js"
        # The URL to the page returning device configuration information
        self._config_url = f"{url}/cfgjsn.js"

    def set_device_credentials(self, username: str, password: str) -> None:
        """Set the authentication credentials for the GUDE API.

        === Arguments ===

        ``username``: <str>
            The username to use for authentication.

        ``password``: <str>
            The password to use for authentication.
        """

        self._session.auth = (username, password)

    def _check(self):
        """Check if the device URL is set."""

        if self.url is None:
            raise Error("ERROR: No device URL set")

    def switch_output(self, state: bool, port: int = 1, timeout: int = 5):
        """Switch the port specified.

        === Arguments ===

        ``state``: <int>
            The state to switch the port to. True for on, False for off.

        ``port``: <int>
            (Optional) The port number to switch on. Defaults to (Power Port) 1.

        ``timeout``: <int>
            The timeout for the request in seconds.

        Returns a ``requests.Response`` object.
        """

        self._check()

        state = 0 if not state else 1

        cmd = Commands.EASY_SWITCH.value
        parameters = {"cmd": cmd, "p": port, "s": state}

        resp = self.get(self.url, params=parameters, timeout=timeout)
        return resp

    def pulse_output(self, state: bool, duration: int, port: int = 1, timeout: int = 5):
        """Pulse the port specified for a given duration.

        Uses batch mode to pulse the port specified for a given duration.

        === Arguments ===

        ``state``: <int>
            The state to switch the port to. True for on, False for off.

        ``duration``: <int>
            The duration to pulse the port for in seconds. [1...65535]

        ``port``: <int>
            (Optional) The port number to switch on. Defaults to (Power Port) 1.

        ``timeout``: <int>
            The timeout for the request in seconds.

        Uses https://wiki.gude-systems.com/EPC_HTTP_Interface#Start_Batch_mode to pulse the port.
        """

        self._check()

        cmd = Commands.START_BATCH.value
        action1 = 0 if not state else 1
        action2 = 1 if not state else 0
        parameters = {
            "cmd": cmd,
            "p": port,
            "a1": action1,
            "a2": action2,
            "s": duration,
        }

        resp = self.get(self.url, params=parameters, timeout=timeout)
        return resp

    def cancel_pulse(self, port: int = 1, timeout: int = 5):
        """Cancel a batch switch.

        === Arguments ===

        ``port``: <int>
            (Optional) The port number to affect. Defaults to (Power Port) 1.

        ``timeout``: <int>
            The timeout for the request in seconds.

        Returns a ``requests.Response`` object.
        """

        self._check()

        cmd = Commands.CANCEL_BATCH.value
        parameters = {"cmd": cmd, "p": port}

        resp = self.get(self.url, params=parameters, timeout=timeout)
        return resp

    def reset(self, port: int = 1, timeout: int = 5):
        """Reset the device.

        Reset is a preconfigured batch mode, which turns the switch off and then on again
        with a preconfigured power-down phase duration.

        === Arguments ===

        ``port``: <int>
            (Optional) The port number to affect. Defaults to (Power Port) 1.

        ``timeout``: <int>
            The timeout for the request in seconds.

        Returns a ``requests.Response`` object.
        """

        self._check()

        cmd = Commands.RESET.value
        parameters = {"cmd": cmd, "p": port}

        resp = self.get(self.url, params=parameters, timeout=timeout)
        return resp

    def configure_output(self, port: int = 1, **kwargs):
        """Configure Output Properties.

        === Arguments ===

        ``port``: <int>
            (Optional) The number of the output to be configured. Defaults to (Power Port) 1.

        ``kwargs``: key-value pairs for setting the output parameters. Ensure that the key names
                    match exactly the parameter names in the GUDE API.

        Configuration parameters of note:

        | Parameter | Description   | Values        |
        | --------- | ------------- | ------------- |
        | `powup`     | Power-up state of the output. | `0` for off, `1` for on. |
        | `powrem`    | Remember switch status for next device start. | `0` for don't \
        remember, `1` for remember. |
        | `idle`      | Time in seconds to wait after device start before switching on \
        the output. | `[0...N]` |
        | `on_again`  | Amount of waiting time in seconds after switching off one output before \
        automatically switching back on again. | `[0...N]` where `0` means no automatic \
        switch on. |
        | `reset`     | Reset duration - duration of power-down phase in seconds by the `reset` \
        command. | `[1...N]` |

        See https://wiki.gude-systems.com/EPC_HTTP_Interface#Configuration:_Outputs for
        more information.
        """

        self._check()

        cmd = Commands.CONFIGURE_OUTPUTS.value
        parameters = {"cmd": cmd, "p": port}
        parameters.update(kwargs)

        resp = self.get(self._config_url, params=parameters)
        return resp

    def get_status(self, *components: int, timeout: int = 5) -> requests.Response:
        """Get the status of the device.

        === Arguments ===

        ``components``: a number of <int> arguments
            The components to get the status of. These can be either the raw integer values or
            from the ``Components`` enum.

        ``timeout``: <int>
            The timeout for the request in seconds.

        See https://wiki.gude-systems.com/EPC_HTTP_Interface#JSON_Data for more information
        on the components.

        Returns a ``dict`` containing the requested status information.
        """

        self._check()

        comp = sum(components)
        parameters = {"components": comp}

        resp = self.get(self._status_url, params=parameters, timeout=timeout).json()
        return resp


@library
class GudePowerControl(GudeBase):
    r"""GudePowerControl Robot Framework Library

    ``GudePowerControl`` is a Robot Framework Library for controlling GUDE Expert Power
    Control (EPC) units using the GUDE
    [EPC HTTP Interface](https://wiki.gude-systems.com/EPC_HTTP_Interface).

    = Table of Contents =

    %TOC%

    = Usage =

    To import the library into your Robot Framework test suite, add the following line to your
    settings section:

    | Library    tester.GudePowerControl

    """

    ROBOT_LIBRARY_SCOPE = "SUITE"

    def __init__(self, url: str = None, username: str = None, password=None):
        """Initialize GudePowerControl Library

        === Arguments ===

        ``url``: <str>
            (Optional)The URL of the GUDE PDU to connect to. Something like
            ``http://<switch_name/ip>.cern.ch`` where the URL points to the GUDE web interface.
        """

        super().__init__()

        if url:
            self.set_device_url(url)

        self.set_device_credentials(username, password)

    @keyword(types={"url": str})
    def set_device_url(self, url: str) -> None:
        """Set the device URL.

        === Arguments ===

        ``url``: <str>
            The URL of the GUDE PDU to connect to. Something like
            ``http://<switch_name/ip>.cern.ch`` where the URL points to the GUDE web interface.
        """
        super().set_device_url(url)
        logger.info(f"Using GUDE device: {url}")

    @keyword
    def get_device_url(self) -> str:
        """Get the presently set device URL"""
        return self.url

    @keyword(types={"username": str, "password": str})
    def set_device_credentials(
        self, username: str = None, password: str = None
    ) -> None:
        """Set the authentication credentials for the GUDE API.

        === Arguments ===

        ``username``: <str>
            (Optional) The username to use for authentication.

        ``password``: <str>
            (Optional) The password to use for authentication.

        If no username or password is provided, the Robot Framework Suite Variables are searched
        for the following variables:

        | ${GUDE_USERNAME}
        | ${GUDE_PASSWORD}

        And the values of these variables are used for authentication.
        """

        if builtin.robot_running:
            if not username:
                username = builtin.get_variable_value(
                    r"\${GUDE_USERNAME}", default=None
                )
            if not password:
                password = builtin.get_variable_value(
                    r"\${GUDE_PASSWORD}", default=None
                )

        if username:
            super().set_device_credentials(username, password)
            logger.info(f"Using GUDE username: {username}")

    def get(self, url: str, params: dict = None, timeout: int = 5):
        """Send a GET request to the GUDE Network Switch

        === Arguments ===

        ``url``: <str>
            The URL to send the GET request to.

            Example: ``http://<switch_alias>.cern.ch/statusjsn.js``

            Where the `statusjsn.js` is the API endpoint for retrieving the
            status of the GUDE Network Switch as well as sending commands.

            See https://wiki.gude-systems.com/EPC_HTTP_Interface#JSON_Data for
            further information regarding getting the status from the HTTP interface.

        ``params``: <dict>
            A dictionary of parameters to send with the GET request.
            See https://wiki.gude-systems.com/EPC_HTTP_Interface for further information
            regarding the HTTP interface.

            Example: ``{"cmd": 1, "p": 1, "s": 1}``

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request
        """

        logger.info(f"Sending GET request to {url} with params {params}")
        return super().get(url, params, timeout)

    @keyword(types={"port": int, "timeout": int})
    def switch_on(self, port: int = 1, timeout: int = 5):
        """Switch on the GUDE Network Switch

        === Arguments ===

        ``port``: <int>
            (Optional) Number of the Output (power port) to be switched on. Defaults to 1.

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request
        """

        logger.info(f"Switching on port: {port} on device: {self.url}")
        resp = super().switch_output(True, port, timeout)
        return resp

    @keyword(types={"port": int, "timeout": int})
    def switch_off(self, port: int = 1, timeout: int = 5):
        """Switch off the GUDE Network Switch

        === Arguments ===

        ``port``: <int>
            (Optional) Number of the Output (power port) to be switched on. Defaults to 1.

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request
        """

        logger.info(f"Switching off port: {port} on device: {self.url}")
        resp = super().switch_output(False, port, timeout)
        return resp

    @keyword(
        types={
            "state": Union[int, str, bool],
            "duration": int,
            "port": int,
            "timeout": int,
        }
    )
    def pulse_switch(
        self,
        state: Union[int, str, bool],
        duration: int,
        port: int = 1,
        timeout: int = 5,
    ):
        """Pulse the GUDE Network Switch for a given duration.

        Performs a batch (two) switching actions serially with a delay between the two actions.

        === Arguments ===

        ``state``: <str> (on/off) or <int> (1/0) or <bool> (True/False)
            State to pulse the power port output at

        ``duration``: <int>
            Duration in seconds for the pulse. `[1...65535]`

        ``port``: <int>
            (Optional) Number of the Output (power port) to be switched.
            Defaults to (Power Port) 1.

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request
        """

        actions = {0: ("off"), 1: ("on")}

        # Convert state to int
        if isinstance(state, str):
            state = int(state.lower() == "on")
        elif isinstance(state, bool):
            state = int(state)

        logger.info(
            f"Pulsing port: {port} on device: {self.url} \
                    to {actions[state]} for {duration} seconds"
        )

        resp = super().pulse_output(state, duration, port, timeout)
        return resp

    @keyword(types={"port": int, "timeout": int})
    def cancel_pulse(self, port: int = 1, timeout: int = 5):
        """Cancel the batch mode of the GUDE Network Switch

        Canceling the batch mode will stop the GUDE Network Switch from performing the second
        action in the batch mode, the switch remains in the state of the first action.

        === Arguments ===

        ``port``: <int>
            (Optional) Number of the Output (power port) to be switched. Defaults to (Power Port) 1.

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request
        """

        logger.info(f"Cancelling batch mode on port: {port} on device: {self.url}")
        resp = super().cancel_pulse(port, timeout)
        return resp

    @keyword(types={"port": int, "timeout": int})
    def reset_device(self, port: int = 1, timeout: int = 5):
        """Reset the GUDE Network Switch, turns the switch off and then on again
        with a preconfigured power-down phase duration.

        === Arguments ===

        ``port``: <int>
            (Optional) Number of the output (power port) on which a reset should be performed

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request
        """

        logger.info(f"Resetting port: {port} on device: {self.url}")
        resp = super().reset(port, timeout)
        return resp

    @keyword(types={"timeout": int})
    def get_switch_status(self, timeout: int = 5):
        """Get the status of the GUDE Network Switch Device Outputs (power ports)

        === Arguments ===

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request

        Returns a dictionary containing the status of the GUDE Network Switch.
        """

        logger.info(f"Getting status of device: {self.url}")
        resp = super().get_status(Components.OUTPUTS, timeout=timeout)
        status = resp["outputs"]
        return status

    @keyword(types={"port": int, "timeout": int})
    def is_output_on(self, port: int = 1, timeout: int = 5) -> bool:
        """Returns True if a given switch is on, False otherwise.

        === Arguments ===

        ``port``: <int>
            (Optional) Number of the Output (power port) to be checked. Defaults to (Power Port) 1.

        ``timeout``: <int>
            (Optional) The timeout in seconds for the API request
        """

        logger.info(f"Checking if port: {port} on device: {self.url} is on")
        status = self.get_switch_status(timeout=timeout)

        if port > len(status):
            raise ValueError(f"Port {port} does not exist on device {self.url}")

        state = int(status[port - 1]["state"])
        logger.info(f"Port: {port} on device: {self.url} is {'on' if state else 'off'}")

        return state != 0

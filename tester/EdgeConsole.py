# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-03-17T10:01:37
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name
"""Edge library for robotframework"""
import re
import ast
import struct
from functools import reduce
from robot.api.exceptions import Failure
from robot.api.deco import library, keyword, not_keyword
from tester.Expect import Expect


def b2str(b: bytes):
    """Convert a byte sequence to a printable hex string"""
    return "0x" + reduce(lambda a, b: f"{b:02x}" + a, b, "")


@library
class EdgeConsole(Expect):
    """EdgeConsole library provides Edge automation using edgeconsole

    To import the library:

    `Library  tester.EdgeConsole`
    """

    ROBOT_LIBRARY_SCOPE = "SUITE"

    def __init__(self, encoding="utf-8", **kwargs):
        """Import the Shell library"""
        Expect.__init__(self, encoding=encoding, echo=False, env={}, **kwargs)

        self.prompt = re.compile(r"edgeConsole\([^)]*\)>")

    def __del__(self):
        self.disconnect()

    @not_keyword
    def expect(self, *args, **kwargs):
        return Expect.expect(self, *args, **kwargs)

    @not_keyword
    def send(self, *args, **kwargs):
        return Expect.send(self, *args, **kwargs)

    @not_keyword
    def sendline(self, *args, **kwargs):
        return Expect.sendline(self, *args, **kwargs)

    @keyword
    def connect(self, command: str = "edgeconsole", timeout: int = 30, **kwargs):
        """Connect and EdgeConsole shell

        === Arguments ===
        - command: the edgeconsole command
        - timeout: connection timeout value
        """
        Expect.connect(self, command, **kwargs)
        self.expect(self.prompt, timeout=timeout)

    @keyword
    def open_device(self, driver: str, lun: int = 0, path: str = None):
        """Open an Edge device

        === Arguments ===
        - driver: driver to open
        - lun: device number
        - path: path to local EDGE driver dir, if not provided, deployed HW description is used
        """
        if path:
            out = self.execute_command(f"open {driver} {lun} {path}").strip()
        else:
            out = self.execute_command(f"open {driver} {lun}").strip()
        if not out.startswith(f"{driver} driver version") or "failed" in out:
            raise Failure(f"Failed to open device: {out}")

    @keyword
    def read_register(self, register: str, fmt: str = "<I", **kwargs):
        """Read a register

        === Arguments ===
        - register: the register to read
        - from: index of element to read
        - nelt: number of elements to read
        - fmt: python unpacking format
            (see https://docs.python.org/3/library/struct.html#format-strings)
        """
        cmd = f"read {register} --var out"
        if "from" in kwargs:
            cmd += f' --from {kwargs["from"]}'
        if "nelt" in kwargs:
            cmd += f' --nelt {kwargs["nelt"]}'
        out = self.execute_command(cmd)
        if out.strip():
            raise Failure(f"Failed to read register: {out}")
        out = self.execute_command('print([k.tobytes() for k in self.vars["out"]])')
        value = ast.literal_eval(out.strip())
        value = [struct.unpack(fmt, f)[0] for f in value]
        return value[0] if (len(value) == 1) else value

    @keyword
    def write_register(self, register: str, value, fmt: str = "<I", **kwargs):
        """Write to a register

        === Arguments ===
        - register: the register to write
        - value: value or list of values to write
        - from: index of element to read
        - nelt: number of elements to read
        - fmt: python packing format
            (see https://docs.python.org/3/library/struct.html#format-strings)
        """
        cmd = f"write {register}"
        if "from" in kwargs:
            cmd += f' --from {kwargs["from"]}'
        if "nelt" in kwargs:
            cmd += f' --nelt {kwargs["nelt"]}'
        cmd += " "
        if isinstance(value, list):
            cmd += " ".join([b2str(struct.pack(fmt, v)) for v in value])
        elif isinstance(value, str):
            cmd += b2str(struct.pack(fmt, ast.literal_eval(value)))
        else:
            cmd += b2str(struct.pack(fmt, value))

        out = self.execute_command(cmd)
        if out.strip():
            raise Failure(f"Failed to write register: {out}")

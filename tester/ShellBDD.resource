*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-15
# SPDX-FileContributor: Author: Fargier Sylvain <sylvain.fargier@cern.ch>


*** Settings ***
Library     tester.Shell    AS    _BDD_Shell


*** Keywords ***
I start "${command}" shell
    [Documentation]    Start the given shell (command)
    _BDD_Shell.Connect    ${command}

I start a bash shell using "${command}"
    [Documentation]    Start a bash shell using specified command
    I start "${command}" shell
    _BDD_Shell.Sendline    unset PS0
    _BDD_Shell.Sendline    export PS1='prompt$'
    _BDD_Shell.Sendline    unset PS2
    _BDD_Shell.Sendline    unset RPS1
    _BDD_Shell.Set Prompt    prompt\\$
    _BDD_Shell.Sync Prompt

I start a bash shell
    [Documentation]    Start a bash shell
    I start a bash shell using "bash --norc --noprofile"

I close shell
    [Documentation]    Disconnect current connection
    _BDD_Shell.Disconnect

I execute "${command}"
    [Documentation]    Send a line on the connection
    ${timeout} =    Get Variable Value    \${timeout}    5
    ${out} =    _BDD_Shell.Execute Command    ${command}    timeout=${timeout}
    RETURN    ${out}

I set prompt "${regex}"
    [Documentation]    Set current prompt.
    ...
    ...    Note: Set to an empty string to disable prompt synchronization.
    _BDD_Shell.Set Prompt    ${regex}
    _BDD_Shell.Sync Prompt

I send line "${line}"
    [Documentation]    Send raw line
    _BDD_Shell.Sendline    ${line}

I synchronize with prompt
    [Documentation]    Synchronize with prompt to clear input buffer
    ...
    ...    Note: prompt is automatically synchronized when set.
    _BDD_Shell.Sync Prompt

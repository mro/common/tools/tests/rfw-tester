*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-03-19T19:32:35
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Library     tester.EdgeConsole    AS    _Edge
Library     String


*** Variables ***
${EDGECONSOLE_COMMAND} =    edgeconsole
${EDGE_DRIVER} =            fmc_mfe
${EDGE_LUN} =               0


*** Keywords ***
I get EdgeConsole instance
    ${ret} =    Get Library Instance    _Edge
    RETURN    ${ret}

I split register name "${name}"
    [Tags]    robot:private
    ${matches} =    Get Regexp Matches    ${name}    (.*)\\[([0-9]+)\\]    1    2
    IF    ${matches}
        RETURN    ${matches[0]}
    ELSE
        Run Keyword And Return
        ...    Create List    ${name}    0
    END

I connect EdgeConsole
    [Documentation]    Connect EdgeConsole
    ...
    ...    Uses the following Variables:
    ...    - \${EDGECONSOLE_COMMAND} : command to connect to edgeconsole
    ...    - \${EDGE_DRIVER} : the Edge driver to use
    ...    - \${EDGE_LUN} : the Edge device (lun) to connect to
    Wait Until Keyword Succeeds    5x    1s
    ...    _Edge.Connect    ${EDGECONSOLE_COMMAND}
    _Edge.Open Device    ${EDGE_DRIVER}    ${EDGE_LUN}

I disconnect EdgeConsole
    [Documentation]    Close EdgeConsole connection
    _Edge.Disconnect

I read "${name}" register
    [Documentation]    Read and return a register value
    ...    === Arguments ===
    ...    - name: name of the register to read
    ...    === Example ===
    ...    | I read "motion_core.res_mode" register
    ${reg} =    I split register name "${name}"
    ${out} =    _Edge.Read Register    ${reg[0]}    nelt=1    from=${reg[1]}
    RETURN    ${out}

I read all "${name}" registers
    [Documentation]    Read and return one or more registers
    ...
    ...    The number of registers returned depends on the driver
    ...    === Arguments ===
    ...    - name: name of the register to read
    ...    === Example ===
    ...    | I read all "motion_core.res_mode" registers
    ${out} =    _Edge.Read Register    ${name}
    RETURN    ${out}

I read "${name}" register as float
    [Documentation]    Read and return a register
    ...    === Example ===
    ...    | I read "motion_core.res_angle" register as float
    ...    | # equivalent to:
    ...    | I read "motion_core.res_angle" register using "<f" format
    ${reg} =    I split register name "${name}"
    ${out} =    _Edge.Read Register    ${reg[0]}    fmt=<f    nelt=1    from=${reg[1]}
    RETURN    ${out}

I read all "${name}" registers as float
    [Documentation]    Read and return a register
    ${out} =    _Edge.Read Register    ${name}    fmt=<f
    RETURN    ${out}

I read "${name}" register using "${fmt}" format
    [Documentation]    Read and return register using the specified format
    ...    === Arguments ===
    ...    - name: name of the register to read
    ...    - fmt: value packng format, for more details about available formats, see https://docs.python.org/3/library/struct.html#format-strings
    ...    === Example ===
    ...    | I read "motion_core.res_angle" register using "<f" format
    ${reg} =    I split register name "${name}"
    ${out} =    _Edge.Read Register    ${reg[0]}    fmt=${fmt}    nelt=1    from=${reg[1]}
    RETURN    ${out}

I read all "${name}" registers using "${fmt}" format
    [Documentation]    Read and return on or more registers
    ...
    ...    The number of registers returned depends on the driver
    ...    === Arguments ===
    ...    - name: name of the register to read
    ...    - fmt: value packng format, for more details about available formats, see https://docs.python.org/3/library/struct.html#format-strings
    ...    === Example ===
    ...    | I read all "motion_core.res_angle" registers using "<f" format
    ${out} =    _Edge.Read Register    ${name}    fmt=${fmt}
    RETURN    ${out}

register "${name}" value should be "${value}"
    [Documentation]    Compare register values (uint32)
    ...    === Arguments ===
    ...    - name: name of the to read
    ...    - value: value to compare
    ...    === Example ===
    ...    | Then register "motion_core.res_mode" should be "200"
    ${out} =    I read "${name}" register
    Should Be Equal As Integers    ${out}    ${value}
    ...    msg=invalid value for register: ${name}

I write "${value}" to "${name}" register
    [Documentation]    Write given uint32 value to Register
    ${reg} =    I split register name "${name}"
    _Edge.Write Register    ${reg[0]}    ${value}    nelt=1    from=${reg[1]}

I write "${value}" to all "${name}" registers
    [Documentation]    Write given uint32 value to one or more registers
    ...
    ...    The number of registers returned depends on the driver
    _Edge.Write Register    ${name}    ${value}

I write "${value}" to "${name}" register using "${fmt}" format
    [Documentation]    Write to the register using the provided binary format
    ...
    ...    The number of registers returned depends on the driver
    ...    === Arguments ===
    ...    - value: value to write
    ...    - name: name of the register to write
    ...    - fmt: value packng format, for more details about available formats, see https://docs.python.org/3/library/struct.html#format-strings
    ...    === Example ===
    ...    | I write "3.14" to "motion_core.res_angle" register using "<f" format
    ${reg} =    I split register name "${name}"
    _Edge.Write Register    ${reg[0]}    ${value}    fmt=${fmt}    nelt=1    from=${reg[1]}

I write "${value}" to all "${name}" registers using "${fmt}" format
    [Documentation]    Write to the register using the provided binary format
    ...
    ...    The number of registers returned depends on the driver
    ...    === Arguments ===
    ...    - value: value to write
    ...    - name: name of the register to write
    ...    - fmt: value packng format, for more details about available formats, see https://docs.python.org/3/library/struct.html#format-strings
    ...    === Example ===
    ...    | I write "0.00" to all "motion_core.res_angle" registes using "<f" format
    _Edge.Write Register    ${name}    ${value}    fmt=${fmt}    nelt=1    from=0

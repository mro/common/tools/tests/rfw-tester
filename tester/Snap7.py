# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-07-04T11:27:58
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name
"""Snap7 library for robotframework"""

from typing import Union, Dict, Tuple, Callable
from collections import OrderedDict

import re
import socket

from robot.api import logger
from robot.api.exceptions import Failure
from robot.api.deco import library, keyword

try:
    import snap7
except ImportError:
    snap7 = None


def parse_type(s7type: str):
    """parse a type and return a (getter, setter, max_size) tuple"""
    if snap7 is None:
        raise Failure("Snap7 library not available")

    if s7type.startswith("FSTRING"):
        max_size = re.search(r"\d+", s7type)
        if max_size is None:
            raise ValueError(
                "Max size could not be determinate. re.search() returned None"
            )
        max_size = int(max_size[0])
        get_fstring = snap7.util.get_fstring
        set_fstring = snap7.util.set_fstring
        return (
            lambda buffer, position: get_fstring(buffer, position, max_size),
            lambda buffer, position, value: set_fstring(
                buffer, position, value, max_size
            ),
            max_size,
        )
    if s7type.startswith("STRING"):
        max_size = re.search(r"\d+", s7type)
        if max_size is None:
            raise ValueError(
                "Max size could not be determinate. re.search() returned None"
            )
        max_size = int(max_size[0])
        set_string = snap7.util.set_string
        return (
            snap7.util.get_string,
            lambda buffer, position, value: set_string(
                buffer, position, value, max_size
            ),
            max_size + 2,
        )  # First two bytes are max-len and actual-len
    if s7type.startswith("WSTRING"):
        max_size = re.search(r"\d+", s7type)
        if max_size is None:
            raise ValueError(
                "Max size could not be determinate. re.search() returned None"
            )
        max_size = int(max_size[0])
        return (snap7.util.get_wstring, None, max_size * 2 + 4)

    type_info: Dict[str, Tuple[Callable, Union[Callable, None], int]] = {
        "REAL": (snap7.util.get_real, snap7.util.set_real, 4),
        "DWORD": (snap7.util.get_dword, snap7.util.set_dword, 4),
        "DINT": (snap7.util.get_dint, snap7.util.set_dint, 4),
        "INT": (snap7.util.get_int, snap7.util.set_int, 2),
        "WORD": (snap7.util.get_word, snap7.util.set_word, 2),
        "BYTE": (snap7.util.get_byte, snap7.util.get_byte, 1),
        "S5TIME": (snap7.util.get_s5time, None, 2),
        "DATE_AND_TIME": (snap7.util.get_dt, None, 8),
        "USINT": (snap7.util.get_usint, snap7.util.set_usint, 1),
        "SINT": (snap7.util.get_sint, snap7.util.set_sint, 1),
        "TIME": (snap7.util.get_time, snap7.util.set_time, 4),
        "DATE": (snap7.util.get_date, None, 2),
        "TIME_OF_DAY": (snap7.util.get_tod, None, 4),
        "LREAL": (snap7.util.get_lreal, snap7.util.set_lreal, 8),
        "TOD": (snap7.util.get_tod, None, 4),
        "CHAR": (snap7.util.get_char, snap7.util.set_char, 1),
        "WCHAR": (snap7.util.get_wchar, None, 2),
        "DTL": (snap7.util.get_dtl, None, 9),
    }
    if s7type in type_info:
        return type_info[s7type]
    raise ValueError


@library
class Snap7:
    """Snap7 library to access Siemens PLC blocks

    To import the library:

    `Library  tester.Snap7`
    """

    ROBOT_LIBRARY_SCOPE = "SUITE"

    def __init__(self, address: Union[str, None] = None):
        """Import the Snap7 library

        Using `Connect` should be preferred regarding `address` option
        """
        self.layouts = {}
        self.current_db = None
        if address is not None:
            self.connect(address)

    def __del__(self):
        self.disconnect()

    @keyword
    def connect(self, address: str, rack: int = 0, slot: int = 2, port: int = 102):
        """Connects to the PLC

         === Arguments ===
        - address: IPv4 address of the PLC
        - rack: rack number
        - slot: slot number
        - port: TCP communication port
        """
        if snap7 is None:
            raise Failure("Snap7 library not available")
        self.cli = snap7.client.Client()

        try:
            ipaddr = socket.gethostbyname(address)
            self.cli.connect(ipaddr, rack, slot, port)
        except Exception as e:
            raise Failure(f"Failed to connect: {e}") from e

        if not self.cli.get_connected():
            del self.cli
            raise Failure(f"Failed to connect to {address}:{port}")

    @keyword
    def disconnect(self):
        """Disconnect the PLC"""
        if hasattr(self, "cli"):
            self.cli.disconnect()
            del self.cli

    @keyword
    def db_read(self, db_number: int, offset: int, size: int):
        """Raw read the given DB"""
        if not hasattr(self, "cli"):
            raise Failure("PLC not connected")

        return self.cli.db_read(db_number, offset, size)

    @keyword
    def db_write(self, db_number: int, offset: int, data: bytearray):
        """Raw write the given DB"""
        if not hasattr(self, "cli"):
            raise Failure("PLC not connected")

        self.cli.db_write(db_number, offset, data)

    @keyword
    def load_db_layout(self, db_number: int, layout: str):
        """
        Load layout information for a given DB

        === Example Layout ===
        | # Index    Name  Datatype
        | layout=\"\"\"
        | 4     ID         INT
        | 6     NAME	   STRING[6]
        |
        | 12.0  testbool1  BOOL
        | 12.1  testbool2  BOOL
        | 12.2  testbool3  BOOL
        | 12.7  testbool8  BOOL
        | 13    testReal   REAL
        | 17    testDword  DWORD
        | \"\"\"

        Available data-types : `FSTRING`, `STRING`, `WSTRING`, `REAL`, `DWORD`,
            `DINT`, `INT`, `WORD`, `BYTE`, `S5TIME`, `DATE_AND_TIME`, `USINT`,
            `SINT`, `TIME`, `DATE`, `TIME_OF_DAY`, `LREAL`, `TOD`, `CHAR`,
            `WCHAR`, `DTL`
        """
        if snap7 is None:
            raise Failure("Snap7 library not available")
        if not hasattr(self, "cli"):
            raise Failure("PLC not connected")

        self.layouts[db_number] = snap7.util.parse_specification(layout)
        self.set_current_db(db_number)
        logger.info("layout loaded:")
        for name, value in self.layouts[db_number].items():
            logger.info(f"{name} -> {value}")

    @keyword
    def add_to_layout(
        self,
        name: str,
        dataType: str,
        address: Union[str, int],
        db_number: Union[int, None] = None,
    ):
        """
        Add a value to the current layout (creates a new one if none were loaded)
        """
        if not hasattr(self, "cli"):
            raise Failure("PLC not connected")

        if db_number is None:
            db_number = self.current_db
        if db_number is None:
            raise Failure("No db selected")

        if not db_number in self.layouts:
            self.layouts[db_number] = OrderedDict()

        self.layouts[db_number][name] = (address, dataType)

    @keyword
    def set_current_db(self, db_number: int):
        """Set the current DB in use"""
        self.current_db = db_number

    @keyword
    def read_value(self, name: str, db_number: Union[int, None] = None):
        """Read a value from PLC

        === Arguments ===
        - name: value name (as described in layout)
        - db_number: db the value is stored-in (defaults to current db)

        === Example ===
        | Read Value    position
        """
        if snap7 is None:
            raise Failure("Snap7 library not available")
        if not hasattr(self, "cli"):
            raise Failure("PLC not connected")

        if db_number is None:
            db_number = self.current_db
        if db_number is None:
            raise Failure("No db selected")
        if not db_number in self.layouts:
            raise Failure(f"No layout loaded for db {db_number}")

        layout = self.layouts[db_number]

        if not name in layout:
            raise Failure(f"Unknown variable for db {name}")

        byte_index, type_ = layout[name]

        # set parsing non case-sensitive
        type_ = type_.upper()

        if type_ == "BOOL":
            byte_index, bool_index = str(byte_index).split(".")
            bytearray_ = self.db_read(db_number, int(byte_index), 1)
            return snap7.util.get_bool(bytearray_, 0, int(bool_index))

        getter, _, size = parse_type(type_)
        bytearray_ = self.db_read(db_number, int(byte_index), size)
        return getter(bytearray_, 0)

    @keyword
    def write_value(
        self, name: str, value: Union[str, int], db_number: Union[int, None] = None
    ):
        """Write a value to PLC

        === Arguments ===
        - name: value name (as described in layout)
        - value: the value to write
        - db_number: db the value is stored-in (defaults to current db)

        === Example ===
        | Write Value    actualPosition    ${1234}
        """
        if snap7 is None:
            raise Failure("Snap7 library not available")
        if not hasattr(self, "cli"):
            raise Failure("PLC not connected")

        if db_number is None:
            db_number = self.current_db
        if db_number is None:
            raise Failure("No db selected")
        if not db_number in self.layouts:
            raise Failure(f"No layout loaded for db {db_number}")

        layout = self.layouts[db_number]

        if not name in layout:
            raise Failure(f"Unknown variable for db {name}")

        byte_index, type_ = layout[name]

        # set parsing non case-sensitive
        type_ = type_.upper()

        if type_ == "BOOL":
            if not "." in byte_index:
                raise Failure(
                    f'Missing bit offset in register description: "{byte_index} {name}"'
                )
            byte_index, bool_index = str(byte_index).split(".")
            bytearray_ = self.db_read(db_number, int(byte_index), 1)
            snap7.util.set_bool(bytearray_, 0, int(bool_index), bool(value))
            self.db_write(db_number, int(byte_index), bytearray_)
        else:
            _, setter, size = parse_type(type_)
            if setter is None:
                raise Failure(f"Can't set value with type {type_}")
            bytearray_ = bytearray(size)
            setter(bytearray_, 0, value)
            self.db_write(db_number, int(byte_index), bytearray_)

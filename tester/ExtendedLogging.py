# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-05-26T13:43:36
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name

"""Extended Logging module for Robot Framework

Convenience functions to strip and shrink logs
"""

from collections import deque
from functools import partial
from typing import Deque, List, NamedTuple, TypeVar, Optional

import robot
from robot.api import logger
from robot.api.deco import keyword, library, not_keyword
from robot.api.exceptions import Error
from robot.output import LOGGER, Message
from robot.output.logger import Logger

ROBOT_MAJOR_VERSION = int(robot.get_version()[0])


class Keyword(NamedTuple):
    """
    Keyword data object

    Encapsulates a call to a logging method, storing the args,
    kwargs and logging method name for later retrieval
    """

    method_name: str
    args: list
    kwargs: dict


class ELFilter:
    """Extended Logs filter class

    This is the base class for filters implemented here

    It provides base methods that will forward incoming keywords, those methods
    can be overridden
    """

    def __init__(self, parent=None):
        self.next: Optional[ELFilter] = None
        if parent:
            parent.next = self

    def get_method(self, kword: Keyword):
        """return the most specialized method for a given method_name"""

        if ROBOT_MAJOR_VERSION < 7 and self.next:
            kword = self.backwards_compatibility(kword)

        if hasattr(self, kword.method_name):
            # if the filter has a direct override for this method, use it
            method = getattr(self, kword.method_name)
        elif hasattr(self, "generic_keyword"):
            # else if the filter has a generic method, use it
            method = getattr(self, "generic_keyword")
        else:
            # else, propagate to the next filter
            method = self.propagate

        return method

    def propagate(self, kword: Keyword):
        """Propagate a method call to the next filter"""
        if self.next:
            self.next.get_method(kword)(kword)

    @staticmethod
    def backwards_compatibility(kword: Keyword) -> Keyword:
        """Make the keyword compatible with Robot Framework < 7"""

        _start_keyword_methods = {
            "For": "start_for",
            "ForIteration": "start_for_iteration",
            "While": "start_while",
            "WhileIteration": "start_while_iteration",
            "If": "start_if",
            "IfBranch": "start_if_branch",
            "Try": "start_try",
            "TryBranch": "start_try_branch",
            "Return": "start_return",
            "Continue": "start_continue",
            "Break": "start_break",
            "Error": "start_error",
        }
        _end_keyword_methods = {
            "For": "end_for",
            "ForIteration": "end_for_iteration",
            "While": "end_while",
            "WhileIteration": "end_while_iteration",
            "If": "end_if",
            "IfBranch": "end_if_branch",
            "Try": "end_try",
            "TryBranch": "end_try_branch",
            "Return": "end_return",
            "Continue": "end_continue",
            "Break": "end_break",
            "Error": "end_error",
        }

        # amend the keyword object to get the correct method name and args
        if kword.method_name.startswith("start_"):
            name = _start_keyword_methods.get(
                type(kword.args[0].result).__name__, kword.method_name
            )
        elif kword.method_name.startswith("end_"):
            name = _end_keyword_methods.get(
                type(kword.args[0].result).__name__, kword.method_name
            )
        else:
            name = kword.method_name

        kword = Keyword(name, kword.args, kword.kwargs)

        return kword


ELFilterType = TypeVar("ELFilterType", bound=ELFilter)


class ForLoopEvent:
    """Data object that stores a complete For/While loop"""

    keyword: Keyword
    children: List[Keyword]
    iterations: int
    stripped: bool
    last_iteration_idx: int

    def __init__(self, kword: Keyword):
        self.children = []
        self.children.append(kword)
        self.iterations = 0
        self.last_iteration_idx = -1
        self.stripped = False

    def add_iteration(self, kword: Keyword, max_iterations: int):
        """Start a new iteration in the given loop"""
        self.iterations += 1
        if self.iterations >= max_iterations:
            if self.last_iteration_idx >= 0:
                self.children = self.children[: self.last_iteration_idx]
            if not self.stripped:
                # Should recurse
                getattr(LOGGER, "_log_message")(Message("Iterations skipped ..."))
                self.stripped = True
        self.last_iteration_idx = len(self.children)
        self.children.append(kword)

    def take_children(self):
        """Consume and return children LoggerEvents"""
        self.last_iteration_idx = -1
        ret = self.children
        self.children = []
        return ret


class LoopFilter(ELFilter):
    """Loop Filtering class

    This class limits the number of iterations that are displayed in a loop

    The N-1 first iterations are logged (by flowing through), then the last one
    is displayed when the loop returns
    """

    DEFAULT_MAX_ITERATIONS = 20

    _stack: Deque[ForLoopEvent]

    def __init__(self, parent=None):
        super().__init__(parent)
        # For loops stacks, each elements will have a sub-deque per iteration
        self._stack = deque()
        self.max_iterations = LoopFilter.DEFAULT_MAX_ITERATIONS

    def start_for(self, kword: Keyword):
        """Start of `For` keyword event"""
        # Force-use keyword logger (as in output/logger.py `start_body_item` wrapper)
        LOGGER.log_message = getattr(LOGGER, "_log_message")
        if self.max_iterations > 0:
            self._stack.append(ForLoopEvent(kword))
        else:
            super().propagate(kword)

    start_while = start_for

    def end_for(self, kword: Keyword):
        """End of `For` keyword event"""
        if self._stack:
            self.generic_keyword(kword)
            loop = self._stack.pop()
            if self._stack:
                self._stack[-1].children += loop.children
            else:
                for child in loop.children:
                    super().propagate(child)
        else:
            super().propagate(kword)

    end_while = end_for

    def _log_message(self, kword: Keyword):
        """
        Log message (from `_log_message`) event

        `BuiltIn Log` uses the `log_message` method which should be bound to
        the `_log_message` method of the logger.
        """
        if self._stack:
            self._stack[-1].children.append(kword)
        else:
            super().propagate(kword)

    def generic_keyword(self, kword: Keyword):
        """A generic keyword event, acts as a catch-all for any keywords not directly replaced"""
        if self._stack:
            self._stack[-1].children.append(kword)
        else:
            super().propagate(kword)

    def start_for_iteration(self, kword: Keyword):
        """Start of an iteration in a loop"""
        if self._stack:
            self._stack[-1].add_iteration(kword, self.max_iterations)
        else:
            super().propagate(kword)

    start_while_iteration = start_for_iteration

    def end_for_iteration(self, kword: Keyword):
        """End of an iteration in a loop"""
        if self._stack:
            self.generic_keyword(kword)
            loop = self._stack[-1]

            if loop.iterations < self.max_iterations:
                if len(self._stack) >= 2:
                    self._stack[-2].children += loop.take_children()
                else:
                    for child in loop.take_children():
                        super().propagate(child)
        else:
            super().propagate(kword)

    end_while_iteration = end_for_iteration


class LoggerWrapper(ELFilter):
    """Global logger wrapper class"""

    ## Get all start_ and end_ methods in LOGGER
    # _method_names = [name for name in dir(LOGGER) if
    #                  name.startswith("start_") or name.startswith("end_")
    #                  or "message" in name]

    def __init__(self):
        super().__init__()
        self.filter: Optional[ELFilter] = None
        self.loop_filter: LoopFilter = self.add_logger(LoopFilter())

        if ROBOT_MAJOR_VERSION < 7:
            self._method_names = ["start_keyword", "end_keyword", "_log_message"]
        else:
            self._method_names = [
                name
                for name, obj in vars(Logger).items()
                if callable(obj) and "body_item" in str(obj) or "message" in name
            ]

        logger.trace("Extended logs enabled")
        self._refs = 1
        self.direct_methods = {
            method_name: getattr(LOGGER, method_name)
            for method_name in self._method_names
        }

        for method_name in self._method_names:
            intercept = partial(self.intercept, method_name)
            setattr(LOGGER, method_name, intercept)
            resolution = partial(self.direct_call, method_name)
            setattr(self, method_name, resolution)

    def add_logger(self, el_logger: ELFilterType) -> ELFilterType:
        """Add a new `ELFilter` in the chain"""
        if self.filter:
            last = self.filter
            while last.next:
                last = last.next
            last.next = el_logger
        else:
            self.filter = el_logger
        el_logger.next = self
        return el_logger

    def ref(self):
        """Increment refcount on this class"""
        self._refs += 1

    def unref(self):
        """Decrement refcount on this class, eventually release everything"""
        self._refs -= 1
        if self._refs <= 0:
            logger.trace("Extended logs disabled")
            for method_name in self._method_names:
                setattr(LOGGER, method_name, self.direct_methods[method_name])
        return self._refs

    def intercept(self, method_name, *args, **kwargs):
        """Intercept a method call"""
        if self.filter:
            kword = Keyword(method_name, args, kwargs)
            self.filter.get_method(kword)(kword)

    def direct_call(self, method_name, kword: Keyword):
        """Releases the method call such that it goes directly to the logger"""
        if method_name != kword.method_name:
            raise Error("LoggerWrapper released with wrong method name")
        if method_name not in self.direct_methods:
            raise Error("LoggerWrapper released with unknown method name")
        self.direct_methods[method_name](*kword.args, **kword.kwargs)


@library
class ExtendedLogging:
    """Extended Logging library"""

    ROBOT_LIBRARY_SCOPE = "TEST"

    wrapper: Optional[LoggerWrapper] = None

    def __init__(self):
        self._enabled = False

    def __del__(self):
        self._disable_extended_logs()

    @not_keyword
    def _enable_extended_logs(self):
        if not self._enabled:
            self._enabled = True
            if not ExtendedLogging.wrapper:
                ExtendedLogging.wrapper = LoggerWrapper()
            else:
                ExtendedLogging.wrapper.ref()

    @not_keyword
    def _disable_extended_logs(self):
        if self._enabled:
            if not ExtendedLogging.wrapper:
                pass
            elif not ExtendedLogging.wrapper.unref():
                ExtendedLogging.wrapper = None
            else:
                self.set_max_logged_iterations(LoopFilter.DEFAULT_MAX_ITERATIONS)

    @keyword
    def enable_extended_logging(self):
        """Activate extended logging module for the given test"""
        self._enable_extended_logs()
        self.set_max_logged_iterations(LoopFilter.DEFAULT_MAX_ITERATIONS)

    @keyword
    def disable_extended_logging(self):
        """Disable extended logging module for the given test"""
        self._disable_extended_logs()

    @keyword
    def set_max_logged_iterations(self, iterations: int):
        """Set logged iterations limits for For/While loops"""
        self._enable_extended_logs()
        if ExtendedLogging.wrapper:
            ExtendedLogging.wrapper.loop_filter.max_iterations = iterations


if __name__ == "__main__":
    print("Extended Logging")

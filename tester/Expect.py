# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-09
# SPDX-FileContributor: Author: Fargier Sylvain <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name
"""Expect library for robotframework"""
import sys
from typing import Union, Match, Pattern
from pexpect import spawn, TIMEOUT
from robot.api import logger
from robot.api.exceptions import Failure, Error
from robot.api.deco import library, keyword


class _logger:
    """robot replaces sys.stdout with a different object on each run,
    we need a wrapper there"""

    def __init__(self, stdout=True):
        self.stdout = stdout

    def write(self, *args, **kwargs):
        """emulate stdout.write"""
        if self.stdout:
            sys.stdout.write(*args, **kwargs)
        else:
            sys.stderr.write(*args, **kwargs)

    def flush(self):
        """emulate stdout.flush"""
        if self.stdout:
            sys.stdout.flush()
        else:
            sys.stderr.flush()

    def close(self):
        """do nothing"""


@library
class Expect:
    """Expect library provides [https://en.wikipedia.org/wiki/Expect|Expect]
    like primitives on a stream oriented device (console, tcp, serial ...)

    To import the library:

    `Library  tester.Expect`
    """

    ROBOT_LIBRARY_SCOPE = "SUITE"

    def __init__(self, encoding="utf-8", echo=False, command=None, **kwargs):
        """Import the Expect library

        Using `Connect` should be preferred regarding `command` option
        """
        self.conf = kwargs
        # converts byte arrays to strings when reading
        self.conf["encoding"] = encoding
        self.conf["echo"] = echo
        self.conf["delaybeforesend"] = None
        self.conf["delayafterread"] = 0.0001
        self.prompt = kwargs.get("prompt")

        if command is not None:
            self.connect(command)

    def __del__(self):
        self.disconnect()

    @keyword(types={"command": Union[str, object]})
    def connect(self, command, **kwargs):
        """Spawn the given `command` and connect its input/output to Expect"""
        if hasattr(self, "spawn"):
            raise Error("Connection must be disconnected first")

        self.conf.update(kwargs)
        self.conf["command"] = command

        if isinstance(command, str):
            spawn_args = {
                k: v
                for (k, v) in self.conf.items()
                if k in {"command", "timeout", "cwd", "env", "echo", "encoding"}
            }
            logger.debug(f"Connecting {spawn_args}")
            self.spawn = spawn(**spawn_args)

            # Override default delays
            self.spawn.delaybeforesend = self.conf["delaybeforesend"]
            self.spawn.delayafterread = self.conf["delayafterread"]
        else:
            self.spawn = command

        if "logfile" not in self.conf:
            self.conf["logfile"] = "stdout"
        logfile = self.conf["logfile"]
        if logfile == "stdout":
            self.spawn.logfile_read = _logger(True)
        elif logfile == "stderr":
            self.spawn.logfile_read = _logger(False)
        else:
            # pylint: disable=consider-using-with
            self.spawn.logfile_read = open(logfile, "w", encoding="utf-8")
        # Use same logfile for logging data sent to Grbl
        self.spawn.logfile_send = self.spawn.logfile_read
        # Needed to support long commands when retriving echo, commands longer
        # than 4k won't be supported
        self.spawn.setwinsize(4096, 4096)

    @keyword
    def disconnect(self):
        """Disconnect input/output stream, killing the command"""
        if hasattr(self, "spawn"):
            self.spawn.close()
            if self.spawn.logfile_read:
                self.spawn.logfile_read.close()
            del self.spawn

    @keyword
    def send(self, line):
        """Raw send `line` to connected stream"""
        if not hasattr(self, "spawn"):
            raise Error("Not connected")
        self.spawn.send(line)

    @keyword
    def sendline(self, line: str):
        """send `line` to connected stream (appending line-feed)"""
        if not hasattr(self, "spawn"):
            raise Error("Not connected")
        self.spawn.sendline(line)

    @keyword
    def expect(self, regex: Union[str, Pattern], *args, timeout: int = 5, **kwargs):
        """Waiting for the given `regex` on the output stream,
        up to `timeout` seconds"""
        if not hasattr(self, "spawn"):
            raise Error("Not connected")
        try:
            self.spawn.expect(regex, timeout=timeout, *args, **kwargs)
            return self.spawn.match
        except Exception as ex:
            logger.error(str(ex))
            raise Failure(f"Failed to find: {regex}", html=True) from ex

    @keyword
    def execute_command(
        self, cmdline: str, output: str = None, timeout: int = 5
    ) -> Union[str, Match]:
        """Send a command

        === Arguments ===
        - cmdline: command to execute
        - output: output matching regex
        - timeout: timeout in seconds

        if output is provided then matching object is returned.

        if output is None (default) then complete output is returned as a string.
        """
        if self.prompt is None:
            raise Failure("No prompt set, can't send command")
        ret = None
        buff = None

        logger.debug(f"Executing {cmdline}...")
        self.sendline(cmdline)

        if output:
            try:
                self.spawn.expect(output, timeout=timeout)
                ret = self.spawn.match
            except TIMEOUT:
                buff = self.spawn.before
            finally:
                self.spawn.expect(self.prompt, timeout=1)
        else:
            self.spawn.expect(self.prompt, timeout=timeout)
            ret = self.spawn.before
            logger.debug(f"Result {ret}")

        if output and not ret:
            raise Failure(f"Unmatched pattern: {output[:16]}, before: {buff}")
        return ret

    def __repr__(self):
        if "command" in self.conf:
            return f'{type(self).__name__}("{self.conf["command"][:15]}...")'
        return f"{type(self).__name__}()"

# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-23
# SPDX-FileContributor: Author: Fargier Sylvain <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name
"""Sigrok library for robotframework"""
from tempfile import NamedTemporaryFile
import shutil
from enum import Enum
import csv
import re
from subprocess import Popen, DEVNULL, PIPE, TimeoutExpired
from datetime import datetime
from os import dup, fdopen
from robot.api.exceptions import Failure
from robot.api.deco import library, keyword
from robot.api import logger


class Trigger(Enum):
    """Enum class that identifies triggers

    Values are matching Sigrok trigger arguments
    """

    HIGH = 1
    LOW = 0
    RISING = "r"
    FALLING = "f"
    ANY = "e"

    def __str__(self):
        return self.value

    def isLike(self, value):
        """flexible enum comparison"""
        if isinstance(value, self.__class__):
            return value == self
        if isinstance(value, str):
            return str(self.value) == value or self.name.upper() == value.upper()
        return self.value == value

    @classmethod
    def parse(cls, value):
        """parse construct a Trigger object"""
        for t in cls:
            if t.isLike(value):
                return t
        raise Failure(f'unknown trigger type: "{value}"')


class Variance:
    """Class for calculating variance in a single pass (online)
    Using Welford's algorithm

    Based on code found in:
    https://en.wikipedia.org/wiki/Algorithms_for_calculating_variance#Welford's_online_algorithm
    """

    count = 0
    mean = 0
    M2 = 0

    def update(self, value):
        """Update running variance with new value"""
        if self.count == 0:
            self.mean = value
        self.count += 1
        delta = value - self.mean
        self.mean += delta / self.count
        delta2 = value - self.mean
        self.M2 += delta * delta2

    def get_values(self):
        """Get final variance value"""
        if self.count < 2:
            return float("nan")
        return (self.M2 / self.count), self.mean


@library
class Sigrok:
    """Sigrok logical analyzer library

    To import the library:

    `Library  tester.Sigrok`
    """

    # We need to keep multiple attributes
    # pylint: disable=too-many-instance-attributes

    ROBOT_LIBRARY_SCOPE = "SUITE"

    def __init__(self, driver=None, command="sigrok-cli", channels=None, **kwargs):
        """Import the Sigrok library

        Attributes:
        - command: sigrok-cli base command
        - driver: see `sigrok-cli -L` for a list of drivers
        - channels: a signal:name dict, ex: `${{"D0":"test"}}`
        """
        self.set_command(command)
        self.driver = driver
        self.channels = {} if channels is None else channels
        self.config = {}
        if "config" in kwargs:
            self.config.update(kwargs["config"])
        self.out = None
        self.process = None
        self.extraArgs = None
        self.skipSamples = 0

    def __del__(self):
        if self.process is not None:
            self.process.kill()
            self.process.communicate()
        if self.out is not None:
            self.out.close()

    @keyword(types={"command": (str, list)})
    def set_command(self, command):
        """Set the Sigrok command

        Arguments:
        - command: a string or array of string
        """
        self.command = [command] if isinstance(command, str) else command

    @keyword(types={"driver": str})
    def set_driver(self, driver):
        """Set the driver

        Note:
        To list available drivers use `sigrok-cli -L`
        """
        self.driver = driver

    @staticmethod
    def _parse_duration(duration):
        """Parse duration in s or ms from string, return int in ms"""
        match = re.search(r"(\d+) ?(us|ms|s)", duration)
        if match:
            duration_ms = int(match.group(1))
            if match.group(2) is None or match.group(2) == "s":
                duration_ms *= 1000
            elif match.group(2) == "us":
                duration_ms /= 1000
            return duration_ms
        raise Failure(f"Failed to parse duration: {duration}")

    @keyword(types={"samples": int})
    def skip_samples(self, samples):
        """Set samples to skip for acceleration phase

        Arguments:
        - samples: number of samples to skip
        """
        self.skipSamples = samples

    @keyword(types={"duration": str})
    def skip_duration(self, duration):
        """Set duration to skip for acceleration phase

        Arguments:
        - duration: duration string, e.g. 2s or 20ms
        """
        samplerate_khz = self.get_samplerate() / 1000
        self.skipSamples = self._parse_duration(duration) * samplerate_khz

    @keyword(types={"args": (None, list)})
    def set_extra_args(self, args=None):
        """Additional arguments for Sigrok

        Arguments:        - pre_delay: acceleration length to excludeguments or `None` to clear
        """
        self.extraArgs = args

    @keyword
    def clear_channels(self):
        """Clear channels configuration"""
        self.channels = {}

    @keyword(types={"channel": str, "name": (str, None)})
    def set_channel_name(self, channel, name):
        """Set name for the given channel.

        Set it to `None` to disable channel.
        """
        self.channels[channel] = name

    def _get_channel_arg(self):
        ret = ""
        for chan, name in self.channels.items():
            if name is None:
                continue
            if ret:
                ret += ","
            ret += chan if not name else f"{chan}={name}"
        return ret

    @keyword
    def get_samplerate(self):
        """Get sample rate from current out file"""
        self.out.seek(0)
        for line in self.out:
            match = re.search(r"Samplerate: (\d+) (kHz|MHz|GHz)?", line)
            if match:
                samplerate = int(match.group(1))
                if match.group(2) == "kHz":
                    samplerate *= 1000
                elif match.group(2) == "MHz":
                    samplerate *= 1000000
                elif match.group(2) == "GHz":
                    samplerate *= 1000000000
                return samplerate
        raise Failure("Failed to parse samplerate from file")

    @keyword(
        types={
            "time": (int, str, None),
            "samplerate": (str, None),
            "samples": (int, None),
            "trigger": (str, dict, None),
        }
    )
    def record(self, time=None, samplerate=None, samples=None, trigger=None):
        """Start recording.

        Arguments:
        - time : record duration in milliseconds (or as a string, ex: 1s)
        - samplerate : samplerate string with `MHz` or `kHz` suffix
        - samples : number of samples to acquire
        - trigger : comma separated list of triggers,
                    in the form `<channel>=(rising|falling|any|high|low)`,
                    or a dict: `{ "CH1": "r", "CH2": "RISING" }`

        Notes: either time or samples should be provided

        Available samplerates for fx2lafw device driver:
        20 kHz, 25 kHz, 50 kHz
        100 kHz, 200 kHz, 250 kHz, 500 kHz
        1 MHz, 2 MHz, 3 MHz, 4 MHz, 6 MHz, 8 MHz
        12 MHz, 16 MHz, 24 MHz, 48 MHz
        """

        if self.out is not None:
            self.out.close()

        if self.driver is None:
            raise Failure("no driver set, please select a driver")

        args = [
            "--driver",
            self.driver,
            "--output-format",
            f"csv:label={'channel' if self.channels else 'off'}",
        ]
        if self.channels:
            args.extend(["--channels", self._get_channel_arg()])
        if time:
            args.extend(["--time", str(time)])
        if samplerate:
            args.extend(["--config", f"samplerate={samplerate}"])
        if samples:
            args.extend(["--samples", str(samples)])
        if isinstance(trigger, str):
            triggers = []
            for t in trigger.split(","):
                chan, val = t.split("=")
                triggers.append(f"{chan}={Trigger.parse(val).value}")

            args.extend(["--triggers", ",".join(triggers)])
        elif trigger:
            args.extend(
                [
                    "--triggers",
                    ",".join(
                        [f"{k}={Trigger.parse(v).value}" for k, v in trigger.items()]
                    ),
                ]
            )
        if self.extraArgs:
            args.extend(self.extraArgs)

        # pylint: disable=consider-using-with
        self.out = NamedTemporaryFile(
            mode="w+", encoding="utf-8", prefix="sigrok_robot_", delete=False
        )
        cmd = self.command + args
        logger.debug(f'running: "{" ".join(cmd)}"')
        self.process = Popen(
            cmd,
            stdin=DEVNULL,
            stdout=self.out,
            stderr=PIPE,
            close_fds=True,
            encoding="utf-8",
        )

    @keyword(types={"timeout": (int, None)})
    def wait_record(self, timeout=None):
        """Wait for record to finish

        Arguments:
        - timeout: timeout value in seconds
        """

        if self.process is None:
            raise Failure("logical analyzer not running")
        try:
            _, err = self.process.communicate(timeout=timeout)
            if self.process.returncode != 0:
                raise Failure(f"record failed: {str(err)}")
        except TimeoutExpired as exc:
            self.process.kill()
            self.process.communicate()
            raise Failure("logical analyzer timeout") from exc
        finally:
            self.process = None

    @keyword(types={"path": str})
    def save_recording_file_as(self, path=None):
        """Save current recording file"""
        if self.out is None:
            raise Failure("No recording file available for saving")
        if path is None:
            path = "Sigrok_" + datetime.now.strftime("%Y_%m_%d-%H_%M_%S")
        shutil.copy(self.out.name, path)

    @keyword(types={"path": str})
    def load_recording_file(self, path):
        """Load file with recordings"""
        # File needs to stay open even after function exit
        # pylint: disable=consider-using-with
        if not path:
            raise Failure("File path not provided")
        self.out = open(path, "r", encoding="utf-8")
        if self.out is None:
            raise Failure("Could not open file " + path)

    def _iter_channel(self, channel):
        # pylint: disable=too-many-branches
        if not self.out:
            raise Failure("no record")
        if self.process:
            raise Failure("logical analyzer still running: wait for it")

        with fdopen(dup(self.out.fileno())) as localFile:
            localFile.seek(0)

            # Remove comment and initial values
            rfile = filter(lambda row: row[0] != ";", localFile)
            row_name = reader = None
            if self.channels:
                row_name = (
                    self.channels[channel]
                    if channel in self.channels and self.channels[channel]
                    else channel
                )
                reader = csv.DictReader(rfile)
            else:
                row_name = int(channel)
                reader = csv.reader(rfile)

            if row_name is None:
                raise Failure(f'unknown channel "{channel}"')

            try:
                if self.channels:
                    for row in reader:
                        if row_name not in row:
                            raise Failure(
                                f'unknown channel "{channel}" for row: "{row}"'
                            )
                        yield row[row_name]
                else:
                    for row in reader:
                        if len(row) == 1 and ":" in row[0]:
                            # initial analog value
                            continue
                        if len(row) < row_name:
                            raise Failure(
                                f'unknown channel "{channel}" for row: "{row}"'
                            )
                        yield row[row_name]
            except GeneratorExit:
                pass

    @keyword(types={"channel": str, "trigger": (int, str)})
    def get_pulse_count(self, channel, trigger=Trigger.RISING):
        """Get total pulse count

        Arguments:
        - channel: channel name or index (depending on record type)
        - trigger: pulse trigger type: "rising", "falling", "any", "high", "low"
        """
        if Trigger.RISING.isLike(trigger):

            def counter(state, new_state):
                return 1 if (not state and new_state) else 0

        elif Trigger.FALLING.isLike(trigger):

            def counter(state, new_state):
                return 1 if (state and not new_state) else 0

        elif Trigger.ANY.isLike(trigger):

            def counter(state, new_state):
                return 1 if (bool(state) != bool(new_state)) else 0

        elif Trigger.HIGH.isLike(trigger):

            def counter(_, new_state):
                return 1 if new_state else 0

        elif Trigger.LOW.isLike(trigger):

            def counter(_, new_state):
                return 1 if not new_state else 0

        else:
            raise Failure(
                f'invalid trigger value: "{trigger}"'
                + ' must be one of "rising", "falling", "any", "high", "low"'
            )

        count = 0
        state = 0
        for new_state in self._iter_channel(channel):
            new_state = int(new_state)
            count += counter(state, new_state)
            state = new_state
        return count

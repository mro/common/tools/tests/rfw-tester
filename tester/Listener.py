# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-11-22
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""
Listener for Robot Framework Libraries
 - Logs the duration of each keyword
"""

from robot import get_version
from robot.api.deco import keyword

ROBOT_MAJOR_VERSION = int(get_version()[0])

if ROBOT_MAJOR_VERSION < 7:
    from tester.listener_versions.ListenerV2 import ListenerV2 as vListener

    _listener_api_version = 2
else:
    from tester.listener_versions.ListenerV3 import ListenerV3 as vListener

    _listener_api_version = 3


class Listener:
    """
    Listener for Robot Framework Libraries

    The Library runs adjacent to a test suite and hooks onto different Robot Framework events.
    Currently, it only logs the duration of specified keywords.

    When initialising the Listener, passing the names of the desired keywords will cause the
    time elapsed (duration) of the keyword to be logged to the ``KEYWORD_DURATION`` variable if the
    keyword passes.

    Example:

    | ROBOT_LIBRARY_LISTENER = Listener("Keyword1", "Keyword2")

    Where the Keyword name is that presented in Robot Framework, not the python function name.

    The keyword name for a python function ``measure_this`` would be ``Measure This``.

    Alternatively, passing the string ``all`` will cause the duration of all keywords in
    the test suite to be logged to the ``KEYWORD_DURATION`` variable.
    """

    ROBOT_LIBRARY_SCOPE = "SUITE"
    ROBOT_LISTENER_API_VERSION = _listener_api_version

    def __init__(self, *keyword_names) -> None:
        """
        Initialise the Listener

        === Argument Details ===

        ``*keyword_names``: (optional) The names of the keywords to log the duration of.

        === Example ===

        | ROBOT_LIBRARY_LISTENER = Listener("Keyword1", "Keyword2")

        === Note ===

        The keyword names passed to the constructor are optional, rather, it is preferable to
        specify the keywords the listener should listen to in the Robot Framework variable
        ``@{LISTENER_KEYWORDS}``. This variable can be created in the Robot Framework test suite
        under the ``*** Variables ***`` section. The variable should be a list of keywords.

        Additionally, the keyword name ``all`` can be passed to the constructor or added to the
        ``@{LISTENER_KEYWORDS}`` variable to cause the duration of all keywords in the test suite
        to be logged to the ``KEYWORD_DURATION`` variable.
        """

        self.ROBOT_LIBRARY_LISTENER = self
        self.listener = vListener(*keyword_names)

    @keyword
    def update_listener_keywords(self, *keyword_names):
        """
        Update the keywords to be listened to

        === Argument Details ===

        ``*keyword_names``: The names of the keywords to log the duration of.
            - An optional argument, if the keyword is called with no arguments, the listener will
                update the keywords to be listened to from the ``@{LISTENER_KEYWORDS}`` variable.

        === Example ===

        | Update Listener Keywords | Keyword1 | Keyword2

        """
        return self.listener.update_listener_keywords(*keyword_names)

    def start_suite(self, *args, **kwargs):
        """Called when a robot framework suite starts"""
        return self.listener.start_suite(*args, **kwargs)

    def end_keyword(self, *args, **kwargs):
        """Called when a robot framework keyword ends"""
        return self.listener.end_keyword(*args, **kwargs)

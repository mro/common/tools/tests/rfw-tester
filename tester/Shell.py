# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-11
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name
"""Shell library for robotframework"""
import re
from robot.api.exceptions import Failure
from robot.api.deco import library, keyword
from tester.Expect import Expect


@library
class Shell(Expect):
    """Shell library provides Shell automation

    To import the library:

    `Library  tester.Shell`
    """

    ROBOT_LIBRARY_SCOPE = "SUITE"

    def __init__(self, prompt=None, encoding="utf-8", **kwargs):
        """Import the Shell library"""
        Expect.__init__(self, encoding=encoding, echo=False, **kwargs)

        if "env" not in self.conf:
            self.conf["env"] = {"TERM": ""}
        elif "TERM" not in self.conf["env"]:
            self.conf["env"]["TERM"] = ""

        self.prompt = re.compile(prompt) if prompt is not None else None

    def __del__(self):
        self.disconnect()

    @keyword
    def sync_prompt(self):
        """Synchronize output by sending a specific keyword"""
        self.spawn.flush()
        self.execute_command("echo @sync@", "@sync@")

    @keyword
    def set_prompt(self, prompt):
        """Set prompt matching regular-expression"""
        self.prompt = re.compile(prompt) if prompt is not None else None

    @keyword(types={"timeout": int})
    def execute_command(self, cmdline, output=None, timeout=5, check=True):
        """Send a cmdline

        Arguments:
        - cmdline: command to execute
        - output: output matching regex
        - timeout: timeout in seconds
        - check: check for command return code

        if output is provided then matching object is returned.

        if output is None (default) then complete output is returned as a string.
        """
        if self.prompt is None:
            raise Failure("No prompt set, can't send command")

        ret = Expect.execute_command(self, cmdline, output, timeout)

        if check:
            self.sendline('echo "@$?@"')
            result = self.expect("@([0-9]+)@", timeout=1).group(1)
            if result != "0":
                raise Failure(f"Command returned {result}")
            self.expect(self.prompt, timeout=1)

        return ret

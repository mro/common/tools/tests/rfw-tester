# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2024-03-05
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""
Base Class for the Listener Implementation
"""

from robot.api import logger
from robot.libraries.BuiltIn import BuiltIn, RobotNotRunningError


class SilentBuiltIn(BuiltIn):  # pylint: disable=too-many-ancestors
    """BuiltIn class that doesn't log `KEYWORD_DURATION=None` actions"""

    def _log_set_variable(self, name, value):
        if name == r"\${KEYWORD_DURATION}" and value is not None:
            super()._log_set_variable(name, value)


class ListenerBase:
    """
    Base Class for the Listener Implementation
    """

    def __init__(self, *keyword_names) -> None:
        """
        Initialise the Listener

        === Argument Details ===

        ``*keyword_names``: (optional) The names of the keywords to log the duration of.

        === Example ===

        | ROBOT_LIBRARY_LISTENER = Listener("Keyword1", "Keyword2")

        === Note ===

        The keyword names passed to the constructor are optional, rather, it is preferable to
        specify the keywords the listener should listen to in the Robot Framework variable
        ``@{LISTENER_KEYWORDS}``. This variable can be created in the Robot Framework test suite
        under the ``*** Variables ***`` section. The variable should be a list of keywords.

        Additionally, the keyword name ``all`` can be passed to the constructor or added to the
        ``@{LISTENER_KEYWORDS}`` variable to cause the duration of all keywords in the test suite
        to be logged to the ``KEYWORD_DURATION`` variable.
        """

        self._b = SilentBuiltIn()
        self.ROBOT_LIBRARY_LISTENER = self
        self.keywords = None
        self.all_keywords = False

        self.update_listener_keywords(*keyword_names)

    def update_listener_keywords(self, *keyword_names):
        """Update the keywords to be listened to"""

        self.all_keywords = False

        if len(keyword_names) > 0:
            self.keywords = keyword_names
        else:
            # get the listener keywords from the robot framework variable
            try:
                listener_keywords = self._b.get_variable_value(
                    r"\@{LISTENER_KEYWORDS}", None
                )
            except RobotNotRunningError:
                listener_keywords = None

            self.keywords = listener_keywords

        if not self.keywords or None in self.keywords:
            logger.debug("Listener initialised without keywords")
            self.keywords = None
        elif len(self.keywords) == 1:
            logger.debug(f"Listener initialised with keyword {self.keywords[0]}")
            if str(self.keywords[0]).lower().strip() == "all":
                logger.debug("Listener will log all keywords")
                self.all_keywords = True
        else:
            logger.debug(f"Listener initialised with keywords {self.keywords}")

    def start_suite(self, arg1, arg2):
        """
        Called when a robot framework suite starts
        """
        raise NotImplementedError

    def end_keyword(self, arg1, arg2):
        """
        Called when a robot framework keyword ends
        """
        raise NotImplementedError

    def check_keyword(self, name, full_name):
        """
        Check if the keyword is one of the keywords to log
        """

        if self.all_keywords:
            return True
        if self.keywords is None:
            return False
        if name in self.keywords:
            return True
        if full_name in self.keywords:
            return True
        return False

    def log_suite_start(self, name, start_time):
        """Log the start of a suite from the listener"""

        if not self.keywords:
            return

        logger.debug(f"Suite {name} started at {start_time}")
        logger.debug("Listener will log the duration of the following keywords:")
        for keyword in self.keywords:
            logger.debug(f"   - {keyword}")

        self._b.set_suite_variable(r"\${KEYWORD_DURATION}", None)

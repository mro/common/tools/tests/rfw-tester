# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2024-03-04
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""
Listener for Robot Framework Libraries
 - Logs the duration of each keyword
"""

from typing import TYPE_CHECKING

from robot.api import logger

from .ListenerBase import ListenerBase

if TYPE_CHECKING:
    from robot import running, result


class ListenerV3(ListenerBase):
    """
    Listener for Robot Framework Libraries

    Uses Listener Interface V3, recommended for use in Robot Framework Version > 7
    """

    ROBOT_LIBRARY_SCOPE = "SUITE"
    ROBOT_LISTENER_API_VERSION = 3

    def __init__(self, *keyword_names) -> None:
        """
        Initialise the Listener

        === Argument Details ===

        ``*keyword_names``: (optional) The names of the keywords to log the duration of.

        === Example ===

        | ROBOT_LIBRARY_LISTENER = Listener("Keyword1", "Keyword2")

        === Note ===

        The keyword names passed to the constructor are optional, rather, it is preferable to
        specify the keywords the listener should listen to in the Robot Framework variable
        ``@{LISTENER_KEYWORDS}``. This variable can be created in the Robot Framework test suite
        under the ``*** Variables ***`` section. The variable should be a list of keywords.

        Additionally, the keyword name ``all`` can be passed to the constructor or added to the
        ``@{LISTENER_KEYWORDS}`` variable to cause the duration of all keywords in the test suite
        to be logged to the ``KEYWORD_DURATION`` variable.
        """

        logger.debug("Listener (API v3) Initialised")
        super().__init__(*keyword_names)

    def start_suite(self, arg1: "running.TestSuite", arg2: "result.TestSuite"):
        """Called when a robot framework suite starts"""
        self.start_suite_v3(arg1, arg2)

    def end_keyword(self, arg1: "running.Keyword", arg2: "result.Keyword"):
        """Called when a robot framework keyword ends"""
        self.end_keyword_v3(arg1, arg2)

    def start_suite_v3(self, data: "running.TestSuite", result: "result.TestSuite"):
        """Implementation of start_suite for Listener Interface V3"""
        del data  # unused parameter, but required by the API - delete to appease pylint

        self.log_suite_start(result.name, result.start_time)

    def end_keyword_v3(self, data: "running.Keyword", result: "result.Keyword"):
        """
        Implementation of end_keyword for Listener Interface V3

        If the keyword passes and is in the list of keywords to log, the duration of the keyword
        will be logged to the ``KEYWORD_DURATION`` suite variable.
        """

        del data  # unused parameter, but required by the API - delete to appease pylint

        if not self.check_keyword(result.name, result.full_name):
            self._b.set_suite_variable(r"\${KEYWORD_DURATION}", None)
            return

        if result.status == "PASS":
            logger.debug(
                f"Keyword {result.name} passed in {result.elapsed_time} milliseconds"
            )
            self._b.set_suite_variable(r"\${KEYWORD_DURATION}", result.elapsed_time)

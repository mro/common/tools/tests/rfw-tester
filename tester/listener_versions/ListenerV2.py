# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2024-03-04
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""
Listener for Robot Framework Libraries
 - Logs the duration of each keyword
"""

from robot.api import logger
from .ListenerBase import ListenerBase


class ListenerV2(ListenerBase):
    """
    Listener for Robot Framework Libraries

    Uses Listener Interface V2, recommended for use in Robot Framework Version < 7
    """

    ROBOT_LIBRARY_SCOPE = "SUITE"
    ROBOT_LISTENER_API_VERSION = 2

    def __init__(self, *keyword_names) -> None:
        """
        Initialise the Listener

        === Argument Details ===

        ``*keyword_names``: (optional) The names of the keywords to log the duration of.

        === Example ===

        | ROBOT_LIBRARY_LISTENER = Listener("Keyword1", "Keyword2")

        === Note ===

        The keyword names passed to the constructor are optional, rather, it is preferable to
        specify the keywords the listener should listen to in the Robot Framework variable
        ``@{LISTENER_KEYWORDS}``. This variable can be created in the Robot Framework test suite
        under the ``*** Variables ***`` section. The variable should be a list of keywords.

        Additionally, the keyword name ``all`` can be passed to the constructor or added to the
        ``@{LISTENER_KEYWORDS}`` variable to cause the duration of all keywords in the test suite
        to be logged to the ``KEYWORD_DURATION`` variable.
        """

        logger.debug("Listener (API v2) Initialised")
        super().__init__(*keyword_names)

    def start_suite(self, arg1, arg2):
        """Called when a robot framework suite starts"""
        self.start_suite_v2(arg1, arg2)

    def end_keyword(self, arg1, arg2):
        """Called when a robot framework keyword ends"""
        self.end_keyword_v2(arg1, arg2)

    def start_suite_v2(self, name, attrs):
        """Implementation of start_suite for Listener Interface V2"""

        self.log_suite_start(name, attrs["starttime"])

    def end_keyword_v2(self, name, attrs):
        """
        Implementation of end_keyword for Listener Interface V2

        If the keyword passes and is in the list of keywords to log, the duration of the keyword
        will be logged to the ``KEYWORD_DURATION`` suite variable.
        """

        if not self.check_keyword(name, attrs["kwname"]):
            self._b.set_suite_variable(r"\${KEYWORD_DURATION}", None)
            return

        if attrs["status"] == "PASS":
            elapsed = attrs["elapsedtime"]
            logger.debug(f"Keyword {name} passed in {elapsed} milliseconds")
            self._b.set_suite_variable(r"\${KEYWORD_DURATION}", elapsed)

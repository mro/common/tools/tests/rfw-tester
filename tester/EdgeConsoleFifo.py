# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-05-05T14:24:20
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name
"""Fifo support for EdgeConsole objects"""
from pexpect.spawnbase import SpawnBase
from robot.api.exceptions import Error
from robot.api.deco import not_keyword, library, keyword
from tester.EdgeConsole import EdgeConsole
from tester.Expect import Expect


@library
class Connector(Expect):
    """Example integration of EdgeConsoleFifo"""

    @keyword
    def connect_EdgeConsoleFifo(self, edge: EdgeConsole, **kwargs):
        """Spawn the given `command` and connect its input/output to Expect"""
        if hasattr(self, "spawn"):
            raise Error("Connection must be disconnected first")
        return self.connect(EdgeConsoleFifo(edge, **kwargs), **kwargs)


class LogDisable:
    def __init__(self, spawn_base: SpawnBase, disable: bool):
        self.spawn_base = spawn_base
        self.logfile_read = spawn_base.logfile_read
        self.logfile_send = spawn_base.logfile_send
        if disable:
            spawn_base.logfile_read = None
            spawn_base.logfile_send = None

    def __enter__(self):
        pass

    def __exit__(self, *args):
        self.spawn_base.logfile_read = self.logfile_read
        self.spawn_base.logfile_send = self.logfile_send


class EdgeConsoleFifo(SpawnBase):
    """SpawnBase for Edge FIFO registers

    This creates a pexpect like object on the top of an `EdgeConsole`

    === Example ===
    | edgeConsole = EdgeConsole()
    | edgeConsole.connect('ssh ...')
    | mgrbl = Expect()
    | mgrbl.connect(EdgeConsoleFifo(edgeConsole))
    |
    | mgrbl.send('?')
    | mgrbl.expect('Idle')
    """

    def __init__(self, console: EdgeConsole = None, **kwargs):
        SpawnBase.__init__(self, encoding="utf-8", **kwargs)
        self.edge = console
        self.tx_reg = "grbl_fifo.rx_dr"
        self.rx_counter = "grbl_fifo.tx_counter"
        self.rx_reg = "grbl_fifo.tx_dr"
        self.str_last_chars = 100
        self.delayafterread = None
        self.delaybeforesend = None

    @not_keyword
    def read_nonblocking(self, size=1, timeout=None):
        if not self.edge:
            raise Error("No edge connection")
        with LogDisable(self.edge.spawn, self.edge.conf["logfile"] == "stdout"):
            ret = ""
            while True:
                fifo_size = self.edge.read_register(self.rx_counter, fmt="<i")
                if fifo_size > 0:
                    buf = self.edge.read_register(self.rx_reg, fmt="<B", nelt=fifo_size)
                    if type(buf) is int:
                        buf = [buf]
                    ret += self._decoder.decode(bytes(buf), final=False)
                else:
                    break
            if len(ret) > 0:
                self._log(ret, "read")
            return ret

    @not_keyword
    def setwinsize(self, *args, **kwargs):
        """setwinsize stub implementation"""

    def send(self, value):
        """send string to fifo"""

        buffer = [ord(c) for c in value]
        self.edge.write_register(self.tx_reg, buffer, nelt=len(buffer))

    def sendline(self, s=""):
        """sendline implementation (similar to pty_spawn)"""
        s = self._coerce_send_string(s)
        return self.send(s + self.linesep)

    def close(self):
        """close override

        does not close the edge object
        """

    @not_keyword
    def __str__(self):
        """This returns a human-readable string that represents the state of
        the object."""

        s = []
        s.append(repr(self))
        s.append(
            (
                f"buffer (last {self.str_last_chars} chars): "
                f"{repr(self.buffer[-self.str_last_chars :])}"
            )
        )
        s.append(
            (
                f"before (last {self.str_last_chars} chars): "
                f"{repr(self.before[-self.str_last_chars :] if self.before else '')}"  # pylint: disable-all
            )
        )
        s.append(f"after: {repr(self.after)}")
        s.append(f"match: {repr(self.match)}")
        s.append("match_index: " + str(self.match_index))
        s.append("exitstatus: " + str(self.exitstatus))
        return "\n".join(s)

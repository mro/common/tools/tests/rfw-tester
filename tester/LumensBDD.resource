*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-07-03
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Library     tester.Lumens    AS    _Lumens


*** Keywords ***
I set Lumens command to "${cmd}"
    [Documentation]    Set the Lumens command - this is the command to run the
    ...    Lumens binary, e.g. "lumensctl" and may also include an ssh command
    ...    to connect to a remote host and run the Lumens binary there.
    ...    Set the Lumens command
    ...
    ...    === Arguments ===
    ...
    ...    lumens_cmd: <str>
    ...    The command to use the lumens tool, this needs to include
    ...    accessing the remote host if needed
    ...
    ...    Example:
    ...
    ...    | Set Lumens Command | ssh -tt hostname /usr/local/sbin/lumensctl |

    _Lumens.Set Lumens Command    ${cmd}

I start Lumens service "${service}"
    [Documentation]    Start a service using Lumens
    ...
    ...    === Arguments ===
    ...
    ...    service: <str>
    ...    The service to start

    _Lumens.Start Service    service=${service}

I stop Lumens service "${service}"
    [Documentation]    Stop a service using Lumens
    ...
    ...    === Arguments ===
    ...
    ...    service: <str>
    ...    The service to stop

    _Lumens.Stop Service    service=${service}

I restart Lumens service "${service}"
    [Documentation]    Restart a service using Lumens
    ...
    ...    === Arguments ===
    ...
    ...    service: <str>
    ...    The service to restart

    _Lumens.Restart Service    service=${service}

I get Lumens service status "${service}"
    [Documentation]    Get the status of a Lumens service
    ...
    ...    === Arguments ===
    ...
    ...    service: <str>
    ...    The service to get the status off
    ...
    ...    === Returns ===
    ...
    ...    status: <str>
    ...    The status of the service

    ${status} =    _Lumens.Service Status    service=${service}
    RETURN    ${status}

I check if Lumens service "${service}" is running
    [Documentation]    Check if a service is running using Lumens
    ...
    ...    === Arguments ===
    ...
    ...    service: <str>
    ...    The service to check if it is running
    ...
    ...    === Returns ===
    ...
    ...    running: <bool>
    ...    True if the service is running, False otherwise

    ${running} =    _Lumens.Service Is Running    service=${service}
    RETURN    ${running}

I get a list of Lumens services
    [Documentation]    List all services available using Lumens

    ${services} =    _Lumens.List Services
    RETURN    ${services}

# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-10-23
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

# pylint: disable=invalid-name

"""Fesa library for robotframework"""

import re
import math
import json
import time
import itertools
import pexpect

from robot.api import logger
from robot.api.exceptions import Failure
from robot.api.deco import library, keyword
from robot.libraries.BuiltIn import BuiltIn

from tester.Shell import Shell
from tester.Listener import Listener

b = BuiltIn()


class URIParse:
    """
    URI parser class
    ---
    This class is used to parse a URI into its components and aids in command building.
    URI format: ``<device_name>/<property_name>#<field_name>``

    Example:
    ``TestMLStepper/Acquisition#position``

    Device Name is the name of the FESA device, Property Name is the name of the property
    and Field Name is the name of the field. The field name is optional.

    The class has the following properties:
    - ``uri``: the URI
    - ``device_name``: the device name taken from the URI
    - ``property_name``: the property name taken from the URI
    - ``field_name``: the field name taken from the URI
      - ``field_name`` is a list if multiple fields are specified in the URI
    """

    def __init__(self, uri) -> None:
        self._uri = uri
        self._device_name = None
        self._property_name = None
        self._field_name = None
        self._parse_uri()

    def _parse_uri(self):
        components = self.uri.split("/")
        if len(components) >= 2:
            self._device_name = components[0]
            property_components = components[1].split("#")
            if len(property_components) >= 1:
                self._property_name = property_components[0]
                if len(property_components) > 1:
                    if len(property_components[1:]) > 1:
                        self._field_name = property_components[1:]
                    else:
                        self._field_name = property_components[1]

        if self._device_name is None:
            raise Failure("Device name not found in uri")

        if self._property_name is None:
            raise Failure("Property name not found in uri")

    @property
    def uri(self):
        """Get the URI"""
        return self._uri

    @property
    def device_name(self):
        """Get the Device Name"""
        return self._device_name

    @property
    def property_name(self):
        """Get the Property Name"""
        return self._property_name

    @property
    def field_name(self):
        """Get the Field Name"""
        return self._field_name

    def __str__(self) -> str:
        return f"""URI: {self.uri}
        Device Name: {self.device_name}
        Property Name: {self.property_name}
        Field Name: {self.field_name}"""

    def __repr__(self) -> str:
        return f"""URI: {self.uri}
        Device Name: {self.device_name}
        Property Name: {self.property_name}
        Field Name: {self.field_name}"""


@library
class FesaInterface(Shell):
    """
    FESA interface library

    This library is used to build commands for FESA interface and execute them in shell.
    The RDA CMW command line tools are used to communicate with the FESA class.

    To import the library:

    | Library    tester.FesaInterface

    Constructor arguments:
    - ``ssh_command``: `str` - ssh command to connect to the test bench

    The constructor arguments are optional, connection to the test bench can be done using
    the `connect` keyword.

    Where the library is used, the suite variable ``${KEYWORD_DURATION}`` is created and
    after each specified keyword is executed and passed, the duration of the keyword
    is set to the variable.

    The FESA interface offers a number of keywords to interact with FESA properties and fields.
    Using the `set value` keyword, the value of a field can be set. The `wait for value` keyword
    can be used to wait for a field to be a value. The `get value` keyword can be used to get
    the value of a field. The `get info` keyword can be used to get information about a property
    or field.

    """

    ROBOT_LIBRARY_LISTENER = Listener()
    ROBOT_LIBRARY_SCOPE = "SUITE"
    ROBOT_LISTENER_API_VERSION = 2

    def __init__(self, ssh_command=None, **kwargs):
        super().__init__(
            prompt=r"\\[.*\\][\\$#]\s*", encoding="utf-8", command=ssh_command, **kwargs
        )

        if ssh_command is not None:
            self._configure_prompt()
        else:
            logger.info("SSH command not given, call connect to host method")

    @keyword(types={"command": str})
    def connect(self, command, **kwargs):
        super().connect(command, **kwargs)
        self._configure_prompt()

    def _configure_prompt(self):
        self.spawn.sendline("unset PS0")
        self.spawn.sendline("export PS1='prompt$ '")
        self.spawn.sendline("unset PS2")
        self.spawn.sendline("unset RPS1")

        prompt = re.compile("prompt\\$ ")
        self.set_prompt(prompt)
        self.sync_prompt()

    @keyword(types={"uri": str})
    def get_value(self, uri):
        """
        Get the value of a complete property or field(s)

        === Arguments ===
        - ``uri``: the URI of the FESA property or field

          - URI format: ``<device_name>/<property_name>#<field_name>``

          - Where ``<field_name>`` is optional or can be multiple fields separated by ``#``

        If a single field is specified, only the value of the field is returned.

        === Examples ===

        | Get Value    TestMLStepper/Acquisition

        | Get Value    TestMLStepper/Acquisition#position

        | Get Value    TestMLStepper/Acquisition#position#state

        """

        parser = URIParse(uri)

        info = self.get_info(uri)
        logger.info(f"Get Info: {info}")
        res = {key: value["Value"] for key, value in info.items()}

        if isinstance(parser.field_name, str):
            logger.info("Single field provided, returns only the value of the field")
            res = info[parser.field_name]["Value"]
        else:
            logger.info("Multiple fields provided, returns a dict of field_name: value")

        logger.info(f"Get Value: {res}")

        return res

    @keyword(types={"uri": str, "value": (str, int, float, bool)})
    def set_value(self, uri, value):
        """
        Set the value of a FESA field

        === Arguments ===
        - ```uri```: the URI of the FESA field

          - URI format: ``<device_name>/<property_name>#<field_name>``

          - Where ``<field_name>`` is not optional and can only be one field

        - ``value``: the value to set

        === Examples ===

        | Set Value    TestMLStepper/Acquisition#position    100

        """

        parser = URIParse(uri)
        logger.info(f"Setting value of {parser}")

        # get the type of the field
        f_type = self._get_type(
            parser.device_name, parser.property_name, parser.field_name
        )

        if f_type == "bool":
            value = self._handle_bool(value)

        # formulate command
        command = self._build_command(
            "set",
            parser.device_name,
            parser.property_name,
            "-v",
            field_name=parser.field_name,
            type=f_type,
            value=value,
        )

        logger.info(f"Setting value of {parser} to {value}")

        # execute command
        output = self.execute_command(command, check=True)

        logger.debug(output)
        return output

    @keyword(
        types={"uri": str, "value": (str, int, float), "timeout": int, "precision": int}
    )
    def wait_for_value(self, uri, value, timeout: int = 120, precision: int = 3):
        """
        Wait for a FESA field to be a value

        === Arguments ===
        - ``uri``: the URI of the FESA field

            - URI format: ``<device_name>/<property_name>#<field_name>``

            - Where ``<field_name>`` is not optional and can only be one field

        - ``value``: the value to wait for

        - ``timeout``: timeout in seconds

        - ``precision``: number of decimal places to consider in comparison if the value is numeric

        === Examples ===

        | Wait For Value    TestMLStepper/Acquisition#position  100

        | Wait For Value    TestMLStepper/Acquisition#position  100    timeout=60

        | Wait For Value    TestMLStepper/Acquisition#position  100
        | ...    timeout=60
        | ...    precision=3

        """

        parser = URIParse(uri)
        logger.info(f"Waiting for {parser} to be {value}")

        # formulate command - subscribe to the field
        command = self._build_command(
            "subscribe",
            parser.device_name,
            parser.property_name,
            "-v",
            field_name=parser.field_name,
        )

        # get the type of the field
        f_type = self._get_type(
            parser.device_name, parser.property_name, parser.field_name
        )

        # execute command
        self.sync_prompt()
        output = self._execute_until_value(
            command, value, f_type, timeout=timeout, precision=precision
        )

        # type cast
        output = self._cast_type_for_get(output, f_type)

        logger.debug(output)
        return output

    @keyword(types={"uri": str})
    def get_info(self, uri):
        """
        Get information of a FESA property or field

        === Arguments ===
        - ``uri``: the URI of the FESA property or field

            - URI format: ``<device_name>/<property_name>#<field_name>``

            - Where ``<field_name>`` is optional or can be multiple fields separated by ``#``

        === Examples ===

        | Get Info    TestMLStepper/Acquisition

        | Get Info    TestMLStepper/Acquisition#position

        | Get Info    TestMLStepper/Acquisition#position#state

        """

        parser = URIParse(uri)
        logger.info(f"Getting information for {parser}")

        # formulate command
        command = self._build_command(
            keyword_name="get",
            device_name=parser.device_name,
            property_name=parser.property_name,
            field_name=parser.field_name,
        )

        # execute command
        output = self.execute_command(command, check=True)
        # process output
        dicts = self._process_output(output)

        if parser.field_name is not None:
            if isinstance(parser.field_name, list):
                temp_dicts = [
                    d
                    for d in dicts
                    if any(name in d.values() for name in parser.field_name)
                ]
            else:
                temp_dicts = [d for d in dicts if parser.field_name in d.values()]
        else:
            temp_dicts = dicts

        info = {
            d["Name"]: {key: value for key, value in d.items() if key != "Name"}
            for d in temp_dicts
            if d["Name"] is not None
        }

        # cast values to correct type
        for key, value in info.items():
            f_type = value["Type"]
            info[key]["Value"] = self._cast_type_for_get(value["Value"], f_type)

        return info

    def execute_command(self, cmdline, output=None, timeout=5, check=True):
        """
        Wrapper on execute command that checks for RdaException in output
        - Only works when check=False else the error is caught lower in the stack
        """
        output = super().execute_command(cmdline, output, timeout, check)

        if isinstance(output, str):
            if "[RdaException]" in output:
                raise Failure(f"RdaException in output: {output}")

        return output

    @staticmethod
    def _process_output(output):
        # split at ']' character and take the last part, then strip
        parts = str(output).split("]", maxsplit=1)
        if len(parts) > 1:
            data = parts[1].split("\x1b")[0].strip()
        else:
            raise Failure(f"No Field Information found in {output}")
        # split output when there are two successive newlines
        res = re.split("[\n\r]{2}", data)
        # split into sub-lists at each empty string
        sublists = [
            list(y) for x, y in itertools.groupby(res, lambda z: z == "") if not x
        ]
        if len(sublists) == 0:
            raise Failure(f"No Field Information found in {output}")
        # convert in list of dictionaries
        dicts = [
            {
                key.strip(): (
                    json.loads(value.strip())
                    if value.strip().startswith("{") and value.strip().endswith("}")
                    else value.strip()
                )
                for key, value in (item.split(":", maxsplit=1) for item in sublist)
            }
            for sublist in sublists
        ]

        return dicts

    def _execute_until_value(self, command, value, field_type, timeout=120, **kwargs):
        logger.info(f"Expecting value: {value}")

        precision: int = int(kwargs.get("precision", 3))

        ret = None
        buff = None
        is_float = False

        # check if the value is/can be numeric -> may be inputted as a string
        if field_type in ["float", "double"]:
            is_float = True
        elif field_type == "bool":
            # value needs to be the same format as the FESA bool -> False vs
            # false
            logger.info(f"Value {value} is type {type(value)}")
            # handle bool
            value = self._handle_bool(value)
            logger.info(f"Value {value} is now type {type(value)}")

        if is_float:
            # try to convert value to a float
            try:
                value = float(value)
            except ValueError as exc:
                raise Failure(
                    f"Value '{value}' cannot be converted "
                    "to a float - check the type of the value you are waiting for"
                ) from exc

        try:
            self.spawn.sendline(command)

            if not is_float:
                logger.info("Not float - comparing strings with REGEX")
                ret = self._wait_for_non_numeric(value, timeout)
            # using numbers -> floating point comparison/precision, cannot
            # directly match
            else:
                logger.info("Float - comparing floats with given precision")
                ret = self._wait_for_numeric(value, timeout, precision)

        except pexpect.exceptions.TIMEOUT:
            logger.info("Timeout")
            buff = self.spawn.before

        finally:
            logger.info("Stopping Subscribe Process")
            self.spawn.sendcontrol("c")
            self.spawn.sendline("clear")
            self.sync_prompt()

        if ret is None:
            printable_chars = [i for i in buff if i.isprintable()]
            printable_string = "".join(printable_chars)
            raise Failure(
                f"Unmatched pattern: {value[:16] if type(value) not in [float, int] else value}, "
                f"before: {printable_string}"
            )

        return ret

    def _wait_for_non_numeric(self, value, timeout):
        # convert value into regex matching FESA value output
        value_prompt = re.compile(f"(?:^|\\W)(?<!\\S){value}(?=[\\n\\r]+)(?:$|\\W)")
        while True:
            index = self.spawn.expect([value_prompt, self.prompt], timeout=timeout)
            if index == 0:
                logger.info(f"Match! Got {value}")
                return value
            if index == 1:
                logger.info("Prompt Occurred - process ended before finding the value")
                return self._timeout()
            raise Failure("Unexpected output")

    def _wait_for_numeric(self, value, timeout, precision):
        end_time = time.time() + timeout

        while True:
            if timeout is not None and timeout < 0:
                return self._timeout()

            line = self.spawn.readline()
            if line:
                if self._almost_equal_floats(float(line), float(value), precision):
                    logger.info(f"Match! Got {line.strip()} == {value}")
                    return line
            else:
                raise RuntimeError("No line read")

            if timeout is not None:
                timeout = end_time - time.time()

    @staticmethod
    def _almost_equal_floats(value: float, expected: float, precision=3):
        """
        Float almost equal comparison

        === Arguments ===
        - `value`: `float` - value to compare
        - `expected`: `float` - expected value
        - `precision`: `int` - number of decimal places to consider

        === Description ===

        This method is used to compare two float/numeric values: an expected value to wait
        for and the value read from the rda-subscribe command.

        It works by making a series of comparisons:
        - compare the values directly - short circuit in the unlikely
        off chance of being directly equal
        - compare with absolute comparison according to the
        precision given - returns true if within the desired precision
        - see:
        https://randomascii.wordpress.com/2012/02/25/comparing-floating-point-numbers-2012-edition/
          - absolute and relative comparison used to handle values close to zero
        """

        # short circuit if the values are the same
        if value == expected:
            return True

        diff = math.fabs(value - expected)

        if diff < math.pow(10.0, -precision):
            logger.info(
                f"Values are almost equal with difference: {diff}"
                f"(precision = {math.pow(10.0, -precision)})"
            )
            return True

        return False

    def _timeout(self, err=None):
        self.spawn.after = pexpect.exceptions.TIMEOUT
        raise pexpect.exceptions.TIMEOUT(err)

    def _build_command(
        self, keyword_name, device_name, property_name, *args, **kwargs
    ) -> str:
        """
        = Build command from given arguments =

        Requires:
        - keyword_name: `str` - command keyword -> get, set and subscribe
        - device_name: `str` - device name -> TestMLStepper
        - property_name: `str` - property name -> Acquisition, Status, ...

        Optional args:
        - csv - add --csv to the command: output as csv file
        - -v - add -v to the command: display values only

        Optional kwargs:
        - field_name: `str` - field name -> position, state, ...
        - show_value: `bool` - return values only - same as the -v arg but as a kwarg
        - value: `str` - value to set -> only works with set keyword
        - type: `str` - type of the value to set -> only works with set keyword
            - `bool`, `int8`, `int16`, `int32`, `int64`, `float`, `double`, `string`

        == Using the keyword `get` ==

        The get keyword is used to get the value of a property or field.
        The command is built as follows:

        ``rda-get -d <device_name> -p <property_name>``

        This is the simplest form and can be used to get the data from a property.
        If the property has fields, the field name can be specified with the ``-f`` option:

        ``rda-get -d <device_name> -p <property_name> -f <field_name>``

        The output of the above two commands includes header information. To get only the values,
        the ``-v`` option can be used:

        ``rda-get -d <device_name> -p <property_name> -v``

        or

        ``rda-get -d <device_name> -p <property_name> -f <field_name> -v``

        Multiple fields can also be specified, separating them with a comma:

        ``rda-get -d <device_name> -p <property_name> -f <field_name1>,<field_name2> -v``

        To get the output as a csv file, the ``--csv`` option can be used though this
        is not supported in the current version of the library:

        ``rda-get --csv -d <device_name> -p <property_name>``

        The csv file contains all the data for a property and
        the header of information even if the ``-v`` option is used.

        == Using the keyword `set` ==

        The set keyword is used to set the value of a field. The command is built as follows:

        ``rda-set -d <device_name> -p <property_name> -f <field_name> -t <type> -v <value>``

        The ``-t`` option is used to specify the type of the value to set.
        The following types are supported:

        - `bool`, `int8`, `int16`, `int32`, `int64`, `float`, `double`, `string`

        """

        keyword_name = str(keyword_name).lower().strip()
        if keyword_name not in ["get", "set", "subscribe"]:
            raise ValueError(f"Keyword {keyword_name} is not supported")

        if device_name is None:
            raise ValueError("Device is not configured")

        if property_name is None:
            raise ValueError("Property is not configured")

        rda_command = f"rda-{keyword_name} -d {device_name} -p {property_name}"

        field_name = kwargs.get("field_name")
        if field_name:
            if isinstance(kwargs.get("field_name"), list):
                field_name = ",".join(kwargs.get("field_name"))

        if field_name is None:
            if keyword_name == "set":
                raise KeyError("Field name is required for set")
            if keyword_name == "subscribe":
                raise KeyError("Field name is required for subscribe")

        if field_name is not None:
            rda_command += f" -f {field_name}"

        if "set" in keyword_name:
            rda_command = self._handle_set(rda_command, args, kwargs)
        elif "subscribe" in keyword_name:
            rda_command = self._handle_subscribe(args, rda_command)
        else:
            rda_command = self._handle_commands(args, rda_command)

        command = rda_command
        logger.info(f"Built command: {command}")

        return command

    def _get_type(self, device_name, property_name, field_name):
        """
        Get the type of a field

        === Arguments ===
        - `device_name`: `str` - device name -> TestMLStepper

        - `property_name`: `str` - property name -> Acquisition, Status, ...

        - `field_name`: `str` - field name -> position, state, ...

        """

        logger.info(
            f"""Getting Field Type for field
                    {field_name} of property {property_name}"""
        )

        command = self._build_command(
            keyword_name="get",
            device_name=device_name,
            property_name=property_name,
            field_name=field_name,
        )

        # execute command
        output = self.execute_command(command, check=True)

        # process output -> returns a list of dicts
        information = self._process_output(output)
        filtered = next((d for d in information if d["Name"] == field_name), None)
        d_type = filtered["Type"]

        logger.info(f"Field Type: {d_type}")

        return d_type

    @staticmethod
    def _field_information(string):
        """
        Use Regex to get the field information

        === Arguments ===
        - string: `str` - string to search
        """

        pattern = r"(\b[^\s:]+\b)(?:\s*:\s*)(\b[^\s:]+\b)"
        logger.info(f"Searching string: {string} with pattern: {pattern}")

        searcher = re.findall(pattern, string)
        if searcher is None:
            raise ValueError(f"Pattern {pattern} not found in {string}")

        # searcher is a list of tuples, each tuple contains the key
        # and value of the field - convert to dict
        information = dict(searcher)
        return information

    @staticmethod
    def _handle_set(rda_command, args, kwargs):
        keyword_name = rda_command.split()[0].split("-")[1]
        if "-v" not in args:
            logger.console(f"{keyword_name} requires -v to set a value to the field")
            # this is then added automatically hence the log to console, not
            # the error
        if "type" not in kwargs.keys():
            raise KeyError(f"Type required to {keyword_name} value")
        if "value" not in kwargs.keys():
            raise KeyError(f"Value required to {keyword_name}")

        d_type = str(kwargs.get("type")).lower().strip()
        rda_command += f" -t {d_type} -v {kwargs.get('value')}"

        return rda_command

    @staticmethod
    def _handle_subscribe(args, rda_command):
        keyword_name = rda_command.split()[0].split("-")[1]
        if "-v" not in args:
            logger.console(f"{keyword_name} requires -v to get the value of the field")
        rda_command += " -v"

        return rda_command

    @staticmethod
    def _handle_commands(args, rda_command):
        keyword_name = rda_command.split()[0].split("-")[1]
        if "-v" in args:
            logger.info(f"adding -v  for {keyword_name} command, values only")
            rda_command += " -v"

        return rda_command

    @staticmethod
    def _cast_type_for_get(value, f_type):
        valid_types = [
            "bool",
            "int8",
            "int16",
            "int32",
            "int64",
            "float",
            "double",
            "string",
        ]
        if f_type not in valid_types:
            raise ValueError(f"Type {type} is not valid, must be one of {valid_types}")

        if f_type == "bool":
            logger.info(f"Casting {value} to {f_type}")
            value = str(value).lower() == "true"
        elif f_type in ["int8", "int16", "int32", "int64"]:
            logger.info(f"Casting {value} to {f_type} (int))")
            value = int(value)
        elif f_type in ["float", "double"]:
            logger.info(f"Casting {value} to {f_type} (float))")
            value = float(value)
        elif f_type == "string":
            logger.info(f"Casting {value} to {f_type}")
            value = str(value)

        return value

    @staticmethod
    def _handle_bool(value):
        if isinstance(value, bool):
            value = str(value).lower()
            return value
        if isinstance(value, str):
            return value.lower()

        raise ValueError(f"Value {value} is not a valid type for FESA bool")

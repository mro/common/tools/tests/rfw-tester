# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2025 CERN (home.cern)
# SPDX-Created: 2025-02-17T14:30:06
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>

# pylint: disable=invalid-name

"""Loop limiting module for Robot Framework

Simply import this library to ensure no extremely large loops are being
created, prevents machine from going OOM
"""

from functools import wraps
from robot.api.deco import keyword, library
from robot.running.bodyrunner import ForInRangeRunner
from robot.errors import DataError
from robot import utils


def map_values_wrapper(method):
    """Method wrapper for `ForInRangeRunner::_map_values_to_rounds`

    The original method generates arrays with elements, it may quickly wreck the
    test runner by consuming its RAM for mis-constructed loops.

    This wrapper will throw an exception when extra-large loops are detected,
    not even trying to run it.
    """

    def to_number(item):
        if utils.is_number(item):
            return item
        number = eval(str(item), {})  # pylint: disable=eval-used
        if not utils.is_number(number):
            raise TypeError(f"Expected number, got {item}.")
        return number

    @wraps(method)
    def wrapper(self, values, per_round):
        if len(values) == 1:
            size = to_number(values[0])
        elif len(values) == 2:
            size = to_number(values[1]) - to_number(values[0])
        elif len(values) == 3:
            size = (to_number(values[1]) - to_number(values[0])) / to_number(values[2])
        else:
            raise TypeError(f"frange expected 1-3 arguments, got {values}.")
        if (
            ForInRangeWrapper.max_iterations > 0
            and size > ForInRangeWrapper.max_iterations
        ):
            raise DataError(f"FOR IN RANGE too large: {size} elements.")
        return method(self, values, per_round)

    return wrapper


class ForInRangeWrapper(ForInRangeRunner):
    """Wrapper for ForInRangeRunner objects

    Attributes:
        max_iterations  Maximum number of iterations allowed for a "FOR IN RANGE" loop, 0 to disable
    """

    max_iterations = 0
    _original = ForInRangeRunner._map_values_to_rounds

    @staticmethod
    def wrap():
        """Wrap ForInRangeRunner protected methods"""
        # work-around: protected member access
        ForInRangeRunner._map_values_to_rounds = map_values_wrapper(
            ForInRangeRunner._map_values_to_rounds
        )

    @staticmethod
    def unwrap():
        """Unwrap ForInRangeRunner protected methods"""
        ForInRangeRunner._map_values_to_rounds = ForInRangeWrapper._original


@library
class LoopLimiter:
    """Loop limiting library"""

    ROBOT_LIBRARY_SCOPE = "TEST"

    def __init__(self):
        ForInRangeWrapper.wrap()
        ForInRangeWrapper.max_iterations = pow(2, 16)

    def __del__(self):
        ForInRangeWrapper.unwrap()

    @keyword
    def set_max_loop_iterations(self, iterations: int):
        """Set maximum iterations for a loop

        Only applies the limitation to `FOR IN RANGE` loops
        """
        ForInRangeWrapper.max_iterations = iterations

    @keyword
    def get_max_loop_iterations(self):
        """Get maximum iterations for a loop"""
        return ForInRangeWrapper.max_iterations

# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-11-22
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>

import sys
import pytest
from tester.FesaInterface import FesaInterface, URIParse

# run with `python3 -m pytest tests/TestFesaInterface.py`


class TestUriParse:
    def test_device_property_uri(self):
        parsed = URIParse("device/property")
        assert parsed.device_name == "device"
        assert parsed.property_name == "property"

    def test_single_field_uri(self):
        parsed = URIParse("device/property#field")
        assert parsed.device_name == "device"
        assert parsed.property_name == "property"
        assert parsed.field_name == "field"

    def test_multiple_fields_uri(self):
        parsed = URIParse("device/property#field1#field2")
        assert parsed.device_name == "device"
        assert parsed.property_name == "property"
        assert parsed.field_name == ["field1", "field2"]


class TestFesaInterface:
    def test_basic_float_comparison(self):
        res = FesaInterface._almost_equal_floats(1, 2, 3)
        assert res == False

        res = FesaInterface._almost_equal_floats(112367, 112366, 3)
        assert res == False

        res = FesaInterface._almost_equal_floats(1.0, 1.0000, 3)
        assert res == True

        res = FesaInterface._almost_equal_floats(1.0, 1.0001, 3)
        assert res == True

        res = FesaInterface._almost_equal_floats(1.0, 1.0010, 3)
        assert res == False

        res = FesaInterface._almost_equal_floats(1.0, 1.0100, 3)
        assert res == False

    def test_new_precision_float_comparison(self):
        res = FesaInterface._almost_equal_floats(1.0, 1.0000, 1)
        assert res == True

        res = FesaInterface._almost_equal_floats(1.0, 1.01, 1)
        assert res == True

        res = FesaInterface._almost_equal_floats(1.0, 1.1, 1)
        assert res == False

    def test_zero_float_comparison(self):
        res = FesaInterface._almost_equal_floats(0.0, 0.0, 3)
        assert res == True

        res = FesaInterface._almost_equal_floats(0.0, 0.0001, 3)
        assert res == True

        res = FesaInterface._almost_equal_floats(0.0, 0.0010, 3)
        assert res == False

    def test_large_float_comparison(self):
        res = FesaInterface._almost_equal_floats(1000000.0, 1000000.0, 3)
        assert res == True

        res = FesaInterface._almost_equal_floats(1000000.0, 1000000.0001, 3)
        assert res == True

        res = FesaInterface._almost_equal_floats(1000000.0, 1000000.0010, 3)
        assert res == False

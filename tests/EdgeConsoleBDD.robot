*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-03-19T20:01:22
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Resource            ${EXECDIR}/tester/EdgeConsoleBDD.resource
Variables           ${CURDIR}/config.yaml

Test Teardown       I disconnect EdgeConsole

# This test requires functional hardware to run
Test Tags           robot:skip


*** Variables ***
${EDGECONSOLE_COMMAND} =    ssh -t cfn-937-sam1.cern.ch edgeconsole
${EDGE_DRIVER} =            fmc_mfe
${EDGE_LUN} =               0


*** Test Cases ***
Basic BDD Connection
    Given I connect EdgeConsole
    When I write "200" to "motion_core.res_mode" register
    Then register "motion_core.res_mode" value should be "200"

    When I write "400" to "motion_core.res_mode[1]" register
    Then register "motion_core.res_mode[1]" value should be "400"

    ${value} =    When I read "motion_core.res_mode" register
    Then Should Be Equal As Integers    ${value}    200

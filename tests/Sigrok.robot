*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-23
# SPDX-FileContributor: Author: Fargier Sylvain <sylvain.fargier@cern.ch>


*** Settings ***
Library             OperatingSystem
Library             tester.Sigrok    driver=demo

Test Teardown       Reset Sigrok


*** Test Cases ***
Basic Record
    [Tags]    sigrok-cli    sigrok-cli-0.7.2
    Set Extra Args    [ '-c', 'channel_group=Logic:pattern=incremental' ]
    Record    samples=100
    Wait Record    timeout=100
    ${pulses} =    Get Pulse Count    0
    Should Be Equal As Integers    50    ${pulses}

Named Channel Record
    [Tags]    sigrok-cli    sigrok-cli-0.7.2
    Set Channel Name    D0    test
    Set Channel Name    D1    test2
    Set Extra Args    [ '-c', 'channel_group=Logic:pattern=incremental' ]
    Record    samples=100
    Wait Record    timeout=100
    ${pulses} =    Get Pulse Count    test
    Should Be Equal As Integers    50    ${pulses}

Triggered Record
    [Tags]    sigrok-cli    sigrok-cli-0.7.2
    Set Channel Name    D0    test
    Set Extra Args    [ '-c', 'channel_group=Logic:pattern=incremental' ]
    Record    time=100    samplerate=100MHz    trigger=test=r
    Wait Record    timeout=100
    ${pulses} =    Get Pulse Count    test
    Should Be True    ${pulses} > 0

Triggered Record Using Object
    [Tags]    sigrok-cli    sigrok-cli-0.7.2
    Set Channel Name    D0    test
    Set Extra Args    [ '-c', 'channel_group=Logic:pattern=incremental' ]
    Record    time=100    samplerate=100MHz    trigger=${{{ 'test': 'r' }}}
    Wait Record    timeout=100
    ${pulses} =    Get Pulse Count    test
    Should Be True    ${pulses} > 0

Named Triggers
    [Tags]    sigrok-cli    sigrok-cli-0.7.2
    Set Channel Name    D0    test
    Set Extra Args    [ '-c', 'channel_group=Logic:pattern=incremental' ]
    Record    time=100    samplerate=100MHz    trigger=${{{ 'test': tester.Sigrok.Trigger.FALLING }}}
    Wait Record

    Record    time=100    samplerate=100MHz    trigger=test=rising
    Wait Record

    Record    samples=100
    Wait Record
    ${pulses} =    Get Pulse Count    test    trigger=rising
    Should Be Equal As Integers    50    ${pulses}
    ${pulses} =    Get Pulse Count    test    trigger=any
    Should Be Equal As Integers    99    ${pulses}

File Save Load
    [Tags]    sigrok-cli    sigrok-cli-0.7.2
    Set Channel Name    D0    test
    Set Extra Args    [ '-c', 'channel_group=Logic:pattern=incremental' ]

    Record    samples=100
    Wait Record
    Save Recording File As    TestRecFile

    Record    samples=200
    Wait Record
    ${pulses} =    Get Pulse Count    test    trigger=any
    Should Be Equal As Integers    199    ${pulses}

    Load Recording File    TestRecFile
    ${pulses} =    Get Pulse Count    test    trigger=any
    Should Be Equal As Integers    99    ${pulses}

    Remove File    TestRecFile

Invalid Driver
    [Tags]    sigrok-cli
    Set Driver    invalid
    Record    samples=100
    Run Keyword And Expect Error    STARTS:record failed: Driver invalid not found
    ...    Wait Record

Invalid Channel Name
    [Tags]    sigrok-cli
    Set Channel Name    XX    test
    Record    samples=100
    Run Keyword And Expect Error    STARTS:record failed: unknown channel
    ...    Wait Record

Samplerate
    Load Recording File    tests/sample_files/SampleRecFile2GHz
    ${samplerate} =    Get Samplerate
    Should Be Equal As Numbers    2 000 000 000    ${samplerate}

    Load Recording File    tests/sample_files/SampleRecFile210MHz
    ${samplerate} =    Get Samplerate
    Should Be Equal As Numbers    210 000 000    ${samplerate}


*** Keywords ***
Reset Sigrok
    Set Extra Args    @{EMPTY}
    Clear Channels
    Set Command    sigrok-cli
    Set Driver    demo

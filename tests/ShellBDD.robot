*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-15
# SPDX-FileContributor: Author: Fargier Sylvain <sylvain.fargier@cern.ch>


*** Settings ***
Resource            ${EXECDIR}/tester/ShellBDD.resource
Variables           ${CURDIR}/config.yaml

Test Teardown       I close shell


*** Test Cases ***
Shell Simple
    Given I start a bash shell
    ${out} =    When I execute "echo world"
    Should Be Equal    world    ${out.strip()}

Shell Custom Command
    Given I start "sh" shell
    And I send line "export PS1='$ '"
    And I set prompt "\\$ "
    ${out} =    When I execute "echo world"
    Then Should Be Equal    world    ${out.strip()}

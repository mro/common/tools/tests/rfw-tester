*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2024-01-23
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Testing the remote control of the Gude Power Control device

Library             tester.GudePowerControl    AS    Switch

Test Setup          Switch.Set Device Url    ${URL}

Test Tags           robot:skip


*** Variables ***
${URL}      http://cfs-937-sam2.cern.ch


*** Test Cases ***
I can get the switch status
    ${status} =    Switch.Get Switch Status
    Log    ${status}

I can get the switch state
    ${state} =    Switch.Is Output On
    Log    ${state}

I can turn the switch on
    Given Switch.Switch On
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${True}

I can turn the switch off
    Given Switch.Switch Off
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${False}

I can pulse the switch for a given duration
    Given Switch.Pulse Switch    On    10
    Sleep    5s
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${True}
    Sleep    10s
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${False}

I can cancel as batch instruction
    Given Switch.Pulse Switch    On    10
    Sleep    5s
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${True}
    Then Switch.Cancel Pulse
    Sleep    10s
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${True}

I can reset the switch device
    Given Switch.Reset Device
    Sleep    5s
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${False}
    Sleep    30s    # sleep for the reset time specified in the device configuration
    ${state} =    Switch.Is Output On
    Then Should Be True    ${state} is ${True}

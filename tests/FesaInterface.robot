*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-10-25
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Test the FESA interface library: requires a FESA class and some hardware/simulation

Library             Collections
Library             tester.FesaInterface    AS    fesa_interface

Suite Setup         fesa_interface.Connect    ${SSH_COMMAND}
Suite Teardown      fesa_interface.Disconnect

# Test requires tn access to run
Test Tags           robot:skip


*** Variables ***
${hostname} =               cwe-513-vpl818
${SSH_COMMAND} =            ssh ${hostname}
@{LISTENER_KEYWORDS} =      Wait For Value    fesa_interface.Set Value    Get Info


*** Test Cases ***
Get Values
    [Documentation]    Test Getting values from the Fesa interface
    ${values} =    fesa_interface.Get Value    uri=TestMLStepper/Acquisition
    Dictionary Should Contain Key    ${values}    position

    # check that the values are typed - not just strings
    @{vals} =    Get Dictionary Values    ${values}
    FOR    ${val}    IN    @{vals}
        Run Keyword And Continue On Failure
        ...    Evaluate    print((type(${val})))
    END

    ${values} =    fesa_interface.Get Value    uri=TestMLStepper/Acquisition#position
    Evaluate    (type(${values})) == float

    ${values} =    fesa_interface.Get Value    uri=TestMLStepper/Acquisition#position#axisState#switch_minus
    Dictionary Should Contain Key    ${values}    position
    Dictionary Should Contain Key    ${values}    axisState
    Dictionary Should Contain Key    ${values}    switch_minus
    Dictionary Should Not Contain Key    ${values}    switch_plus
    Log Dictionary    dictionary=${values}

Set Values
    [Documentation]    Test Setting values from the Fesa interface
    fesa_interface.Set Value    uri=TestMLStepper/AbsolutePosition#position    value=0
    fesa_interface.Set Value    uri=TestMLStepper/RelativePosition#position    value=1

    # need more tests here for setting values of different types

Wait for Values
    [Documentation]    Test Waiting for values from the Fesa interface
    [Setup]    fesa_interface.Set Value    uri=TestMLStepper/AbsolutePosition#position    value=0

    Sleep    5s    # wait for the setup move to complete

    # test with expecting an int when the tpye
    ${to_move} =    Set Variable    10
    fesa_interface.Set Value    uri=TestMLStepper/AbsolutePosition#position    value=${to_move}
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds
    fesa_interface.Wait For Value    uri=TestMLStepper/Acquisition#controllerPosition    value=${to_move}
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    ${to_move} =    Set Variable    30.275
    fesa_interface.Set Value    uri=TestMLStepper/AbsolutePosition#position    value=${to_move}
    ${moved_to} =    fesa_interface.Wait For Value
    ...    uri=TestMLStepper/Acquisition#controllerPosition
    ...    value=${to_move}
    Evaluate    type(${moved_to}) == float
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    Run Keyword And Expect Error
    ...    STARTS: Unmatched
    ...    fesa_interface.Wait For Value
    ...    uri=TestMLStepper/Acquisition#controllerPosition
    ...    value=30.277    timeout=3

    fesa_interface.Wait For Value
    ...    uri=TestMLStepper/Acquisition#controllerPosition
    ...    value=30.27    precision=2

    fesa_interface.Wait For Value
    ...    uri=TestMLStepper/Acquisition#controllerPosition
    ...    value=30.2    precision=1

    fesa_interface.Wait For Value
    ...    uri=TestMLStepper/Acquisition#controllerPosition
    ...    value=30    precision=0

    fesa_interface.Wait For Value
    ...    uri=TestMLStepper/Acquisition#controllerPosition
    ...    value=40    precision=-1

    Run Keyword And Expect Error    STARTS: Value
    ...    fesa_interface.Wait For Value    uri=TestMLStepper/Acquisition#controllerPosition    value=string
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    fesa_interface.Wait For Value    uri=TestMLStepper/Acquisition#axisState
    ...    value=0    precision=10
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    fesa_interface.Wait For Value    uri=TestMLStepper/Acquisition#position_units    value=mm
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    # precision should have no effect
    fesa_interface.Wait For Value    uri=TestMLStepper/Acquisition#switch_minus
    ...    value=false    precision=10
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    fesa_interface.Wait For Value    uri=TestMLStepper/Acquisition#switch_minus    value=False
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    fesa_interface.Wait For Value    uri=TestMLStepper/Acquisition#switch_minus    value=${False}
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    Run Keyword And Expect Error    STARTS: Unmatched    fesa_interface.Wait For Value
    ...    uri=TestMLStepper/Acquisition#position_units    value=nonsense    timeout=3

Get Info
    [Documentation]    Test getting info from the Fesa interface

    ${info} =    fesa_interface.Get Info    uri=TestMLStepper/Acquisition#position#axisState
    Dictionary Should Contain Key    ${info}    position
    Dictionary Should Contain Key    ${info}    axisState

    ${info} =    fesa_interface.Get Info    uri=TestMLStepper/DeviceConfiguration
    Dictionary Should Contain Key    ${info}    axisIndex
    Dictionary Should Contain Key    ${info}    platformID
    Dictionary Should Contain Key    ${info}    deviceConfiguration

    Run Keyword And Expect Error    STARTS: Command returned    fesa_interface.Get Info    uri=TestMLStepper/Nonsense
    Run Keyword And Expect Error
    ...    STARTS: No Field Information
    ...    fesa_interface.Get Info
    ...    uri=TestMLStepper/Acquisition#Nonsense

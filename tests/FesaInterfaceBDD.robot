*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-10-25
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Test of FesaInterfaceBDD resource: requires a FESA class and some hardware/simulation

Resource            ${EXECDIR}/tester/FesaInterfaceBDD.resource

Suite Setup         I connect to "${hostname}" to open a FESA interface
Suite Teardown      I disconnect from the FESA interface

# Test requires tn access to run
Test Tags           robot:skip


*** Variables ***
${device_name} =    TestMLStepper
${hostname} =       cwe-513-vpl818
${SSH_COMMAND} =    ssh ${hostname}


*** Test Cases ***
Get Values
    [Documentation]    Simple get using resource
    ${state} =    I get FESA value for "${device_name}/Status#state"
    Should Be Equal As Integers    ${state}    1

Set Values
    [Documentation]    Simple set using resource
    I set FESA field "${device_name}/AbsolutePosition#position" to value "7"
    Sleep    15s    # wait for the FESA command to execute
    ${position} =    I get FESA value for "${device_name}/Acquisition#position"
    Should Be Equal As Numbers    ${position}    7

Wait for Values
    [Documentation]    Wait for a value to change to a given value

    Given I set FESA field "${device_name}/AbsolutePosition#position" to value "0.75"
    Then I wait for "${device_name}/Acquisition#position" FESA field value to be "0.75"
    ${position} =    I get FESA value for "${device_name}/Acquisition#position"
    Should Be Equal As Numbers    ${position}    0.75
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    When I set FESA field "${device_name}/AbsolutePosition#position" to value "10"
    Then I wait for "${device_name}/Acquisition#position" FESA field value to be "10"
    ${position} =    I get FESA value for "${device_name}/Acquisition#position"
    Should Be Equal As Numbers    ${position}    10
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    Then I set FESA field "${device_name}/AbsolutePosition#position" to value "-10"
    Then I wait for "${device_name}/Acquisition#position" FESA field value to be "-10"
    ${position} =    I get FESA value for "${device_name}/Acquisition#position"
    Should Be Equal As Numbers    ${position}    -10
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    Then I set FESA field "${device_name}/AbsolutePosition#position" to value "-0.127"
    Then I wait for "${device_name}/Acquisition#position" FESA field value to be "-0.127"
    ${position} =    I get FESA value for "${device_name}/Acquisition#position"
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    Then I wait for "${device_name}/Acquisition#position_units" FESA field value to be "mm"
    Log    Keyword duration: ${KEYWORD_DURATION} milliseconds

    Run Keyword And Expect Error    STARTS: Unmatched
    ...    I wait for "${device_name}/Acquisition#position_units" FESA field value to be "nonsense"    timeout=5

Get Info
    [Documentation]    Get info about the FESA device
    ${info} =    I get info for FESA field "${device_name}/Status#state"
    Dictionary Should Contain Key    ${info}    state
    Dictionary Should Contain Key    ${info}[state]    Type
    Dictionary Should Contain Key    ${info}[state]    Value

    ${info} =    I get info for FESA property "${device_name}/DeviceConfiguration"
    Dictionary Should Contain Key    ${info}    axisIndex
    Dictionary Should Contain Key    ${info}    platformID
    Dictionary Should Contain Key    ${info}    deviceConfiguration

    Run Keyword And Expect Error    STARTS: Command returned    I get info for FESA property "${device_name}/Nonsense"
    Run Keyword And Expect Error
    ...    STARTS: No Field Information
    ...    I get info for FESA field "${device_name}/Acquisition#Nonsense"

*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-03-17T10:53:35
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Library             tester.EdgeConsole
Variables           config.yaml

Test Teardown       Disconnect

# This test requires functional hardware to run
Test Tags           robot:skip


*** Variables ***
${command}      ssh -t cfn-937-sam1.cern.ch edgeconsole


*** Test Cases ***
I can connect EdgeConsole
    Connect    ${command}
    Execute Command    help

I can read a register
    Connect    ${command}
    Open Device    fmc_mfe
    ${out} =    Read Register    motion_core.dinr
    Should Be True    type(${out}) == int

    ${out} =    Read Register    motion_core.res_angle
    Should Be True    type(${out}) == list

    ${out} =    Read Register    motion_core.res_angle    fmt=<f
    Should Be True    type(${out}) == list
    Length Should Be    ${out}    8

    ${out} =    Read Register    motion_core.res_angle    fmt=<f    from=2    nelt=1
    Should Be True    type(${out}) == float

I can write a register
    Connect    ${command}
    Open Device    fmc_mfe

    Write Register    motion_core.res_mode    ${200}

    ${out} =    Read Register    motion_core.res_mode
    Should Be Equal    ${out}    ${{[200 for i in range(8)]}}

    Write Register    motion_core.res_mode    ${400}    nelt=1
    ${out} =    Read Register    motion_core.res_mode
    Should Be Equal As Integers    ${out[0]}    ${400}
    Should Be Equal As Integers    ${out[1]}    ${200}

    # Using a string describing an hexadecimal value
    Write Register    motion_core.res_mode    0x190
    ${out} =    Read Register    motion_core.res_mode
    Should Be Equal    ${out}    ${{[400 for i in range(8)]}}

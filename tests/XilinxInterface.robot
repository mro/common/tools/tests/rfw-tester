*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-11-08
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Test the Xilinx Tools library

Library             tester.XilinxInterface    AS    xsct
Library             Collections
Variables           ${EXECDIR}/tests/TestXilinxInterface/TestConfiguration.json

Test Setup          xsct.Start
...                     command=bash -c 'cd ${xilinx_workspace_location}; x-builder xsct'
Test Teardown       xsct.Stop

Test Tags           robot:skip


*** Test Cases ***
The interface can be started with only a command
    [Documentation]    The interface can be started by simply giving a command to start `xsct`
    [Setup]    No Operation

    xsct.Start    command=bash -c 'cd ${xilinx_workspace_location}; x-builder xsct'

The interface can be started with a command and a port
    [Documentation]    The interface can be started with a port specified to change (from default)
    ...    which port the server listens on
    [Setup]    No Operation

    xsct.Start
    ...    command=bash -c 'cd ${xilinx_workspace_location}; x-builder xsct'
    ...    port=55555
    ${report} =    xsct.Get Interface Details
    Should Be Equal As Strings    ${report['server']['port']}    55555

The interface cannot be started with a bad command
    [Documentation]    A bad command is given
    [Setup]    No Operation

    Run Keyword And Expect Error
    ...    *
    ...    xsct.Start
    ...    command=ls
    [Teardown]    No Operation

The interface cannot be started with a bad port
    [Documentation]    A bad value for the port argument is given
    [Setup]    No Operation

    Run Keyword And Expect Error
    ...    *
    ...    xsct.Start
    ...    command=bash -c 'cd ${xilinx_workspace_location}; x-builder xsct'
    ...    port=${None}

The interface can be stopped completely
    [Documentation]    Stopping the running interface succeeds and is complete

    xsct.Stop
    ${report} =    xsct.Get Interface Details
    Should Be Equal As Strings    ${report['server']['state']}    dead
    Should Be Equal As Strings    ${report['client']['state']}    disconnected
    Should Be Equal    ${report['server']['port']}    ${None}
    Should Be Equal    ${report['client']['port']}    ${None}
    Should Be Equal    ${report['server']['hostname']}    ${None}
    Should Be Equal    ${report['client']['hostname']}    ${None}
    [Teardown]    No Operation

Stop by itself does nothing
    [Documentation]    Trying to stop without a running interface does nothing
    [Setup]    No Operation

    xsct.Stop
    [Teardown]    No Operation

A good JTAG device can be connected to
    [Documentation]    Connecting to good JTAG devices
    ...    I want to be able to connect to a valid JTAG target using it's URL
    ...    so that I can perform some operations

    xsct.Add Device Alias    ${device_alias}    ${device_url}
    xsct.Connect    ${device_alias}
    xsct.Connect    ${device_url}

A bad JTAG device cannot be connected to and fails
    [Documentation]    Connecting to a bad JTAG device fails and stops execution
    ...    If I give a bad JTAG target and try to connect, execution should cease

    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Connect
    ...    device=bad_device

The active JTAG device can be disconnected from
    [Documentation]    Disconnecting without specifying a device channel disconnects
    ...    the active device. When I try to disconnect without giving a channel-id,
    ...    the active channel should be disconnected

    xsct.Connect    device=${device_url}
    ${connections} =    xsct.Get Connected Devices
    Evaluate    len(${connections}) == 1
    xsct.Disconnect
    ${connections} =    xsct.Get Connected Devices
    Evaluate    len(${connections}) == 0

A specified JTAG device can be disconnected from
    [Documentation]    Disconnecting and specifying a device channel disconnects the
    ...    specified device. When I try to disconnect while giving a channel id, the
    ...    specified channel should be disconnected

    xsct.Connect    device=${device_url}
    ${connections} =    xsct.Get Connected Devices
    Evaluate    len(${connections}) == 1
    xsct.Disconnect    channel_id=${connections[0]}
    ${connections} =    xsct.Get Connected Devices
    Evaluate    len(${connections}) == 0

Trying to disconnect when there are no connections leads to an error
    [Documentation]    Disconnecting without specifying a device channel while there are
    ...    no connections. When I try to disconnect without giving a channel id and there
    ...    are no connections, I should get an error

    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Disconnect

Targets can be retrieved from a device when connected
    [Documentation]    A list of targets can be retrieved from the connected board

    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Get Device Targets
    xsct.Connect    device=${device_url}
    ${targets} =    xsct.Get Device Targets
    Evaluate    len(${targets}) > 0
    Should Be True    len(${targets}) > 0
    Should Be True    len(${targets[0]}) == 3
    Should Be True    isinstance(${targets[0]}, tuple)
    Should Be True    isinstance(${targets[0][0]}, int)

Targets can be specified by number and activated on a connected device
    [Documentation]    Test connecting to a target on a device using the target number

    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Select Target
    ...    target=10

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${target} =    xsct.Get Active Target
    Should Be Equal As Integers    ${target[0]}    10

Targets can be specified by name and activated on a connected device
    [Documentation]    Test connecting to a target on a device using the target name
    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Select Target
    ...    target=Cortex-A53 #0

    xsct.Connect    device=${device_url}
    ${name} =    Set Variable    Cortex-A53 #0
    xsct.Select Target    target=${name}
    ${target} =    xsct.Get Active Target
    Should Be Equal As Strings    ${target[1]}    ${name}

The presently active target can be retrieved
    [Documentation]    Test getting the active target
    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${target} =    xsct.Get Active Target
    Should Be True    isinstance(${target}, tuple)
    Should Be True    len(${target}) == 3
    Should Be Equal As Integers    ${target[0]}    10

If there is no currently active target, None is returned
    [Documentation]    Requesting the active target when there is no target selected
    xsct.Connect    device=${device_url}
    ${target} =    xsct.Get Active Target
    Should Be Equal As Strings    ${target}    ${None}

Trying to connect with index to a bad target fails
    [Documentation]    Test connecting to a bad target
    xsct.Connect    device=${device_url}
    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Select Target
    ...    target=99

Trying to connect with name to a bad target fails
    [Documentation]    Test connecting to a bad target
    xsct.Connect    device=${device_url}
    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Select Target
    ...    target=bad_target

A single memory value can be read from a connected target as a hexadecimal value
    [Documentation]    Reading a location of memory providing minimum arguments

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${res} =    xsct.Memory Read    address=0x0
    Log    message=${res}
    Should Be True    isinstance(${res}, list)

Memory can be read from a connected target as a list of hexadecimal values
    [Documentation]    Reading a range of memory providing address and number of
    ...    values to read

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${res} =    xsct.Memory Read    address=0x0    num=0x10
    Log    message=${res}
    Should Be True    isinstance(${res}, list)

A single memory value can be read as a char
    [Documentation]    Reading a location of memory as a specific type

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${res} =    xsct.Memory Read    address=0x0    fmt=>B
    Log    message=${res}
    Should Be True    isinstance(${res[0]}, int)
    Should Be True    0 <= ${res[0]} <= 255

A range of memory can be read as a list of chars
    [Documentation]    Reading a range of memory as a specific type

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${res} =    xsct.Memory Read    address=0x0    num=10    fmt=>B
    Log    message=${res}
    Should Be True    isinstance(${res[0]}, int)
    Should Be True    0 <= ${res[0]} <= 255

A single 1 byte memory value can be read
    [Documentation]    Reading a location of memory as a specific size

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${res} =    xsct.Memory Read    address=0x0    size=1
    Log    message=${res}
    ${res} =    Evaluate    int('${res[0]}', 16)
    Should Be True    isinstance(${res}, int)
    Should Be True    0 <= ${res} <= 255

A range of 1 bytes of memory can be read
    [Documentation]    Reading a range of memory as a specific size

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${res} =    xsct.Memory Read    address=0x0    num=10    size=1
    Log    message=${res}
    Should Be True    isinstance(${res[0]}, int)
    ${res} =    Evaluate    int('${res[0]}', 16)
    Should Be True    isinstance(${res}, int)
    Should Be True    0 <= ${res} <= 255

A single value can be written to memory
    [Documentation]    Writing to a location of memory providing minimum arguments

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    xsct.Memory Write    address=0x0    values=[0x1234]
    ${res} =    xsct.Memory Read    address=0x0
    ${res_str} =    Evaluate    [str(x) for x in ${res}]
    List Should Contain Value    ${res}    00001234

A range of values can be written to memory
    [Documentation]    Writing a range of memory providing address and a list of values to write

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    xsct.Memory Write    address=0x0    values=['0x1234', '0x5678']    num=10
    ${res} =    xsct.Memory Read    address=0x0    num=0x10
    ${res_str} =    Evaluate    [str(x) for x in ${res}]
    List Should Contain Value    ${res}    00001234

A single value of specified size can be written to memory
    [Documentation]    Writing a location of memory as a specific size

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    xsct.Memory Write    address=0x0    values=[0x1234]    size=2
    ${res} =    xsct.Memory Read    address=0x0    size=2
    ${res_str} =    Evaluate    [str(x) for x in ${res}]
    List Should Contain Value    ${res}    1234

A range of values of specified size can be written to memory
    [Documentation]    Writing a range of memory as a specific size

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    xsct.Memory Write    address=0x0    values=['0x1234', '0x5678']    num=10    size=2
    ${res} =    xsct.Memory Read    address=0x0    num=0x10    size=4
    ${res_str} =    Evaluate    [str(x) for x in ${res}]
    List Should Contain Value    ${res}    56781234

The state of a target can be retrieved
    [Documentation]    Test getting state of target

    xsct.Connect    device=${device_url}
    xsct.Select Target    target=10
    ${res} =    xsct.Get Target State
    Log    message=${res}
    Should Be Equal As Strings    ${res}    running    ignore_case=True

Trying to get the state of a bad target fails
    [Documentation]    Test getting state of a bad target

    xsct.Connect    device=${device_url}
    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    xsct.Get Target State

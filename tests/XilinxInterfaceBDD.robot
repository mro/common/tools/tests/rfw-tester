*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-12-14
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Test the Xilinx tools library and its keywords.

Resource            ${EXECDIR}/tester/XilinxInterfaceBDD.resource
Variables           ${EXECDIR}/tests/TestXilinxInterface/TestConfiguration.json

Test Setup          I start the Xilinx interface with command "${COMMAND}"
Test Teardown       I stop the Xilinx Interface

Test Tags           robot:skip


*** Variables ***
${COMMAND}      bash -c 'cd ${xilinx_workspace_location}; x-builder xsct'


*** Test Cases ***
Starting the interface with only a command
    [Documentation]    The interface can be started by simply giving a command to start `xsct`
    [Setup]    No Operation

    I start the Xilinx interface with command "${COMMAND}"

Starting the interface with a command and port
    [Documentation]    The interface can be started with a port specified to change (from default)
    ...    which port the server listens on
    [Setup]    No Operation

    I start the Xilinx Interface at port "55555" with command "${COMMAND}"

    ${report} =    I get the Xilinx Interface details
    Should Be Equal As Strings    ${report['server']['port']}    55555

Failing to start the interface with a bad command
    [Documentation]    A bad command is given
    [Setup]    No Operation

    ${command} =    Set Variable    echo "bad_command"
    Run Keyword And Expect Error
    ...    *
    ...    I start the Xilinx Interface with command "${command}"
    [Teardown]    No Operation

Failing to start the interface with a bad port
    [Documentation]    A bad value for the port argument is given
    [Setup]    No Operation

    ${command} =    Set Variable    bash -c 'cd ${xilinx_workspace_location}; x-builder xsct'
    Run Keyword And Expect Error
    ...    *
    ...    I start the Xilinx Interface at port "bad_port" with command "${command}"

The interface can be stopped completely
    [Documentation]    Stopping the interface succeeds and in complete

    I stop the Xilinx Interface
    ${report} =    I get the Xilinx Interface details
    Should Be Equal As Strings    ${report['server']['state']}    dead
    Should Be Equal As Strings    ${report['client']['state']}    disconnected
    Should Be Equal    ${report['server']['port']}    ${None}
    Should Be Equal    ${report['client']['port']}    ${None}
    Should Be Equal    ${report['server']['hostname']}    ${None}
    Should Be Equal    ${report['client']['hostname']}    ${None}
    [Teardown]    No Operation

Stop by itself does nothing
    [Documentation]    Trying to stop without a running interface does nothing
    [Setup]    No Operation

    I stop the Xilinx Interface
    [Teardown]    No Operation

Connecting to a JTAG device
    [Documentation]    Connecting to good JTAG devices
    ...    I want to be able to connect to a valid JTAG target using it's URL
    ...    so that I can perform some operations

    Given I add XSCT device alias "${device_alias}" for "${device_url}"
    ${channel_id} =    I connect to XSCT device "${device_alias}"
    Then I check if channel ${channel_id} is valid
    And I disconnect from XSCT device "${channel_id}"
    ${channel_id} =    I connect to XSCT device "${device_url}"
    Then I check if channel ${channel_id} is valid
    And I disconnect from active XSCT device

Connecting to a bad JTAG device
    [Documentation]    Connecting to a bad JTAG device fails and stops execution
    ...    If I give a bad JTAG target and try to connect, execution should cease

    Run Keyword And Expect Error
    ...    STARTS: ERROR
    ...    I connect to XSCT device "bad_device"

The active JTAG device can be disconnected from
    [Documentation]    Disconnecting without specifying a device channel disconnects
    ...    the active device. When I try to disconnect without giving a channel-id,
    ...    the active channel should be disconnected

    Given I connect to XSCT device "${device_url}"
    ${connections} =    I get connected XSCT devices
    Evaluate    len(${connections}) == 1
    Then I disconnect from active XSCT device
    ${connections} =    I get connected XSCT devices
    Evaluate    len(${connections}) == 0

A specified JTAG device can be disconnected from
    [Documentation]    Disconnecting and specifying a device channel disconnects the
    ...    specified device. When I try to disconnect while giving a channel id, the
    ...    specified channel should be disconnected

    Given I connect to XSCT device "${device_url}"
    ${channel_id} =    I get the active XSCT channel id
    Then I disconnect from XSCT device "${channel_id}"
    ${channel_id} =    I get the active XSCT channel id
    Should Be True    ${channel_id} is None

Disconnecting from the active JTAG device that is not connected fails
    [Documentation]    Disconnecting without specifying a device channel while there are
    ...    no connections. When I try to disconnect without giving a channel id and there
    ...    are no connections, I should get an error

    Run Keyword And Expect Error
    ...    STARTS: ERROR:
    ...    I disconnect from active XSCT device

Disconnecting from a specific JTAG device that is not connected fails
    [Documentation]    Disconnecting while specifying a device channel while there are
    ...    no connections. When I try to disconnect without giving a channel id and there
    ...    are no connections, I should get an error

    Run Keyword And Expect Error
    ...    STARTS: ERROR:
    ...    I disconnect from XSCT device "bad_device"

Targets can be retrieved from a valid device
    [Documentation]    Test that the targets of a device can be retrieved.
    I connect to XSCT device "${device_url}"
    ${targets} =    I get XSCT device targets
    Should Be True    len(${targets}) > 0
    Should Be True    isinstance(${targets[0]}, tuple)

Getting targets from a device fails when no device is connected
    [Documentation]    Test that the targets of a device can be retrieved.
    Run Keyword And Expect Error
    ...    *
    ...    I get XSCT device targets

    I connect to XSCT device "${device_url}"
    ${targets} =    I get XSCT device targets
    Should Be True    len(${targets}) > 0

Selecting a target when no device is connected fails
    [Documentation]    Test connecting to a target when there is no connected device
    Run Keyword And Expect Error
    ...    *
    ...    I select XSCT target with number "10"

Targets on the JTAG can be connected to by number
    [Documentation]    Test connecting to a target on a device using the target number
    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${target} =    I get the active XSCT target
    Should Be Equal As Integers    ${target[0]}    10

Targets on the JTAG can be connected to by name
    [Documentation]    Test connecting to a target on a device using the target name
    I connect to XSCT device "${device_url}"
    ${name} =    Set Variable    Cortex-A53 #0
    I select XSCT target with number "${name}"
    ${target} =    I get the active XSCT target
    Should Be Equal As Integers    ${target[1]}    ${name}

The presently active target can be retrieved
    [Documentation]    Test getting the active target
    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${target} =    I get the active XSCT target
    Should Be True    isinstance(${target}, tuple)
    Should Be True    len(${target}) == 3
    Should Be Equal As Integers    ${target[0]}    10

If there is no currently active target, None is returned
    [Documentation]    Requesting the active target when there is no target selected
    I connect to XSCT device "${device_url}"
    ${target} =    I get the active XSCT target
    Should Be Equal As Strings    ${target}    ${None}

Trying to connect with index to a bad target fails
    [Documentation]    Test connecting to a bad target
    I connect to XSCT device "${device_url}"
    Run Keyword And Expect Error
    ...    STARTS: ERROR: Target
    ...    I select XSCT target with number "99"

Trying to connect with name to a bad target fails
    [Documentation]    Test connecting to a target
    Given I connect to XSCT device "${device_url}"
    Run Keyword And Expect Error
    ...    STARTS: ERROR: Target
    ...    I select XSCT target with name "bad_target"

A single 4-byte hexadecimal value can be read from memory
    [Documentation]    Test reading single 4-byte hexadecimal memory value

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${res} =    I use XSCT to read a "4" byte data value from "0x0"
    Log    message=${res}

A list of hex values can be read from memory
    [Documentation]    Test reading memory

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${res} =    I use XSCT to read "10" "4" byte data values from "0x0"
    Log    message=${res}
    Should Be True    isinstance(${res}, list)

A single value can be read from memory as a char
    [Documentation]    Reading a location of memory as a specific type

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${res} =    I use XSCT to read a data value from "0x0" with format ">B"
    Log    message=${res}
    Should Be True    isinstance(${res}, int)
    Should Be True    0 <= ${res} <= 255

A list of char values can be read from memory
    [Documentation]    Test reading a range of memory as a specific type

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${res} =    I use XSCT to read "10" data values from "0x0" with format ">B"
    Log    message=${res}
    Should Be True    isinstance(${res}, list)
    Should Be True    isinstance(${res[0]}, int)
    Should Be True    0 <= ${res[0]} <= 255

A single 1-byte value can be read from memory
    [Documentation]    Test reading single memory value of a specific size

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${res} =    I use XSCT to read a "1" byte data value from "0x0"
    Log    message=${res}
    ${res} =    Evaluate    int('${res[0]}', 16)
    Should Be True    isinstance(${res}, int)
    Should Be True    0 <= ${res} <= 255

A range of 1 bytes of memory can be read
    [Documentation]    Reading a range of memory as a specific size

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${res} =    I use XSCT to read "10" "1" byte data values from "0x0"
    Log    message=${res}
    Should Be True    isinstance(${res[0]}, int)
    ${res} =    Evaluate    int('${res[0]}', 16)
    Should Be True    isinstance(${res}, int)
    Should Be True    0 <= ${res} <= 255

A single value can be written to memory
    [Documentation]    Test writing a single memory value

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${values} =    Create List    0x1234
    I use XSCT to write "${values}" at address "0x0"
    ${res} =    I use XSCT to read a "4" byte data value from "0x0"
    ${res_str} =    Evaluate    str(${res[0]})
    Should Be Equal As Strings    ${res_str}    00001234

A range of hex values can be written to memory
    [Documentation]    Test writing memory

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${values} =    Create List    0x1234    0x5678
    I use XSCT to write "${values}" at address "0x0"
    ${res} =    I use XSCT to read "2" "4" byte data values from "0x0"
    ${res_str} =    Evaluate    [str(x) for x in ${res}]
    Should Be Equal As Strings    ${res_str}    ['00001234', '00005678']

A single 1-byte value can be written to memory
    [Documentation]    Test writing a single memory value of a specific size

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${values} =    Create List    0xFF
    I use XSCT to write "${values}" at address "0x0"
    ${res} =    I use XSCT to read a data value from "0x0" with format ">B"
    Should Be Equal As Integers    ${res}    255

1 Byte values can be written to memory
    [Documentation]    Test writing different sized values

    Given I connect to XSCT device "${device_url}"
    And I select XSCT target with number "10"
    ${values} =    Create List    0xFF    0xCC
    When I use XSCT to write "1" byte values "${values}" at address "0x0"
    ${res} =    I use XSCT to read "2" data values from "0x0" with format ">B"
    ${expect} =    Create List    255    204
    ${str_res} =    Evaluate    [str(x) for x in ${res}]
    Lists Should Be Equal    ${str_res}    ${expect}

A targets state can be retrieved
    [Documentation]    Test getting the target state

    I connect to XSCT device "${device_url}"
    I select XSCT target with number "10"
    ${state} =    I get the XSCT target state
    Log    message=${state}
    Should Be Equal As Strings    ${state}    running    ignore_case=True

Trying to get the state of a bad target fails
    [Documentation]    Test getting the target state of a bad target

    I connect to XSCT device "${device_url}"
    Run Keyword And Expect Error
    ...    STARTS: ERROR: Target
    ...    I get the XSCT target state

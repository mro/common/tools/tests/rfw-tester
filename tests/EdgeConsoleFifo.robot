*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-05-31T09:17:50
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Resource            ${EXECDIR}/tester/EdgeConsoleBDD.resource
Library             tester.EdgeConsoleFifo.Connector    AS    FIFO
Variables           config.yaml

Test Teardown       I disconnect EdgeConsole

# This test requires functional hardware to run
Test Tags           robot:skip


*** Variables ***
${EDGECONSOLE_COMMAND} =    ssh -t cfn-937-sam1.cern.ch edgeconsole
${EDGE_DRIVER} =            fmc_mfe
${EDGE_LUN} =               0


*** Test Cases ***
I can communicate using EdgeConsoleFifo
    Given I connect EdgeConsole

    ${edge} =    I get EdgeConsole instance
    FIFO.Connect EdgeConsoleFifo    ${edge}

    FIFO.Send    0?
    ${out} =    FIFO.Expect    <(.*)>[\r|\n]+

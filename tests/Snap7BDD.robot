*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-07-04T20:19:19
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Resource            ${EXECDIR}/tester/Snap7BDD.resource
Variables           ${CURDIR}/config.yaml

Test Teardown       I disconnect PLC

# This test requires functional hardware to run
Test Tags           robot:skip


*** Test Cases ***
I can connect a PLC
    I connect PLC "${plc_address}"

I can read a value
    I connect PLC "${plc_address}"
    I load PLC DB layout "${plc_layout}" for DB "1"

    ${position} =    I read "actualPosition" PLC variable
    Should Be True    type(${position}) == int

I can write a value
    I connect PLC "${plc_address}"
    I load PLC DB layout "${plc_layout}" for DB "1"

    I write "${12345}" to "referencePosition" PLC variable
    I write "${True}" to "setActualPosition" PLC variable

    ${position} =    I read "actualPosition" PLC variable
    Should Be Equal As Integers    12345    ${position}
    ...    msg=position should have been set

I can add variable to layout
    I connect PLC "${plc_address}"
    I set current PLC DB to "1"
    I add "actualPosition" variable with datatype "DINT" at address "8" to current PLC layout
    I add "setActualPosition" variable with datatype "BOOL" at address "28.0" to current PLC layout
    I add "referencePosition" variable with datatype "DINT" at address "24" to current PLC layout

    I write "${-12346}" to "referencePosition" PLC variable
    I write "${True}" to "setActualPosition" PLC variable

    ${position} =    I read "actualPosition" PLC variable
    Should Be Equal As Integers    -12346    ${position}
    ...    msg=position should have been set

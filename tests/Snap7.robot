*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-07-04T20:19:19
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Library             tester.Snap7
Variables           ${CURDIR}/config.yaml

Test Teardown       Disconnect

# This test requires functional hardware to run
Test Tags           robot:skip


*** Test Cases ***
I can connect a PLC
    Connect    ${plc_address}

I can read a value
    Connect    ${plc_address}
    Load Db Layout    1    ${plc_layout}

    ${position} =    Read Value    actualPosition
    Should Be True    type(${position}) == int

I can write a value
    Connect    ${plc_address}
    Load Db Layout    1    ${plc_layout}

    Write Value    referencePosition    ${12345}
    Write Value    setActualPosition    ${True}

    ${position} =    Read Value    actualPosition
    Should Be Equal As Integers    12345    ${position}
    ...    msg=position should have been set

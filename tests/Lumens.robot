*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-07-03
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Test the Lumens library

Library             tester.Lumens    AS    lumens

Test Setup          lumens.Set Lumens Command    ${lumens_cmd}

Test Tags           robot:skip


*** Variables ***
${hostname}         cfn-937-sam1
${lumens_cmd}       ssh -tt ${hostname} /usr/local/sbin/lumensctl
${service}          MLStepperAxis_DU_M


*** Test Cases ***
The status of a Lumens service can be retreived
    [Documentation]    Test that the status of a Lumens service can be retreived

    ${status} =    lumens.Service Status    service=${service}
    Should Not Be Empty    ${status}
    Should Contain    ${status}    ${service}
    Should Contain    ${status}    Loaded:
    Should Contain    ${status}    Active:

A Lumens service can be started
    [Documentation]    Test that a Lumens service can be started

    lumens.Start Service    service=${service}
    ${running} =    lumens.Service Is Running    service=${service}
    Should Be True    ${running}

A Lumens service can be stopped
    [Documentation]    Test that a Lumens service can be stopped

    lumens.Stop Service    service=${service}
    ${running} =    lumens.Service Is Running    service=${service}
    Should Not Be True    ${running}

A Lumens service can be restarted
    [Documentation]    Test that a Lumens service can be restarted

    lumens.Stop Service    service=${service}
    ${running} =    lumens.Service Is Running    service=${service}
    Should Not Be True    ${running}

    lumens.Restart Service    service=${service}
    Sleep    5s
    ${running} =    lumens.Service Is Running    service=${service}
    Should Be True    ${running}

A list of Lumens services can be retreived
    [Documentation]    Test that a list of Lumens services can be retreived

    ${services} =    lumens.List Services
    Should Not Be Empty    ${services}
    Should Contain    ${services}    ${service}

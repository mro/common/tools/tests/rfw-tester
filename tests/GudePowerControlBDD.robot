*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2024-01-23
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Testing the Robot Framework Keyword resource for the remote control of
...                 the Gude Power Control device

Resource            ${EXECDIR}/tester/GudePowerControlBDD.resource

Test Setup          I set GUDE url to "${URL}"

Test Tags           robot:skip


*** Variables ***
${URL}      http://cfs-937-sam2.cern.ch


*** Test Cases ***
I can get the switch status
    ${status} =    I get GUDE switch status
    Log    ${status}

I can get the switch state
    ${state} =    I check if GUDE output is on
    Log    ${state}

I can turn the switch on
    Given I turn GUDE switch on
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${True}

I can turn the switch off
    Given I turn GUDE switch off
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${False}

I can pulse the switch to a desired state for a desired time
    Given I pulse GUDE switch at state "On" for "10" seconds
    Sleep    5s
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${True}
    Sleep    10s
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${False}

I can cancel a pulse
    Given I pulse GUDE switch at state "On" for "10" seconds
    Sleep    5s
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${True}
    Then I cancel a GUDE pulse
    Sleep    10s
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${True}

I can reset the switch device
    Given I reset the GUDE device
    Sleep    5s
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${False}
    Sleep    30s    # sleep for the reset time specified in the device configuration
    ${state} =    I check if GUDE output is on
    Then Should Be True    ${state} is ${True}

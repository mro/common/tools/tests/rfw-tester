*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2025 CERN (home.cern)
# SPDX-Created: 2025-02-17T15:35:28
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Library     ${EXECDIR}/tester/LoopLimiter.py


*** Test Cases ***
For in range loops are limited
    Set Max Loop Iterations    ${{pow(2, 16)}}
    Run Keyword And Expect Error    *too large*
    ...    Create loop with "${{pow(2, 17)}}" iterations

    Set Max Loop Iterations    ${{100}}
    Create loop with "100" iterations
    Run Keyword And Expect Error    *too large*
    ...    Create loop with "101" iterations

For in range loops are limits are set per test
    ${limit} =    Get Max Loop Iterations
    Should Be Equal As Integers    ${limit}    ${{pow(2, 16)}}

    Run Keyword And Expect Error    *too large*
    ...    Create loop with "${{pow(2, 17)}}" iterations


*** Keywords ***
Create loop with "${num}" iterations
    FOR    ${x}    IN RANGE    0    ${num}
        Log    ${x}
    END

*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2024 CERN (home.cern)
# SPDX-Created: 2024-04-04
# SPDX-FileContributor: Author: Sam Wilson <samuel.wilson@cern.ch>


*** Settings ***
Documentation       Test of the Robot Framework listener implementation

Library             ${EXECDIR}/tester/ExtendedLogging.py
Library             ${EXECDIR}/tester/Listener.py


*** Variables ***
@{LISTENER_KEYWORDS} =      Set Max Logged Iterations


*** Test Cases ***
Listener should give the keyword duration only for the specified keywords
    [Documentation]    Listener should give the keyword duration only for the specified keywords

    Update Listener Keywords    Set Max Logged Iterations

    Enable Extended Logging
    ${enable_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Log    Keyword duration: ${enable_duration} milliseconds
    Should Be True    $enable_duration is ${None}
    Set Max Logged Iterations    10
    ${iter_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Should Be True    $iter_duration is not ${None}
    Log    Keyword duration: ${iter_duration} milliseconds
    Disable Extended Logging
    ${disable_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Log    Keyword duration: ${disable_duration} milliseconds
    Should Be True    $disable_duration is ${None}

Listener should be able to give the keyword duration for all keywords
    [Documentation]    Listener should be able to give the keyword duration for all keywords

    Update Listener Keywords    All

    Enable Extended Logging
    ${enable_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Log    Keyword duration: ${enable_duration} milliseconds
    Should Be True    $enable_duration is not ${None}
    Set Max Logged Iterations    10
    ${iter_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Should Be True    $iter_duration is not ${None}
    Log    Keyword duration: ${iter_duration} milliseconds
    Disable Extended Logging
    ${disable_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Log    Keyword duration: ${disable_duration} milliseconds
    Should Be True    $disable_duration is not ${None}

Keywords listener pays attention to can be updated
    [Documentation]    Listener should be able to update the keywords it pays attention to

    Update Listener Keywords    All

    Enable Extended Logging
    ${enable_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Log    Keyword duration: ${enable_duration} milliseconds
    Should Be True    $enable_duration is not ${None}
    Set Max Logged Iterations    10
    ${iter_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Should Be True    $iter_duration is not ${None}

    Update Listener Keywords    Set Max Logged Iterations

    Enable Extended Logging
    ${enable_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Log    Keyword duration: ${enable_duration} milliseconds
    Should Be True    $enable_duration is ${None}
    Set Max Logged Iterations    10
    ${iter_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Should Be True    $iter_duration is not ${None}

    @{LISTENER_KEYWORDS} =    Create List    Enable Extended Logging
    Update Listener Keywords

    Enable Extended Logging
    ${enable_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Log    Keyword duration: ${enable_duration} milliseconds
    Should Be True    $enable_duration is not ${None}
    Set Max Logged Iterations    10
    ${iter_duration} =    Get Variable Value    ${KEYWORD_DURATION}
    Should Be True    $iter_duration is ${None}

    Disable Extended Logging

*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-12
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Library             tester.Shell    prompt=\\$\    env=${{{ 'PS1': '$ ' }}}
Variables           config.yaml

Test Teardown       Disconnect


*** Variables ***
${command}      bash --norc --noprofile


*** Test Cases ***
Shell Simple Connection
    Connect    ${command}
    Sync Prompt
    Execute Command    echo world

Shell Sync Prompt
    Connect    ${command}
    Sync Prompt
    ${out} =    Execute Command    echo world
    Should Be Equal    world    ${out.strip()}

Shell Failure
    Connect    ${command}
    Run Keyword And Expect Error    STARTS:Command returned
    ...    Execute Command    false

Shell Output
    Connect    ${command}
    ${out} =    Execute Command    echo world    w..ld
    Should Be Equal    world    ${out.group()}

Shell Output Failure
    Connect    ${command}
    Run Keyword And Expect Error    STARTS:Unmatched pattern
    ...    Execute Command    echo world    X..ld    timeout=2

*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-13
# SPDX-FileContributor: Author: Fargier Sylvain <sylvain.fargier@cern.ch>


*** Settings ***
Library         tester.Expect
Variables       config.yaml


*** Test Cases ***
Basic Connection
    Connect    ${command}
    Sendline    echo World
    Expect    World    timeout=2
    [Teardown]    Disconnect

Failure Test
    [Tags]    fail    robot:skip
    # Not connected
    Sendline    echo World
    Expect    World    timeout=2

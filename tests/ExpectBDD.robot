*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-12
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Resource            ${EXECDIR}/tester/ExpectBDD.resource
Variables           ${CURDIR}/config.yaml

Test Teardown       I Disconnect


*** Test Cases ***
Basic BDD Connection
    Given I connect "${command}"
    When I send "echo World"
    Then I expect "World"

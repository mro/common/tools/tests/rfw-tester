*** Comments ***
# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2023 CERN (home.cern)
# SPDX-Created: 2023-05-25T15:42:57
# SPDX-FileContributor: Author: Sylvain Fargier <sylvain.fargier@cern.ch>


*** Settings ***
Library     ${EXECDIR}/tester/ExtendedLogging.py


*** Test Cases ***
Default for-loop iteration protection
    Enable Extended Logging
    Log    only 20 iterations should be logged
    FOR    ${x}    IN RANGE    1    100
        Log    This is iteration ${x}
    END

    Disable Extended Logging
    Log    all 25 iterations should be logged
    FOR    ${x}    IN RANGE    1    26
        Log    This is iteration ${x}
    END

Nested for-loop iteration protection
    Enable Extended Logging
    Log    only 20 iterations should be logged
    FOR    ${x}    IN RANGE    1    100
        FOR    ${y}    IN RANGE    1    100
            Log    This is iteration ${x} ${y}
        END
    END

Expected Failure
    [Tags]    robot:skip-on-failure
    Enable Extended Logging
    Log    only 20 iterations should be logged, last should be a failure
    FOR    ${x}    IN RANGE    1    100
        FOR    ${y}    IN RANGE    1    100
            Log    This is iteration ${x} ${y}
            Should Be True    ${x} < 50    expected failure
        END
    END

Configured for-loop iteration protection
    Set Max Logged Iterations    5
    Log    only 5 iterations should be logged
    FOR    ${x}    IN RANGE    1    100
        Log    This is iteration ${x}
        # The following log should be visible: `${out} = 12` as "INFO"
        ${out} =    Set Variable    12
    END

Disabling for-loop iteration protection
    Set Max Logged Iterations    0
    Log    all iterations should be logged
    FOR    ${x}    IN RANGE    1    100
        Log    This is iteration ${x}
    END

Default while loop iteration protection
    Enable Extended Logging
    Log    only 20 iterations should be logged
    ${x} =    Set Variable    0
    WHILE    ${x} < 100
        ${x} =    Evaluate    ${x} + 1
        Log    This is iteration ${x}
    END

Configured while-loop iteration protection
    Set Max Logged Iterations    5
    Log    only 5 iterations should be logged
    ${x} =    Set Variable    0
    WHILE    ${x} < 100
        ${x} =    Evaluate    ${x} + 1
        Log    This is iteration ${x}
    END

Disabling while-loop iteration protection
    Set Max Logged Iterations    0
    Log    all iterations should be logged
    ${x} =    Set Variable    0
    WHILE    ${x} < 100
        ${x} =    Evaluate    ${x} + 1
        Log    This is iteration ${x}
    END

Enable Extended Logging in Setup
    [Setup]    Enable Extended Logging

    Enable Extended Logging

Disable Extended Logging in Teardown
    Enable Extended Logging
    [Teardown]    Disable Extended Logging

# Robotframework project draft

## Setup

To prepare environment and run tests :
```bash

python -m venv venv
source venv/bin/activate

pip install -e .

venv/bin/robot examples
```

### Options

The following options are available in this package:
 - **snap7** : Siemens PLC communication support (Snap7 and Snap7BDD)

To install those optional features:
```
python install -e .[snap7]
```

## Development

To install and use development tools:
```bash
pip install -e ".[develop]"

# Lint the project
python setup.py lint

# Style the project
python setup.py style

```

### Tags
The below tags are used to differentiate the test cases:  
`sigrok-cli` - Tests using Sigrok CLI  
`sigrok-cli-0.7.2` - Tests requiring at least 0.7.2 version of Sigrok CLI  
`robot:skip` - Tests to be skipped  
`fail` - Tests that are expected to fail

# SPDX-License-Identifier: Zlib
# SPDX-FileCopyrightText: 2022 CERN (home.cern)
# SPDX-Created: 2022-06-09
# SPDX-FileContributor: Author: Fargier Sylvain <sylvain.fargier@cern.ch>

#!/usr/bin/python

import sys
from pathlib import Path, PurePath
from setuptools import setup, distutils

SRCDIR = "tester"


def python_version_check(major, minor=None):
    """Check if python version is greater or equal to `major.minor`"""
    if sys.version_info.major > major:
        return True
    elif sys.version_info.major == major:
        return minor is None or sys.version_info.minor >= minor
    return False


class StyleScript(distutils.cmd.Command):
    """Run code-style program"""

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        distutils.spawn.spawn(["black", "."])
        if python_version_check(3, 8):
            distutils.spawn.spawn(["robotidy", "."])


class LintScript(distutils.cmd.Command):
    """Run code linter program"""

    user_options = [("fix", "f", "Try to fix linting issues (using autopep8)")]

    def initialize_options(self):
        self.fix = False

    def finalize_options(self):
        pass

    def run(self):
        if self.fix:
            distutils.spawn.spawn(
                ["autopep8", "--in-place", "--verbose", "-a", "-a", "-r", SRCDIR]
            )
        else:
            distutils.spawn.spawn(["pylint", SRCDIR])


class DocScript(distutils.cmd.Command):
    """Generate documentation"""

    user_options = []

    def initialize_options(self):
        pass

    def finalize_options(self):
        pass

    def run(self):
        for file in Path(SRCDIR).glob("*.py"):
            if file.name.startswith("_"):
                continue
            distutils.spawn.spawn(
                [
                    "libdoc",
                    str(file),
                    str(PurePath("public", file.name[:-3] + ".html")),
                ]
            )
        for file in Path(SRCDIR).glob("*.resource"):
            distutils.spawn.spawn(
                [
                    "libdoc",
                    str(file),
                    str(PurePath("public", file.name[:-9] + ".html")),
                ]
            )


setup(
    name="tester",
    version="1.5.0",
    python_requires=">=3.6",
    description="Pexpect test helper",
    author="Fargier Sylvain",
    author_email="sylvain.fargier@cern.ch",
    url="https://gitlab.cern.ch/mro/common/tools/tests/rfw-tester",
    packages=["tester"],
    package_dir={"tester": SRCDIR},
    long_description="""Pexpect test helper for robotframework""",
    install_requires=[
        "robotframework>=6.0.0",
        "pexpect>=4.8.0",
        "pyaml>=6.0",
        "psutil>=5.9.6",
        "requests>=2.27.0",
        "setuptools",
    ],
    extras_require={
        "develop": list(
            filter(
                lambda x: x is not None,
                [
                    "robotframework-tidy>=2.5" if python_version_check(3, 8) else None,
                    "black>=22.3",
                    "pylint>=2.13",
                    "autopep8>=1.6",
                ],
            ),
        ),
        "snap7": ["python-snap7>=1.2"],
    },
    cmdclass={"style": StyleScript, "lint": LintScript, "doc": DocScript},
    include_package_data=True,
)
